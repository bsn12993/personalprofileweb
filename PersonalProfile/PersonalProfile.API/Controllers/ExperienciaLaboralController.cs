﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.API.Controllers
{
    [RoutePrefix("api/workexperience")]
    public class ExperienciaLaboralController : ApiController
    {
        DL_ExperienciaLaboral _services = null;
        public ExperienciaLaboralController()
        {
            _services = new DL_ExperienciaLaboral();
        }

        [Route("all")]
        [HttpGet]
        public HttpResponseMessage GetExperienciasLaborales()
        {
            var respuesta = _services.GetExperienciaLaboral();
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetExperienciaLaboralById(int id)
        {
            var respuesta = _services.GetExperienciaLaboralById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage PostExperienciaLaboral([FromBody] ExperienciaLaboral experienciaLaboral)
        {
            if (ModelState.IsValid && experienciaLaboral != null)
            {
                var respuesta = _services.PostExperienciaLaborial(experienciaLaboral);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutExperienciaLaboral(int id, [FromBody] ExperienciaLaboral experienciaLaboral)
        {
            if (ModelState.IsValid && experienciaLaboral != null)
            {
                var respuesta = _services.PutExperienciaLaboral(id, experienciaLaboral);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteExperienciaLaboral(int id)
        {
            var respuesta = _services.DeleteExperienciaLaboral(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
    }
}
