﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.API.Controllers
{
    [RoutePrefix("api/languages")]
    public class IdiomasController : ApiController
    {
        DL_Idiomas _services = null;
        public IdiomasController()
        {
            _services = new DL_Idiomas();
        }

        [Route("all")]
        [HttpGet]
        public HttpResponseMessage GetIdiomas()
        {
            var respuesta = _services.GetIdiomas();
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetIdiomaById(int id)
        {
            var respuesta = _services.GetIdiomaById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage PostIdioma([FromBody] Idiomas idioma)
        {
            if (ModelState.IsValid && idioma != null)
            {
                var respuesta = _services.PostIdioma(idioma);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutIdioma(int id, Idiomas idioma)
        {
            if (ModelState.IsValid && idioma != null)
            {
                var respuesta = _services.PutIdioma(id, idioma);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteIdioma(int id)
        {
            var respuesta = _services.DeleteIdioma(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
    }
}
