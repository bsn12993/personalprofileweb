﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.API.Controllers
{
    [RoutePrefix("api/typetech")]
    public class TipoTecnologiaController : ApiController
    {
        DL_TipoTecnologia _services;
        public TipoTecnologiaController()
        {
            _services = new DL_TipoTecnologia();
        }

        [Route("all")]
        [HttpGet]
        public HttpResponseMessage GetTipoTecnologia()
        {
            var respuesta = _services.GetTipoTecnologia();
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetTipoTecnologiaById(int id)
        {
            var respuesta = _services.GetTipoTecnologiaById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage PostTipoTecnologia([FromBody] TipoTecnologia tipoTecnologia)
        {
            if (ModelState.IsValid && tipoTecnologia != null)
            {
                var respuesta = _services.PostTipoTecnologia(tipoTecnologia);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutTipoTecnologia(int id, [FromBody] TipoTecnologia tipoTecnologia)
        {
            if (ModelState.IsValid && tipoTecnologia != null)
            {
                var respuesta = _services.PutTipoTecnologia(id, tipoTecnologia);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteTipoTecnologia(int id)
        {

            var respuesta = _services.DeleteTipoTecnologia(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
    }
}
