﻿using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.API.Controllers
{
    [RoutePrefix("api/profile")]
    public class ProfileController : ApiController
    {
        DL_Profile _ProfileServices;
        public ProfileController()
        {
            _ProfileServices = new DL_Profile();
        }

        [Route("resume/{user}")]
        [HttpGet]
        public HttpResponseMessage GetUsuarioPerfil(string user)
        {
            var respuesta = _ProfileServices.GetDataProfile(user);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("userskill/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUsuarioConocimientos(int id)
        {
            var respuesta = _ProfileServices.GetUsuarioConocimiento(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
        [Route("userworkexperience/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUsuarioExperienciaLaboral(int id)
        {
            var respuesta = _ProfileServices.GetUsuarioExperienciaLaboral(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("usereducation/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUsuarioFormacionAcademica(int id)
        {
            var respuesta = _ProfileServices.GetUsuarioFormacionAcademica(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("userlenguages/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUsuarioIdiomas(int id)
        {
            var respuesta = _ProfileServices.GetUsuarioIdiomas(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
    }
}
