﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.API.Controllers
{
    [RoutePrefix("api/technicalskill")]
    public class ConocimientoTecnicoController : ApiController
    {
        DL_ConocimientosTecnicos _services = null;
        public ConocimientoTecnicoController()
        {
            _services = new DL_ConocimientosTecnicos();
        }

        [Route("all")]
        [HttpGet]
        public HttpResponseMessage GetConocimientosTecnicos()
        {
            var respuesta = _services.GetConocimientosTecnicos();
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetConocimientoTecnicoById(int id)
        {
            var respuesta = _services.GetConocimientoTecnicoById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage PostConocimientoTecnico([FromBody] ConocimientoTecnico conocimiento)
        {
            if (ModelState.IsValid && conocimiento != null)
            {
                var respuesta = _services.PostConocimientoTecnico(conocimiento);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutConocimientoTecnico(int id, [FromBody]ConocimientoTecnico conocimiento)
        {
            if (ModelState.IsValid && conocimiento != null)
            {
                var respuesta = _services.PutConocimientoTecnico(id, conocimiento);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteConocimientoTecnico(int id)
        {
            var respuesta = _services.DeleteConocimientoTecnicos(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
    }
}
