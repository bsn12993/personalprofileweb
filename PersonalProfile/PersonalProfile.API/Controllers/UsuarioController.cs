﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.API.Controllers
{
    [RoutePrefix("api/user")]
    public class UsuarioController : ApiController
    {
        DL_Usuario _services;

        public UsuarioController()
        {
            _services = new DL_Usuario();
        }

        #region Usuario
        [Route("all")]
        [HttpGet]
        public HttpResponseMessage GetUser()
        {
            var respuesta = _services.GetUsuarios();
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserById(int id)
        {
            var respuesta = _services.GetUsuarioById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("validate")]
        [HttpGet]
        public HttpResponseMessage GetUserValidate(string correo, string contrasenia)
        {
            var respuesta = _services.GetUsuarioByCorreoContrasenia(correo, contrasenia);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage PostUser([FromBody] Usuario usuario)
        {
            if (ModelState.IsValid && usuario != null)
            {
                var respuesta = _services.PostUsuario(usuario);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutUser(int id, [FromBody] Usuario usuario)
        {
            //var path = HttpContext.Current.Request.MapPath("~");
            //var a = FileUtil.ConvertBase64ToFile(usuario.imagen);
            //var Image = (Image)a.data;
            //Image.Save(HttpContext.Current.Server.MapPath("~/Images/" + usuario.nombre + "" + usuario.apellidoPaterno + "" + usuario.apellidoMaterno));
            if (ModelState.IsValid && usuario != null)
            {
                var respuesta = _services.PutUsuario(id, usuario);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteUser(int id)
        {
            var respuesta = _services.DeleteUsuario(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
        #endregion

        #region Usuario Conocimiento
        [Route("skills/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserSkills(int id)
        {
            var respuesta = _services.GetUsuarioConocimiento(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("skills/byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserSkillsById(int id)
        {
            var respuesta = _services.GetUsuarioConocimientoById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("skills/create")]
        [HttpPost]
        public HttpResponseMessage PostUserSkills([FromBody] UsuarioConocimiento usuarioConocimiento)
        {
            if (ModelState.IsValid && usuarioConocimiento != null)
            {
                var respuesta = _services.PostUsuarioConocimiento(usuarioConocimiento);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("skills/update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutUserSkills(int id, [FromBody] UsuarioConocimiento usuarioConocimiento)
        {
            if (ModelState.IsValid && usuarioConocimiento != null)
            {
                var respuesta = _services.PutUsuarioConocimiento(id, usuarioConocimiento);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("skills/delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteUserSkills(int id)
        {
            var respuesta = _services.DeleteUsuarioConocimiento(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
        #endregion

        #region Usuario Formacion 
        [Route("education/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserEducation(int id)
        {
            var respuesta = _services.GetUsuarioFormacionAcademica(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("education/byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserEducationById(int id)
        {
            var respuesta = _services.GetUsuarioFormacionAcademicaById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("education/create")]
        [HttpPost]
        public HttpResponseMessage PostUserEducation([FromBody] UsuarioFormacionAcademica usuarioFormacion)
        {
            if (ModelState.IsValid && usuarioFormacion != null)
            {
                var respuesta = _services.PostUsuarioFormacionAcademica(usuarioFormacion);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("education/update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutUserEducation(int id, [FromBody] UsuarioFormacionAcademica usuarioFormacion)
        {
            if (ModelState.IsValid && usuarioFormacion != null)
            {
                var respuesta = _services.PutUsuarioFormacionAcademica(id, usuarioFormacion);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("education/delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteUserEducation(int id)
        {
            var respuesta = _services.DeleteUsuarioFormacionAcademica(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
        #endregion

        #region Usuario Experiencia
        [Route("workexperiences/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserWorkExperiences(int id)
        {
            var respuesta = _services.GetUsuarioExperienciaLaboral(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("workexperiences/byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserWorkExperiencesById(int id)
        {
            var respuesta = _services.GetUsuarioExperienciaLaboralById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("workexperiences/create")]
        [HttpPost]
        public HttpResponseMessage PostUserWorkExperiences([FromBody] UsuarioExperienciaLaboral usuarioExperiencia)
        {
            if (ModelState.IsValid && usuarioExperiencia != null)
            {
                var respuesta = _services.PostUsuarioExperienciaLaboral(usuarioExperiencia);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("workexperiences/update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutUserWorkExperiences(int id, [FromBody] UsuarioExperienciaLaboral usuarioExperiencia)
        {
            if (ModelState.IsValid && usuarioExperiencia != null)
            {
                var respuesta = _services.PutUsuarioExperienciaLaboral(id, usuarioExperiencia);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("workexperiences/delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteUserWorkExperiences(int id)
        {
            var respuesta = _services.DeleteUsuarioExperienciaLaboral(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
        #endregion

        #region Usuario Idioma
        [Route("languages/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserLanguages(int id)
        {
            var respuesta = _services.GetUsuarioIdiomas(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("languages/byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserLanguagesById(int id)
        {
            var respuesta = _services.GetUsuarioIdiomasById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("languages/create")]
        [HttpPost]
        public HttpResponseMessage PostUserLanguages([FromBody] UsuarioIdioma usuarioIdioma)
        {
            if (ModelState.IsValid && usuarioIdioma != null)
            {
                var respuesta = _services.PostUsuarioIdiomas(usuarioIdioma);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("languages/update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutUserLanguages(int id, [FromBody] UsuarioIdioma usuarioIdioma)
        {
            if (ModelState.IsValid && usuarioIdioma != null)
            {
                var respuesta = _services.PutUsuarioIdiomas(id, usuarioIdioma);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("languages/delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteUserLanguages(int id)
        {
            var respuesta = _services.DeleteUsuarioIdiomas(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
        #endregion

        #region Usuario Proyectos
        [Route("projects/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserProjects(int id)
        {
            var respuesta = _services.GetUsuarioProyectos(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("projects/byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserProjectsById(int id)
        {
            var respuesta = _services.GetUsuarioProyectosById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("projects/create")]
        [HttpPost]
        public HttpResponseMessage PostUserProjects([FromBody] UsuarioProyectos usuarioProyecto)
        {
            if (ModelState.IsValid && usuarioProyecto != null)
            {
                var respuesta = _services.PostUsuarioProyectos(usuarioProyecto);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("projects/update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutUserProjects(int id, [FromBody] UsuarioProyectos usuarioProyecto)
        {
            if (ModelState.IsValid && usuarioProyecto != null)
            {
                var respuesta = _services.PutUsuarioProyectos(id, usuarioProyecto);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("projects/delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteUserProjects(int id)
        {
            var respuesta = _services.DeleteUsuarioProyectos(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
        #endregion

        #region Usuario Contacto
        [Route("contact/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserContact(int id)
        {
            var respuesta = _services.GetUsuarioContacto(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("contact/byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetUserContactById(int id)
        {
            var respuesta = _services.GetUsuarioContactoById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("contact/create")]
        [HttpPost]
        public HttpResponseMessage PostUserContact([FromBody] UsuarioContacto usuarioContacto)
        {
            if (ModelState.IsValid && usuarioContacto != null)
            {
                var respuesta = _services.PostUsuarioContacto(usuarioContacto);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("contact/update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutUserContact(int id, [FromBody] UsuarioContacto usuarioContacto)
        {
            if (ModelState.IsValid && usuarioContacto != null)
            {
                var respuesta = _services.PutUsuarioContacto(id, usuarioContacto);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("contact/delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteUserContact(int id)
        {
            var respuesta = _services.DeleteUsuarioContacto(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
        #endregion
    }
}
