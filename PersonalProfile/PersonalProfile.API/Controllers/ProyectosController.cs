﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.API.Controllers
{
    [RoutePrefix("api/projects")]
    public class ProyectosController : ApiController
    {
        DL_Proyectos _services = null;
        public ProyectosController()
        {
            _services = new DL_Proyectos();
        }

        [Route("all")]
        [HttpGet]
        public HttpResponseMessage GetProyectos()
        {
            var respuesta = _services.GetProyectos();
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetProyectoById(int id)
        {
            var respuesta = _services.GetProyectosById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage PostProyecto([FromBody] Proyectos proyecto)
        {
            if (ModelState.IsValid && proyecto != null)
            {
                var respuesta = _services.PostProyectos(proyecto);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutProyecto(int id, [FromBody] Proyectos proyecto)
        {
            if (ModelState.IsValid && proyecto != null)
            {
                var respuesta = _services.PutProyecto(id, proyecto);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteProyecto(int id)
        {
            var respuesta = _services.DeleteProyecto(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("technologies/byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetProyectosTecnologiaById(int id)
        {
            var respuesta = _services.GetTecnologiaById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("technologies/byproject/{id}")]
        [HttpGet]
        public HttpResponseMessage GetProyectosTecnologia(int id)
        {
            var respuesta = _services.GetTecnologiaByProject(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("technologies/create")]
        [HttpPost]
        public HttpResponseMessage PostProyectosTecnologia([FromBody] ProyectoTecnologia proyectoTecnologia)
        {
            if (ModelState.IsValid && proyectoTecnologia != null)
            {
                var respuesta = _services.PostProyectoTecnologias(proyectoTecnologia);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("technologies/update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutProyectosTecnologia(int id, [FromBody] ProyectoTecnologia proyectoTecnologia)
        {
            if (ModelState.IsValid && proyectoTecnologia != null)
            {
                var respuesta = _services.PutProyectoTecnologias(id, proyectoTecnologia);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Response
                {
                    Message = "Los datos no son validos"
                }.Message, "application/json");
        }

        [Route("technologies/delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteProyectosTecnologia(int id)
        {
            var respuesta = _services.DeleteProyectoTecnologia(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
    }
}
