﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Linq;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_ConocimientosTecnicos
    {
        ProfileContext context = null;
        Response respuesta = null;

        public DL_ConocimientosTecnicos()
        {
            context = new ProfileContext();
        }
        /// <summary>
        /// Get Conocimientos Tecnicos
        /// </summary>
        /// <returns>Lista de Conocimientos Tecnicos</returns>
        public Response GetConocimientosTecnicos()
        {
            respuesta = new Response();
            try
            {
                var lstConocimientoTec = context.ConocimientoTecnico.Include("TipoTecnologia").ToList();
                if (lstConocimientoTec.Count > 0) 
                {
                    respuesta.Result = lstConocimientoTec;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontraron resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
                respuesta.Result = null;
            }
            return respuesta;
        }
        /// <summary>
        /// Get Conocimiento por Id
        /// </summary>
        /// <param name="id">Id del conocimiento tecnico</param>
        /// <returns></returns>
        public Response GetConocimientoTecnicoById(int id)
        {
            respuesta = new Response();
            try
            {
                var conocimientoTec = context.ConocimientoTecnico.Where(x => x.ConocimientoTecnicoID == id).SingleOrDefault();
                if (conocimientoTec != null)
                {
                    respuesta.Result = conocimientoTec;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontraron resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
            }
            return respuesta;
        }

        public Response PostConocimientoTecnico(ConocimientoTecnico conocimiento)
        {
            respuesta = new Response();
            try
            {
                var count = context.ConocimientoTecnico.Where(x => x.nombre.ToLower().Equals(conocimiento.nombre.ToLower()) &&
                      x.TipoTecnologiaID == conocimiento.TipoTecnologiaID).ToList();
                if (count.Count >= 1)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe un registro con el mismo nombre y tecnologia";
                    return respuesta;
                }

                var count_exist = context.ConocimientoTecnico.Where(x => x.nombre.ToLower().Equals(conocimiento.nombre.ToLower())).ToList();
                if (count_exist.Count > 0)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe un registro con el mismo nombre";
                    return respuesta;
                }

                var exist = context.ConocimientoTecnico.Where(x => x.nombre.Equals(conocimiento.nombre)).SingleOrDefault();
                if (exist == null)
                {
                    context.ConocimientoTecnico.Add(conocimiento);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha guardado el registro";
                }
                else
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Ya existe el registro " + conocimiento.nombre;
                }
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response PutConocimientoTecnico(int id,ConocimientoTecnico conocimiento)
        {
            respuesta = new Response();
            try
            {
                var count = context.ConocimientoTecnico.Where(x => x.nombre.ToLower().Equals(conocimiento.nombre.ToLower()) &&
                      x.TipoTecnologiaID == conocimiento.TipoTecnologiaID).ToList();
                if (count.Count > 1)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe un registro con el mismo nombre y tecnologia";
                    return respuesta;
                }


                var con = context.ConocimientoTecnico.Where(x => x.ConocimientoTecnicoID == conocimiento.ConocimientoTecnicoID).SingleOrDefault().nombre;
                if (!con.ToLower().Equals(conocimiento.nombre.ToLower()))
                {
                    var count_exist = context.ConocimientoTecnico.Where(x => x.nombre.ToLower().Equals(conocimiento.nombre.ToLower())).ToList();
                    if (count_exist.Count > 0)
                    {
                        respuesta.IsSuccess = false;
                        respuesta.Message = "Ya existe un registro con el mismo nombre";
                        return respuesta;
                    }
                }
                 


                var target = context.ConocimientoTecnico.Where(x => x.ConocimientoTecnicoID == id).SingleOrDefault();
                if (target != null)
                {
                    target.nombre = conocimiento.nombre;
                    target.TipoTecnologiaID = conocimiento.TipoTecnologiaID;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha actualizado el registro";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response DeleteConocimientoTecnicos(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.ConocimientoTecnico.Where(x => x.ConocimientoTecnicoID == id).SingleOrDefault();
                if (target != null)
                {
                    context.ConocimientoTecnico.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha eliminado el registro";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
    }
}
