﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_TipoTecnologia
    {
        ProfileContext context = null;
        Response respuesta = null;

        public DL_TipoTecnologia()
        {
            context = new ProfileContext();
        }

        public Response GetTipoTecnologia()
        {
            respuesta = new Response();
            try
            {
                var lstTipoTecnologia = context.TipoTecnologia.ToList();
                if (lstTipoTecnologia.Count > 0)
                {
                    respuesta.Result = lstTipoTecnologia;
                    respuesta.Message = "Se encontró información";
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontró información");
            }
            catch(Exception e)
            {
                respuesta.Result = null;
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
            }
            return respuesta;
        }

        public Response GetTipoTecnologiaById(int id)
        {
            respuesta = new Response();
            try
            {
                var TipoTecnologia = context.TipoTecnologia.Where(x => x.TipoTecnologiaID == id).SingleOrDefault();
                if (TipoTecnologia != null)
                {
                    respuesta.Result = TipoTecnologia;
                    respuesta.Message = "Se encontró información";
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontró información");
            }
            catch (Exception e)
            {
                respuesta.Result = null;
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
            }
            return respuesta;
        }

        public Response PostTipoTecnologia(TipoTecnologia tipoTecnologia)
        {
            respuesta = new Response();
            try
            {
                var exist = context.TipoTecnologia.Where(x => x.nombre.Equals(tipoTecnologia.nombre)).SingleOrDefault();
                if (exist == null)
                {
                    context.TipoTecnologia.Add(tipoTecnologia);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha registrado";
                }
                else
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Ya existe el registro " + tipoTecnologia.nombre;
                }
            }
            catch(Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
                respuesta.Result = null;
            }
            return respuesta;
        }

        public Response PutTipoTecnologia(int id, TipoTecnologia tipoTecnologia)
        {
            respuesta = new Response();
            try
            {
                var count = context.TipoTecnologia.Where(x => x.nombre.ToLower().Equals(tipoTecnologia.nombre.ToLower())).ToList();
                if (count.Count >= 1)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe registro con el mismo nombre";
                    return respuesta;
                }

                var target = context.TipoTecnologia.Where(x => x.TipoTecnologiaID == id).SingleOrDefault();
                if (target != null)
                {
                    target.nombre = tipoTecnologia.nombre;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha actualizado el registro";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
                respuesta.Result = null;
            }
            return respuesta;
        }

        public Response DeleteTipoTecnologia(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.TipoTecnologia.Where(x => x.TipoTecnologiaID == id).SingleOrDefault();
                if (target != null)
                {
                    context.TipoTecnologia.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha eliminado el registro";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
                respuesta.Result = null;
            }
            return respuesta;
        }
    }
}
