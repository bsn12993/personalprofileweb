﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_Contacto
    {
        ProfileContext context = null;
        Response respuesta = null;
        public DL_Contacto()
        {
            context = new ProfileContext();
        }

        public Response GetContacto()
        {
            respuesta = new Response();
            try
            {
                var lstContacto = context.Contacto.ToList();
                if (lstContacto.Count > 0) 
                {
                    respuesta.Result = lstContacto;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontraron resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
                respuesta.Result = null;
            }
            return respuesta;
        }
        /// <summary>
        /// Get Conocimiento por Id
        /// </summary>
        /// <param name="id">Id del conocimiento tecnico</param>
        /// <returns></returns>
        public Response GetContactoById(int id)
        {
            respuesta = new Response();
            try
            {
                var contacto = context.Contacto.Where(x => x.ContactoID == id).SingleOrDefault();
                if (contacto != null)
                {
                    respuesta.Result = contacto;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontraron resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
            }
            return respuesta;
        }

        public Response PostContacto(Contacto contacto)
        {
            respuesta = new Response();
            try
            {
                context.Contacto.Add(contacto);
                context.SaveChanges();
                respuesta.IsSuccess = true;
                respuesta.Message = "Se ha guardado el registro";
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response PutContacto(int id, Contacto contacto)
        {
            respuesta = new Response();
            try
            {
                var target = context.Contacto.Where(x => x.ContactoID == id).SingleOrDefault();
                if (target != null)
                {
                    target.nombre = contacto.nombre;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha actualizado el registro";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response DeleteContacto(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.Contacto.Where(x => x.ContactoID == id).SingleOrDefault();
                if (target != null)
                {
                    context.Contacto.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha eliminado el registro";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

    }
}
