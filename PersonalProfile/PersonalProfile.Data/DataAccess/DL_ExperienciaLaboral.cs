﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Linq;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_ExperienciaLaboral
    {
        ProfileContext context = null;
        Response respuesta = null;

        public DL_ExperienciaLaboral()
        {
            context = new ProfileContext();
        }

        public Response GetExperienciaLaboral()
        {
            respuesta = new Response();
            try
            {
                var lstExperienciaLaboral = context.ExperienciaLaboral.ToList();
                if (lstExperienciaLaboral.Count > 0) 
                {
                    respuesta.Result = lstExperienciaLaboral;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                ////LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response GetExperienciaLaboralById(int id)
        {
            respuesta = new Response();
            try
            {
                var experienciaLaboral = context.ExperienciaLaboral.Where(x => x.ExperienciaLaboralID == id).SingleOrDefault();
                if (experienciaLaboral != null)
                {
                    respuesta.Result = experienciaLaboral;
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
            }
            return respuesta;
        }

        public Response PostExperienciaLaborial(ExperienciaLaboral experiencia)
        {
            respuesta = new Response();
            try
            {
                var exist = context.ExperienciaLaboral.Where(x => x.nombreEmpresa.Equals(experiencia.nombreEmpresa)).SingleOrDefault();
                if (exist == null)
                {
                    context.ExperienciaLaboral.Add(experiencia);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha guardado el registro";
                }
                else
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Ya existe el registro " + experiencia.nombreEmpresa;
                }
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response PutExperienciaLaboral(int id,ExperienciaLaboral experiencia)
        {
            respuesta = new Response();
            try
            {
                var target = context.ExperienciaLaboral.Where(x => x.ExperienciaLaboralID == id).SingleOrDefault();
                if (target != null)
                {
                    target.actividad = experiencia.actividad;
                    target.fechaIngreso = experiencia.fechaIngreso;
                    target.fechaSalida = experiencia.fechaSalida;
                    target.nombreEmpresa = experiencia.nombreEmpresa;
                    target.nombrePuesto = experiencia.nombrePuesto;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response DeleteExperienciaLaboral(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.ExperienciaLaboral.Where(x => x.ExperienciaLaboralID == id).SingleOrDefault();
                if (target != null)
                {
                    context.ExperienciaLaboral.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
    }
}
