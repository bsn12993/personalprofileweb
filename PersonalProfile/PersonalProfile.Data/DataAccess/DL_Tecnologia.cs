﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_Tecnologia
    {
        ProfileContext context = null;
        Response respuesta = null;
        public DL_Tecnologia()
        {
            context = new ProfileContext();
        }

        public Response GetTecnologias()
        {
            respuesta = new Response();
            try
            {
                var lstTecnologia = context.TipoTecnologia.ToList();
                if (lstTecnologia.Count > 0) 
                {
                    respuesta.Result = lstTecnologia;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
                respuesta.Result = null;
            }
            return respuesta;
        }

        public Response GetTecnologiasById(int id)
        {
            respuesta = new Response();
            try
            {
                var tecnologia = context.TipoTecnologia.Where(x => x.TipoTecnologiaID == id).SingleOrDefault();
                if (tecnologia != null)
                {
                    respuesta.Result = tecnologia;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
                respuesta.Result = null;
            }
            return respuesta;
        }

        public Response PostTipoTecnologia(TipoTecnologia tipoTecnologia)
        {
            respuesta = new Response();
            try
            {
                var exist = context.TipoTecnologia.Where(x => x.nombre.Equals(tipoTecnologia.nombre)).SingleOrDefault();
                if (exist == null)
                {
                    context.TipoTecnologia.Add(tipoTecnologia);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha guardado el registro";
                }
                else
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Ya existe el registro " + tipoTecnologia.nombre;
                }
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response PutTecnologia(int id, TipoTecnologia tipoTecnologia)
        {
            respuesta = new Response();
            try
            {
                var target = context.TipoTecnologia.Where(x => x.TipoTecnologiaID == id).SingleOrDefault();
                if (target != null)
                {
                    target.nombre = tipoTecnologia.nombre;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha actualizado el registro";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response DeleteTecnologia(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.TipoTecnologia.Where(x => x.TipoTecnologiaID == id).SingleOrDefault();
                if (target != null)
                {
                    context.TipoTecnologia.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha eliminado el registro";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
    }
}
