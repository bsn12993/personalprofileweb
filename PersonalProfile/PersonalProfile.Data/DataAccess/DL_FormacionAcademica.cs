﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Linq;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_FormacionAcademica
    {
        ProfileContext context = null;
        Response respuesta = null;

        public DL_FormacionAcademica()
        {
            context = new ProfileContext();
        }

        public Response GetFormacionAcademica()
        {
            respuesta = new Response();
            try
            {
                var lstFormacionAcademica = context.FormacionAcademica.ToList();
                if (lstFormacionAcademica.Count > 0) 
                {
                    respuesta.Result = lstFormacionAcademica;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response GetFormacionAcademicaById(int id)
        {
            respuesta = new Response();
            try
            {
                var formacionAcademica = context.FormacionAcademica.Where(x => x.FormacionAcademicaID == id).SingleOrDefault();
                if (formacionAcademica != null)
                {
                    respuesta.Result = formacionAcademica;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response PostFormacionAcademica(FormacionAcademica formacionAcademica)
        {
            respuesta = new Response();
            try
            {
                var exist = context.FormacionAcademica.Where(x => x.instituto.Equals(formacionAcademica.instituto)).SingleOrDefault();
                if (exist == null)
                {
                    context.FormacionAcademica.Add(formacionAcademica);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                }
                else
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Ya existe el registro " + formacionAcademica.instituto;
                }
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response PutFormacionAcademica(int id,FormacionAcademica formacionAcademica)
        {
            respuesta = new Response();
            try
            {
                var target = context.FormacionAcademica.Where(x => x.FormacionAcademicaID == id).SingleOrDefault();
                if (target != null)
                {
                    target.anioIngreso = formacionAcademica.anioIngreso;
                    target.anioSalida = formacionAcademica.anioSalida;
                    target.carrera = formacionAcademica.carrera;
                    target.instituto = formacionAcademica.instituto;
                    target.titulo = formacionAcademica.titulo;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response DeleteFormacionAcademica(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.FormacionAcademica.Where(x => x.FormacionAcademicaID == id).SingleOrDefault();
                if (target != null)
                {
                    context.FormacionAcademica.Remove(target);
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
    }
}
