﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.Domain;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_Proyectos
    {
        ProfileContext context = null;
        Response respuesta = null;

        public DL_Proyectos()
        {
            context = new ProfileContext();
        }

        #region Proyectos
        public Response GetProyectos()
        {
            respuesta = new Response();
            try
            {
                var lstProyectos = context.Proyectos.ToList();
                if (lstProyectos.Count > 0) 
                {
                    respuesta.Result = lstProyectos;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetProyectosById(int id)
        {
            respuesta = new Response();
            try
            {
                var proyecto = context.Proyectos.Where(x => x.ProyectosID == id).SingleOrDefault();
                if (proyecto != null)
                {
                    respuesta.Result = proyecto;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PostProyectos(Proyectos proyecto)
        {
            respuesta = new Response();
            try
            {
                var exist = context.Proyectos.Where(x => x.nombre.Equals(proyecto.nombre)).SingleOrDefault();
                if (exist == null)
                {
                    context.Proyectos.Add(proyecto);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                }
                else
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Ya existe el registro " + proyecto.nombre;
                }
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PutProyecto(int id,Proyectos proyecto)
        {
            respuesta = new Response();
            try
            {
                var target = context.Proyectos.Where(x => x.ProyectosID == id).SingleOrDefault();
                if (target != null)
                {
                    target.nombre = proyecto.nombre;
                    target.descripcionProyecto = proyecto.descripcionProyecto;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response DeleteProyecto(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.Proyectos.Where(x => x.ProyectosID == id).SingleOrDefault();
                if (target != null)
                {
                    context.Proyectos.Remove(target);
                    context.SaveChanges();
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        #endregion

        #region Proyecto Tecnologia
        public Response GetProyectoTecnologias()
        {
            respuesta = new Response();
            List<ProyectosDto> proyectos = null;
            List<TipoTecnologiaDto> lstTipoTecnologia = null;
            TipoTecnologiaDto tipotecnologia = null;
            try
            {
                var lstProyecto = context.Proyectos.ToList();
                if (lstProyecto != null)
                {
                    proyectos = new List<ProyectosDto>();
                    foreach (var p in lstProyecto)
                    {
                        var lstTecnologia = context.ProyectoTecnologia.Include("ConocimientoTecnico").ToList();
                        if (lstTecnologia != null)
                        {
                            lstTipoTecnologia = new List<TipoTecnologiaDto>();
                            foreach (var t in lstTecnologia)
                            {
                                tipotecnologia = new TipoTecnologiaDto();
                                tipotecnologia.id = t.ConocimientoTecnicoID;
                                tipotecnologia.nombre = t.conocimientoTecnico.nombre;
                                lstTipoTecnologia.Add(tipotecnologia);
                            }
                        }
                        proyectos.Add(new ProyectosDto
                        {
                            pro_idproyecto = p.ProyectosID,
                            pro_nombre = p.nombre,
                            pro_descripcionproyecto = p.descripcionProyecto,
                            tipo_tecnologia = lstTipoTecnologia
                        });
                    }
                    respuesta.Result = proyectos;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetTecnologiaById(int id)
        {
            respuesta = new Response();
            try
            {
                var data = context.ProyectoTecnologia.Where(x => x.ProyectoTecnologiaID == id).SingleOrDefault();
                if (data != null)
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se econtrarón registros";
                    respuesta.Result = data;
                }
                else
                    throw new Exception("No se encontrarón registros");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
                respuesta.Result = null;
            }
            return respuesta;
        }

        public Response GetTecnologiaByProject(int id)
        {
            respuesta = new Response();
            try
            {
                var data = context.ProyectoTecnologia.Include("ConocimientoTecnico").Where(x => x.ProyectosID == id).ToList();
                if (data != null && data.Count > 0) 
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se econtrarón registros";
                    respuesta.Result = data;
                }
                else
                    throw new Exception("No se encontrarón registros");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
                respuesta.Result = null;
            }
            return respuesta;
        }
        public Response GetProyectoTecnologiasById(int id)
        {
            respuesta = new Response();
            try
            {
                var data = context.ProyectoTecnologia.Include("Proyectos").Include("ConocimientoTecnico").Where(x => x.ProyectosID == id).ToList();
                if (data != null)
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se econtrarón registros";
                    respuesta.Result = data;
                }
                else
                    throw new Exception("No se encontrarón registros");
            }
            catch(Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
                respuesta.Result = null;
            }
            return respuesta;
        }
        public Response PostProyectoTecnologias(ProyectoTecnologia proyectoTecnologia)
        {
            respuesta = new Response();
            try
            {
                context.ProyectoTecnologia.Add(proyectoTecnologia);
                context.SaveChanges();
                respuesta.IsSuccess = true;
                respuesta.Result = null;
                respuesta.Message = "Se ha registrado una tecnologia en el proyecto";
            }
            catch(Exception e)
            {
                respuesta.Message = e.Message;
                respuesta.Result = null;
                respuesta.IsSuccess = false;
            }
            return respuesta;
        }
        public Response PutProyectoTecnologias(int id,ProyectoTecnologia proyectoTecnologia)
        {
            respuesta = new Response();
            try
            {
                var target = context.ProyectoTecnologia.Where(x => x.ProyectoTecnologiaID == id).SingleOrDefault();
                if (target != null)
                {
                    target.ConocimientoTecnicoID = proyectoTecnologia.ConocimientoTecnicoID;
                    target.ProyectosID = proyectoTecnologia.ProyectosID;
                    context.SaveChanges();
                    respuesta.Result = null;
                    respuesta.Message = "Se ha editado la tecnologia del proyecto";
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontrarón registro");
            }
            catch(Exception e)
            {
                respuesta.Result = null;
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
            }
            return respuesta;
        }
        public Response DeleteProyectoTecnologia(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.ProyectoTecnologia.Where(x => x.ProyectoTecnologiaID == id).SingleOrDefault();
                if (target != null)
                {
                    context.ProyectoTecnologia.Remove(target);
                    context.SaveChanges();
                    respuesta.Result = null;
                    respuesta.Message = "Se ha eliminado la tecnologia del proyecto";
                    respuesta.IsSuccess = true;
                }
            }
            catch(Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
                respuesta.Result = null;
            }
            return respuesta;
        }
        #endregion
    }
}
