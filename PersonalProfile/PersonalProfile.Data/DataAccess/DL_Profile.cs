﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.Domain;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_Profile
    {
        ProfileContext context = null;
        UsuarioDto usuario = null;
        Response respuesta = null;
        List<ProyectosDto> proyectos = null;
        

        public DL_Profile()
        {
            context = new ProfileContext();
        }

        public Response GetDataProfile(string u)
        {
            respuesta = new Response();
            usuario = new UsuarioDto();
            string email = Core.Util.HashHelper.Base64Decode(u);
            try
            {
                var user = context.Usuario.Where(x => x.correo == email).SingleOrDefault();
                if (user != null)
                {
                    usuario.usu_idusuario = user.UsuarioID;
                    usuario.usu_imagen = (!string.IsNullOrEmpty(user.imagen)) ? user.imagen : "";
                    usuario.usu_nombre = (!string.IsNullOrEmpty(user.nombre)) ? user.nombre : "";
                    usuario.usu_apellidopaterno = (!string.IsNullOrEmpty(user.apellidoPaterno)) ? user.apellidoPaterno : "";
                    usuario.usu_apellidomaterno = (!string.IsNullOrEmpty(user.apellidoMaterno)) ? user.apellidoMaterno : "";
                    usuario.usu_correo = (!string.IsNullOrEmpty(user.correo)) ? user.correo : "";
                    usuario.usu_perfil = (!string.IsNullOrEmpty(user.perfil)) ? user.perfil : "";
                    usuario.usu_telefono = (!string.IsNullOrEmpty(user.telefono)) ? user.telefono : "";
                    usuario.usu_fechanacimiento = (user.fechaNacimiento.HasValue)
                        ? user.fechaNacimiento.Value.ToString("dd/MM/yyyy") : "";


                    var skills = context.UsuarioConocimiento.Include("ConocimientoTecnico").Where(x => x.usuarioID == usuario.usu_idusuario).ToList();
                    if (skills.Count > 0 && skills != null)
                    {
                        foreach (var skill in skills)
                        {
                            usuario.conocimiento.Add(new ConocimientoTecnicoDto
                            {
                                rel_nivel = skill.nivel,
                                tct_nombre = (!string.IsNullOrEmpty(skill.conocimientoTecnico.nombre)) ? skill.conocimientoTecnico.nombre : "",
                                tipo_tecnologia = skill.conocimientoTecnico.TipoTecnologiaID
                            });
                        }
                    }

                    var workexperience = context.UsuarioExperienciaLaboral.Include("ExperienciaLaboral").Where(x => x.usuarioID == usuario.usu_idusuario).ToList();
                    if (workexperience.Count > 0 && workexperience != null)
                    {
                        foreach (var experience in workexperience)
                        {
                            usuario.experienciaLaboral.Add(new ExperienciaLaboralDto
                            {
                                ela_nombrepuesto = (!string.IsNullOrEmpty(experience.experienciaLaboral.nombrePuesto)) ? experience.experienciaLaboral.nombrePuesto : "",
                                ela_nombreempresa = (!string.IsNullOrEmpty(experience.experienciaLaboral.nombreEmpresa)) ? experience.experienciaLaboral.nombreEmpresa : "",
                                ela_actividad = (!string.IsNullOrEmpty(experience.experienciaLaboral.actividad)) ? experience.experienciaLaboral.actividad : "",
                                ela_fechaingresao = experience.experienciaLaboral.fechaIngreso,
                                ela_fechasalida = experience.experienciaLaboral.fechaSalida,
                                actual = experience.experienciaLaboral.actual,
                                proyectos = GetProyectosExperienciaLaboral(experience.experienciaLaboralID)
                            });
                        }
                    }

                    var academic = context.UsuarioFormacionAcademica.Include("FormacionAcademica").Where(x => x.usuarioID == usuario.usu_idusuario).ToList();
                    if (academic != null)
                    {
                        foreach (var aca in academic)
                        {
                            usuario.formacionAcademica.Add(new FormacionAcademicaDto
                            {
                                fac_anioingresao = aca.formacionAcademica.anioIngreso,
                                fac_aniofinalizo = aca.formacionAcademica.anioSalida,
                                fac_carrera = (!string.IsNullOrEmpty(aca.formacionAcademica.carrera)) ? aca.formacionAcademica.carrera : "",
                                fac_institucion = (!string.IsNullOrEmpty(aca.formacionAcademica.instituto)) ? aca.formacionAcademica.instituto : "",
                                fac_titulo = aca.formacionAcademica.titulo
                            });
                        }
                    }

                    var projects = context.UsuarioProyectos.Include("Proyectos").Where(x => x.usuarioID == usuario.usu_idusuario).ToList();
                    if (projects != null)
                    {
                        foreach (var p in projects)
                        {
                            usuario.proyectos.Add(new ProyectosDto
                            {
                                pro_nombre = (!string.IsNullOrEmpty(p.proyectos.nombre)) ? p.proyectos.nombre : "",
                                pro_descripcionproyecto = (!string.IsNullOrEmpty(p.proyectos.descripcionProyecto)) ? p.proyectos.descripcionProyecto : ""
                                
                            });
                        }
                    }

                    var idioma = context.UsuarioIdioma.Include("Idiomas").Where(x => x.usuarioID == usuario.usu_idusuario).ToList();
                    if (idioma != null)
                    {
                        foreach (var i in idioma)
                        {
                            usuario.idiomas.Add(new IdiomasDto
                            {
                                idi_nombre = (!string.IsNullOrEmpty(i.idiomas.nombre)) ? i.idiomas.nombre : ""
                            });
                        }
                    }

                    respuesta.Result = usuario;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response GetUsuarioConocimiento(int id)
        {
            usuario = new UsuarioDto();
            try
            {
                var user = context.Usuario.Where(x => x.UsuarioID == id).SingleOrDefault();
                if (user != null)
                {
                    usuario.usu_idusuario = user.UsuarioID;
                    usuario.usu_nombre = (!string.IsNullOrEmpty(user.nombre)) ? user.nombre : "";
                    usuario.usu_apellidopaterno = (!string.IsNullOrEmpty(user.apellidoPaterno)) ? user.apellidoPaterno : "";
                    usuario.usu_apellidomaterno = (!string.IsNullOrEmpty(user.apellidoMaterno)) ? user.apellidoMaterno : "";
                    usuario.usu_correo = (!string.IsNullOrEmpty(user.correo)) ? user.correo : "";
                    usuario.usu_perfil = (!string.IsNullOrEmpty(user.perfil)) ? user.perfil : "";
                    usuario.usu_telefono = (!string.IsNullOrEmpty(user.telefono)) ? user.telefono : "";
                    usuario.usu_fechanacimiento = (user.fechaNacimiento.HasValue)
                        ? user.fechaNacimiento.Value.ToString("dd/MM/yyyy") : string.Empty;

                    var userSkills = context.UsuarioConocimiento.Include("ConocimientoTecnico").Where(x => x.usuarioID == usuario.usu_idusuario).ToList();
                    if (userSkills != null)
                    {
                        foreach (var skill in userSkills)
                        {
                            usuario.conocimiento.Add(new ConocimientoTecnicoDto
                            {
                                tct_nombre = (!string.IsNullOrEmpty(skill.conocimientoTecnico.nombre)) ? skill.conocimientoTecnico.nombre : "",
                                rel_nivel = skill.nivel
                            });
                        }
                    }
                    respuesta.Result = usuario;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response GetUsuarioExperienciaLaboral(int id)
        {
            usuario = new UsuarioDto();
            try
            {
                var user = context.Usuario.Where(x => x.UsuarioID == id).SingleOrDefault();
                if (user != null)
                {
                    usuario.usu_idusuario = user.UsuarioID;
                    usuario.usu_nombre = (!string.IsNullOrEmpty(user.nombre)) ? user.nombre : "";
                    usuario.usu_apellidopaterno = (!string.IsNullOrEmpty(user.apellidoPaterno)) ? user.apellidoPaterno : "";
                    usuario.usu_apellidomaterno = (!string.IsNullOrEmpty(user.apellidoMaterno)) ? user.apellidoMaterno : "";
                    usuario.usu_correo = (!string.IsNullOrEmpty(user.correo)) ? user.correo : "";
                    usuario.usu_perfil = (!string.IsNullOrEmpty(user.perfil)) ? user.perfil : "";
                    usuario.usu_telefono = (!string.IsNullOrEmpty(user.telefono)) ? user.telefono : "";
                    usuario.usu_fechanacimiento = (user.fechaNacimiento.HasValue)
                        ? user.fechaNacimiento.Value.ToString("dd/MM/yyyy") : string.Empty;

                    var userWorks = context.UsuarioExperienciaLaboral.Include("ExperienciaLaboral").Where(x => x.usuarioID == usuario.usu_idusuario).ToList();
                    if (userWorks != null)
                    {
                        foreach (var works in userWorks)
                        {
                            usuario.experienciaLaboral.Add(new ExperienciaLaboralDto
                            {
                                ela_actividad = works.experienciaLaboral.actividad,
                                ela_fechaingresao = works.experienciaLaboral.fechaIngreso,
                                ela_fechasalida = works.experienciaLaboral.fechaSalida,
                                ela_nombreempresa = works.experienciaLaboral.nombreEmpresa,
                                ela_nombrepuesto = works.experienciaLaboral.nombrePuesto
                            });
                        }
                    }
                    respuesta.Result = usuario;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response GetUsuarioFormacionAcademica(int id)
        {
            usuario = new UsuarioDto();
            try
            {
                var user = context.Usuario.Where(x => x.UsuarioID == id).SingleOrDefault();
                if (user != null)
                {
                    usuario.usu_idusuario = user.UsuarioID;
                    usuario.usu_nombre = (!string.IsNullOrEmpty(user.nombre)) ? user.nombre : "";
                    usuario.usu_apellidopaterno = (!string.IsNullOrEmpty(user.apellidoPaterno)) ? user.apellidoPaterno : "";
                    usuario.usu_apellidomaterno = (!string.IsNullOrEmpty(user.apellidoMaterno)) ? user.apellidoMaterno : "";
                    usuario.usu_correo = (!string.IsNullOrEmpty(user.correo)) ? user.correo : "";
                    usuario.usu_perfil = (!string.IsNullOrEmpty(user.perfil)) ? user.perfil : "";
                    usuario.usu_telefono = (!string.IsNullOrEmpty(user.telefono)) ? user.telefono : "";
                    usuario.usu_fechanacimiento = (user.fechaNacimiento.HasValue)
                        ? user.fechaNacimiento.Value.ToString("dd/MM/yyyy") : string.Empty;

                    var userEducations = context.UsuarioFormacionAcademica.Include("FormacionAcademica").Where(x => x.usuarioID == usuario.usu_idusuario).ToList();
                    if (userEducations != null)
                    {
                        foreach (var education in userEducations)
                        {
                            usuario.formacionAcademica.Add(new FormacionAcademicaDto
                            {
                                fac_aniofinalizo = education.formacionAcademica.anioSalida,
                                fac_anioingresao = education.formacionAcademica.anioIngreso,
                                fac_carrera = education.formacionAcademica.carrera,
                                fac_institucion = education.formacionAcademica.instituto,
                                fac_titulo = education.formacionAcademica.titulo
                            });
                        }
                    }
                    respuesta.Result = usuario;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response GetUsuarioIdiomas(int id)
        {
            usuario = new UsuarioDto();
            try
            {
                var user = context.Usuario.Where(x => x.UsuarioID == id).SingleOrDefault();
                if (user != null)
                {
                    usuario.usu_idusuario = user.UsuarioID;
                    usuario.usu_nombre = (!string.IsNullOrEmpty(user.nombre)) ? user.nombre : "";
                    usuario.usu_apellidopaterno = (!string.IsNullOrEmpty(user.apellidoPaterno)) ? user.apellidoPaterno : "";
                    usuario.usu_apellidomaterno = (!string.IsNullOrEmpty(user.apellidoMaterno)) ? user.apellidoMaterno : "";
                    usuario.usu_correo = (!string.IsNullOrEmpty(user.correo)) ? user.correo : "";
                    usuario.usu_perfil = (!string.IsNullOrEmpty(user.perfil)) ? user.perfil : "";
                    usuario.usu_telefono = (!string.IsNullOrEmpty(user.telefono)) ? user.telefono : "";
                    usuario.usu_fechanacimiento = (user.fechaNacimiento.HasValue)
                        ? user.fechaNacimiento.Value.ToString("dd/MM/yyyy") : string.Empty;

                    var userLanguages = context.UsuarioIdioma.Include("Idiomas").Where(x => x.usuarioID == usuario.usu_idusuario).ToList();
                    if (userLanguages != null)
                    {
                        foreach (var language in userLanguages)
                        {
                            usuario.idiomas.Add(new IdiomasDto
                            {
                                idi_nombre = language.idiomas.nombre
                            });
                        }
                    }
                    respuesta.Result = usuario;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public List<ProyectosDto> GetProyectos(int idexperiencia)
        {
            List<ProyectosDto> lstProyectos = null;
            try
            {
                var proyectos = context.ExperienciaLaboral.Include("Proyectos").Where(x => x.ExperienciaLaboralID == idexperiencia).ToList();
                if (proyectos != null)
                {
                   
                    
                }
            }
            catch(Exception e)
            {

            }
            return null;
        }

        public List<ConocimientoTecnicoDto> GetProyectoTecnologias(int idproyecto)
        {
            List<ConocimientoTecnicoDto> lstConocimiento = new List<ConocimientoTecnicoDto>();
            try
            {
                var conocimientos = context.ProyectoTecnologia.Include("ConocimientoTecnico").Where(x => x.ProyectosID == idproyecto).ToList();
                if (conocimientos != null)
                {
                    foreach(var t in conocimientos)
                    {
                        lstConocimiento.Add(new ConocimientoTecnicoDto()
                        {
                            tct_nombre = t.proyectos.nombre
                        });
                    }
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                lstConocimiento = null;
            }
            return lstConocimiento;
        }
        public List<ProyectosDto> GetProyectosExperienciaLaboral(int idexperiencia)
        {
            List<ProyectosDto> proyectos = new List<ProyectosDto>();
            ProyectosDto proyecto = null;
            try
            {
                var data = context.UsuarioProyectos.Include("Proyectos").Where(x => x.experienciaLaboralID == idexperiencia).ToList();
                if (data != null)
                {
                    foreach(var p in data)
                    {
                        proyecto = new ProyectosDto();
                        proyecto.pro_nombre = p.proyectos.nombre;
                        proyecto.pro_descripcionproyecto = p.proyectos.descripcionProyecto;
                        proyecto.tecnologia = GetConocimientoProyecto(p.proyectos.ProyectosID);
                        //proyecto.tecnologia = GetProyectoTecnologias(p.proyectosID);
                        proyectos.Add(proyecto);
                    }
                }
            }
            catch(Exception e)
            {
                proyectos = new List<ProyectosDto>();
            }
            return proyectos;
        }
        public List<ConocimientoTecnicoDto> GetConocimientoProyecto(int idproyecto)
        {
            List<ConocimientoTecnicoDto> lstConocimiento = new List<ConocimientoTecnicoDto>();
            ConocimientoTecnicoDto conocimiento = null;
            try
            {
                var data = context.ProyectoTecnologia.Include("ConocimientoTecnico").Where(x => x.ProyectosID == idproyecto).ToList();
                if (data != null)
                {
                    foreach(var c in data)
                    {
                        conocimiento = new ConocimientoTecnicoDto();
                        conocimiento.tct_nombre = c.conocimientoTecnico.nombre;
                        lstConocimiento.Add(conocimiento);
                    }
                }
            }
            catch(Exception e)
            {
                lstConocimiento = new List<ConocimientoTecnicoDto>();
            }
            return lstConocimiento;
        }
    }
}
