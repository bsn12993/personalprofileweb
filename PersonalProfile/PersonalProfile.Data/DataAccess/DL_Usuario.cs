﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_Usuario
    {
        Response respuesta = null;
        ProfileContext context = null;

        public DL_Usuario()
        {
            context = new ProfileContext();
        }

        #region Usuario
        public Response GetUsuarios()
        {
            respuesta = new Response();
            try
            {
                var lstUsers = context.Usuario.ToList();
                if (lstUsers != null && lstUsers.Count > 0) 
                {
                    respuesta.Result = lstUsers;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetUsuarioById(int id)
        {
            respuesta = new Response();
            try
            {
                var user = context.Usuario.Where(x => x.UsuarioID == id).SingleOrDefault();
                if (user != null)
                {
                    respuesta.Result = user;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetUsuarioByCorreoContrasenia(string correo,string contrasenia)
        {
            respuesta = new Response();
            try
            {
                var passEncode = HashHelper.Base64Encode(contrasenia);
                var user = context.Usuario.Where(x => x.correo == correo && x.contrasenia == passEncode).SingleOrDefault();
                if (user != null)
                {
                    respuesta.Result = user;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha validado el usuario";
                }
                else
                    throw new Exception("El usuario no existe");
            }
            catch(Exception e)
            {
                respuesta.Result = null;
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
            }
            return respuesta;
        }
        public Response PostUsuario(Usuario usuario)
        {
            respuesta = new Response();
            try
            {
                var exist = context.Usuario.Where(x => x.correo.Equals(usuario.correo)).SingleOrDefault();
                if (exist == null)
                {
                    usuario.contrasenia = HashHelper.Base64Encode(usuario.contrasenia);
                    context.Usuario.Add(usuario);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha creado el usuario";
                }
                else
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe un usuario con el correo: " + usuario.correo;
                }
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PutUsuario(int id, Usuario usuario)
        {
            respuesta = new Response();
            try
            {
                var target = context.Usuario.Where(x => x.UsuarioID == id).SingleOrDefault();
                if (target != null)
                {
                    target.nombre = usuario.nombre;
                    target.apellidoPaterno = usuario.apellidoPaterno;
                    target.apellidoMaterno = usuario.apellidoMaterno;
                    target.correo = usuario.correo;
                    target.descripcion = usuario.descripcion;
                    target.direccion = usuario.direccion;
                    target.contrasenia = usuario.contrasenia;
                    target.fechaNacimiento = usuario.fechaNacimiento;
                    target.imagen = usuario.imagen;
                    target.perfil = usuario.perfil;
                    target.telefono = usuario.telefono;
                    context.SaveChanges();

                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se ha editado el usuario";
                    respuesta.Result = target;
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response DeleteUsuario(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.Usuario.Where(x => x.UsuarioID == id).SingleOrDefault();
                if (target != null)
                {
                    context.Usuario.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha eliminado el usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        #endregion

        #region Usuario Conocimiento
        public Response GetUsuarioConocimiento(int idusuario)
        {
            respuesta = new Response();
            try
            {
                var skills = context.UsuarioConocimiento.Include("ConocimientoTecnico").
                    Include("Usuario").
                    Where(x => x.usuarioID == idusuario).ToList();
                if (skills.Count > 0) 
                {
                    respuesta.Result = skills;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetUsuarioConocimientoById(int idusuarioConocimiento)
        {
            respuesta = new Response();
            try
            {
                var skills = context.UsuarioConocimiento.Include("ConocimientoTecnico").
                    Include("Usuario").
                    Where(x => x.UsuarioConocimientoID == idusuarioConocimiento).SingleOrDefault();
                if (skills != null)
                {
                    respuesta.Result = skills;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PostUsuarioConocimiento(UsuarioConocimiento usuarioConocimiento)
        {
            respuesta = new Response();
            try
            {
                var exist = context.UsuarioConocimiento.Where(x => x.conocimientoTecnicoID == usuarioConocimiento.conocimientoTecnicoID && x.usuarioID == usuarioConocimiento.usuarioID).ToList();
                if (exist.Count == 0)
                {
                    context.UsuarioConocimiento.Add(usuarioConocimiento);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha creado el Conocimiento del usuario";
                }
                else
                    throw new Exception("El usuario ya cuenta con este conocimiento");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PutUsuarioConocimiento(int id, UsuarioConocimiento usuarioConocimiento)
        {
            respuesta = new Response();
            try
            {
                var exist = context.UsuarioConocimiento.Where(x => x.conocimientoTecnicoID == usuarioConocimiento.conocimientoTecnicoID).ToList();
                if (exist.Count > 1) 
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe este conocimiento agregado";
                    return respuesta;
                }
                var target = context.UsuarioConocimiento.Where(x => x.UsuarioConocimientoID == id).SingleOrDefault();
                if (target != null)
                {
                    target.usuarioID = usuarioConocimiento.usuarioID;
                    target.conocimientoTecnicoID = usuarioConocimiento.conocimientoTecnicoID;
                    target.nivel = usuarioConocimiento.nivel;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha editado el Conocimiento del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response DeleteUsuarioConocimiento(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioConocimiento.Where(x => x.UsuarioConocimientoID == id).SingleOrDefault();
                if (target != null)
                {
                    context.UsuarioConocimiento.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha eliminado el  conocimiento usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        #endregion

        #region Usuario Formacion Academica
        public Response GetUsuarioFormacionAcademica(int idusuario)
        {
            respuesta = new Response();
            try
            {
                var educations = context.UsuarioFormacionAcademica.Include("FormacionAcademica").
                    Include("Usuario").
                    Where(x => x.usuarioID == idusuario).ToList();
                if (educations.Count > 0) 
                {
                    respuesta.Result = educations;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetUsuarioFormacionAcademicaById(int idusuarioFormacion)
        {
            respuesta = new Response();
            try
            {
                var educations = context.UsuarioFormacionAcademica.Include("FormacionAcademica").
                    Include("Usuario").
                    Where(x => x.UsuarioFormacionAcademicaID == idusuarioFormacion).SingleOrDefault();
                if (educations != null)
                {
                    respuesta.Result = educations;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PostUsuarioFormacionAcademica(UsuarioFormacionAcademica usuarioFormacion)
        {
            respuesta = new Response();
            try
            {
                context.UsuarioFormacionAcademica.Add(usuarioFormacion);
                context.SaveChanges();
                respuesta.IsSuccess = true;
                respuesta.Result = null;
                respuesta.Message = "Se ha creado la formacion del usuario";
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PutUsuarioFormacionAcademica(int id, UsuarioFormacionAcademica usuarioFormacion)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioFormacionAcademica.Include("FormacionAcademica").Where(x => x.UsuarioFormacionAcademicaID == id && x.formacionAcademicaID == usuarioFormacion.formacionAcademicaID).SingleOrDefault();
                if (target != null)
                {
                    target.usuarioID = usuarioFormacion.usuarioID;
                    target.formacionAcademicaID = usuarioFormacion.formacionAcademicaID;
                    target.formacionAcademica.FormacionAcademicaID = usuarioFormacion.formacionAcademica.FormacionAcademicaID;
                    target.formacionAcademica.instituto = usuarioFormacion.formacionAcademica.instituto;
                    target.formacionAcademica.carrera = usuarioFormacion.formacionAcademica.carrera;
                    target.formacionAcademica.anioIngreso = usuarioFormacion.formacionAcademica.anioIngreso;
                    target.formacionAcademica.anioSalida = usuarioFormacion.formacionAcademica.anioSalida;
                    target.formacionAcademica.titulo = usuarioFormacion.formacionAcademica.titulo;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha creado el formacion del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response DeleteUsuarioFormacionAcademica(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioFormacionAcademica.Where(x => x.UsuarioFormacionAcademicaID == id).SingleOrDefault();
                if (target != null)
                {
                    var target2 = context.FormacionAcademica.Where(x => x.FormacionAcademicaID == target.formacionAcademicaID).SingleOrDefault();
                    context.UsuarioFormacionAcademica.Remove(target);
                    context.FormacionAcademica.Remove(target2);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha eliminado el formacion del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        #endregion

        #region Usuario Experiencia Laboral
        public Response GetUsuarioExperienciaLaboral(int idusuario)
        {
            respuesta = new Response();
            try
            {
                var experiences = context.UsuarioExperienciaLaboral.Include("ExperienciaLaboral").
                    Include("Usuario").
                    Where(x => x.usuarioID == idusuario).ToList();
                if (experiences.Count > 0) 
                {
                    respuesta.Result = experiences;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetUsuarioExperienciaLaboralById(int idusuarioExperiencia)
        {
            respuesta = new Response();
            try
            {
                var experiences = context.UsuarioExperienciaLaboral.Include("ExperienciaLaboral").
                    Include("Usuario").
                    Where(x => x.UsuarioExperienciaLaboralID == idusuarioExperiencia).SingleOrDefault();
                if (experiences != null)
                {
                    respuesta.Result = experiences;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PostUsuarioExperienciaLaboral(UsuarioExperienciaLaboral usuarioExperiencia)
        {
            respuesta = new Response();
            try
            {
                var existRecord = context.UsuarioExperienciaLaboral
                    .Where(x => x.experienciaLaboral.nombreEmpresa.ToLower()
                    .Equals(usuarioExperiencia.experienciaLaboral.nombreEmpresa.ToLower()) &&
                    x.experienciaLaboral.nombrePuesto.ToLower()
                    .Equals(usuarioExperiencia.experienciaLaboral.nombrePuesto.ToLower())).Count();
                if (existRecord > 0)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe un registro con la misma empresa y puesto";
                    return respuesta;
                }

                var existEmpresa = context.UsuarioExperienciaLaboral
                    .Where(x => x.experienciaLaboral.nombreEmpresa.ToLower()
                    .Equals(usuarioExperiencia.experienciaLaboral.nombreEmpresa.ToLower())).Count();
                if (existEmpresa > 0)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe un registro con la misma empresa";
                    return respuesta;
                }

                context.UsuarioExperienciaLaboral.Add(usuarioExperiencia);
                context.SaveChanges();
                respuesta.IsSuccess = true;
                respuesta.Result = null;
                respuesta.Message = "Se ha creado la experiencia del usuario";
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PutUsuarioExperienciaLaboral(int id, UsuarioExperienciaLaboral usuarioExperiencia)
        {
            respuesta = new Response();
            try
            {
                var count = context.UsuarioExperienciaLaboral
                    .Where(x => x.experienciaLaboral.nombreEmpresa.ToLower().Equals(usuarioExperiencia.experienciaLaboral.nombreEmpresa.ToLower())).Count();
                if (count > 1)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe un registro con el mismo nombre";
                    return respuesta;
                }

                var emp = context.UsuarioExperienciaLaboral.Include("ExperienciaLaboral")
                    .Where(x => x.UsuarioExperienciaLaboralID == usuarioExperiencia.UsuarioExperienciaLaboralID).SingleOrDefault().experienciaLaboral.nombreEmpresa;
                if (!emp.ToLower().Equals(usuarioExperiencia.experienciaLaboral.nombreEmpresa.ToLower()))
                {
                    var count_exist = context.UsuarioExperienciaLaboral
                        .Where(x => x.experienciaLaboral.nombreEmpresa.ToLower().Equals(usuarioExperiencia.experienciaLaboral.nombreEmpresa.ToLower())).Count();
                    if (count_exist > 0)
                    {
                        respuesta.IsSuccess = false;
                        respuesta.Message = "Ya existe un registro con el mismo nombre";
                        return respuesta;
                    }
                }

                var target = context.UsuarioExperienciaLaboral.Include("ExperienciaLaboral").Where(x => x.UsuarioExperienciaLaboralID == id).SingleOrDefault();
                if (target != null)
                {
                    target.usuarioID = usuarioExperiencia.usuarioID;
                    //target.ProyectosID = usuarioExperiencia.ProyectosID;
                    target.experienciaLaboralID = usuarioExperiencia.experienciaLaboralID;
                    target.experienciaLaboral.actividad = usuarioExperiencia.experienciaLaboral.actividad;
                    target.experienciaLaboral.actual = usuarioExperiencia.experienciaLaboral.actual;
                    target.experienciaLaboral.fechaIngreso = usuarioExperiencia.experienciaLaboral.fechaIngreso;
                    target.experienciaLaboral.fechaSalida = usuarioExperiencia.experienciaLaboral.fechaSalida;
                    target.experienciaLaboral.nombreEmpresa = usuarioExperiencia.experienciaLaboral.nombreEmpresa;
                    target.experienciaLaboral.nombrePuesto = usuarioExperiencia.experienciaLaboral.nombrePuesto;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha editado el experiencia del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response DeleteUsuarioExperienciaLaboral(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioExperienciaLaboral.Where(x => x.UsuarioExperienciaLaboralID == id).SingleOrDefault();
                if (target != null)
                {
                    context.UsuarioExperienciaLaboral.Remove(target);
                    var target2 = context.ExperienciaLaboral.Where(x => x.ExperienciaLaboralID == target.experienciaLaboralID).SingleOrDefault();
                    context.ExperienciaLaboral.Remove(target2);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha eliminado el experiencia del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        #endregion

        #region Usuario Idiomas
        public Response GetUsuarioIdiomas(int idusuario)
        {
            respuesta = new Response();
            try
            {
                var languages = context.UsuarioIdioma.Include("Idiomas").
                    Include("Usuario").
                    Where(x => x.usuarioID == idusuario).ToList();
                if (languages.Count > 0) 
                {
                    respuesta.Result = languages;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetUsuarioIdiomasById(int idusuarioIdioma)
        {
            respuesta = new Response();
            try
            {
                var languages = context.UsuarioIdioma.Include("Idiomas").
                    Include("Usuario").
                    Where(x => x.UsuarioIdiomaID == idusuarioIdioma).SingleOrDefault();
                if (languages != null)
                {
                    respuesta.Result = languages;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PostUsuarioIdiomas(UsuarioIdioma usuarioIdioma)
        {
            respuesta = new Response();
            try
            {
                var exist = context.UsuarioIdioma.Where(x => x.idiomasID == usuarioIdioma.idiomasID && x.usuarioID == usuarioIdioma.usuarioID).Count();
                if (exist > 0)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "El usuario ya cuenta con este idioma";
                    return respuesta;
                }
                context.UsuarioIdioma.Add(usuarioIdioma);
                context.SaveChanges();
                respuesta.IsSuccess = true;
                respuesta.Result = null;
                respuesta.Message = "Se ha creado la idioma del usuario";
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PutUsuarioIdiomas(int id, UsuarioIdioma usuarioIdioma)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioIdioma.Where(x => x.UsuarioIdiomaID == id).SingleOrDefault();
                if (target != null)
                {
                    target.usuarioID = usuarioIdioma.usuarioID;
                    target.idiomasID = usuarioIdioma.idiomasID;
                    target.nivel = usuarioIdioma.nivel;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha creado el idioma del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response DeleteUsuarioIdiomas(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioIdioma.Where(x => x.UsuarioIdiomaID == id).SingleOrDefault();
                if (target != null)
                {
                    context.UsuarioIdioma.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha eliminado el idioma del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        #endregion

        #region Usuario Proyecto
        public Response GetUsuarioProyectos(int idusuario)
        {
            respuesta = new Response();
            try
            {
                var projects = context.UsuarioProyectos.Include("Proyectos").
                    Include("Usuario").
                    Where(x => x.usuarioID == idusuario).ToList();
                if (projects.Count > 0) 
                {
                    respuesta.Result = projects;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetUsuarioProyectosById(int idusuarioProyecto)
        {
            respuesta = new Response();
            try
            {
                var projects = context.UsuarioProyectos.Include("Proyectos").
                    Include("Usuario").
                    Where(x => x.UsuarioProyectosID == idusuarioProyecto).SingleOrDefault();
                if (projects != null)
                {
                    respuesta.Result = projects;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PostUsuarioProyectos(UsuarioProyectos usuarioProyecto)
        {
            respuesta = new Response();
            try
            {
                var exist = context.UsuarioProyectos
                    .Include("Proyectos").Where(x => x.proyectos.nombre.ToLower().Equals(usuarioProyecto.proyectos.nombre.ToLower())).Count();

                if (exist > 0)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe un registro con el mismo nombre";
                    return respuesta;
                }

                context.UsuarioProyectos.Add(usuarioProyecto);
                context.SaveChanges();
                respuesta.IsSuccess = true;
                respuesta.Result = null;
                respuesta.Message = "Se ha creado la proyecto del usuario";
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PutUsuarioProyectos(int id, UsuarioProyectos usuarioProyecto)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioProyectos.Include("Proyectos").Where(x => x.UsuarioProyectosID == id).SingleOrDefault();
                if (target != null)
                {
                    target.usuarioID = usuarioProyecto.usuarioID;
                    target.experienciaLaboralID = usuarioProyecto.experienciaLaboralID;
                    target.proyectosID = usuarioProyecto.proyectosID;
                    target.proyectos.nombre = usuarioProyecto.proyectos.nombre;
                    target.proyectos.descripcionProyecto = usuarioProyecto.proyectos.descripcionProyecto;


                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha creado el proyecto del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response DeleteUsuarioProyectos(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioProyectos.Where(x => x.UsuarioProyectosID == id).SingleOrDefault();
                if (target != null)
                {
                    context.UsuarioProyectos.Remove(target);
                    var target2 = context.Proyectos.Where(x => x.ProyectosID == target.proyectosID).SingleOrDefault();
                    context.Proyectos.Remove(target2);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha eliminado el proyecto del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        #endregion

        #region Usuario Contacto
        public Response GetUsuarioContacto(int idusuario)
        {
            respuesta = new Response();
            try
            {
                var contacts = context.UsuarioContacto.Include("Contacto").
                    Include("Usuario").
                    Where(x => x.UsuarioID == idusuario).ToList();
                if (contacts.Count > 0) 
                {
                    respuesta.Result = contacts;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response GetUsuarioContactoById(int idusuarioContacto)
        {
            respuesta = new Response();
            try
            {
                var contacts = context.UsuarioContacto.Include("Contacto").
                    Include("Usuario").
                    Where(x => x.UsuarioContactoID == idusuarioContacto).SingleOrDefault();
                if (contacts != null)
                {
                    respuesta.Result = contacts;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch (Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PostUsuarioContacto(UsuarioContacto usuarioContacto)
        {
            respuesta = new Response();
            try
            {
                context.UsuarioContacto.Add(usuarioContacto);
                context.SaveChanges();
                respuesta.IsSuccess = true;
                respuesta.Result = null;
                respuesta.Message = "Se ha creado la contacto del usuario";
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response PutUsuarioContacto(int id, UsuarioContacto usuarioContacto)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioContacto.Where(x => x.UsuarioContactoID == id).SingleOrDefault();
                if (target != null)
                {
                    target.UsuarioID = usuarioContacto.UsuarioID;
                    target.ContactoID = usuarioContacto.ContactoID;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha creado el contacto del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        public Response DeleteUsuarioContacto(int id)
        {
            respuesta = new Response();
            try
            {
                var target = context.UsuarioContacto.Where(x => x.UsuarioContactoID == id).SingleOrDefault();
                if (target != null)
                {
                    context.UsuarioContacto.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                    respuesta.Result = null;
                    respuesta.Message = "Se ha eliminado el contacto del usuario";
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch (Exception e)
            {
                respuesta.IsSuccess = false;
                //LogException.LogException.Save(this, e);
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
        #endregion

    }
}
