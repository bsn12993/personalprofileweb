﻿using PersonalProfile.Core.Context;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Data.DataAccess
{
    public class DL_Idiomas
    {
        ProfileContext context = null;
        Response respuesta = null;

        public DL_Idiomas()
        {
            context = new ProfileContext();
        }

        public Response GetIdiomas()
        {
            respuesta = new Response();
            try
            {
                var lstIdiomas = context.Idiomas.ToList();
                if (lstIdiomas.Count > 0) 
                {
                    respuesta.Result = lstIdiomas;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response GetIdiomaById(int id)
        {
            respuesta = new Response();
            try
            {
                var idioma = context.Idiomas.Where(x => x.IdiomasID == id).SingleOrDefault();
                if (idioma != null)
                {
                    respuesta.Result = idioma;
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Se encontrarón resultados";
                }
                else
                    throw new Exception("No se encontrarón resultados");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response PostIdioma(Idiomas idioma)
        {
            respuesta = new Response();
            try
            {
                var exist = context.Idiomas.Where(x => x.nombre.Equals(idioma.nombre)).SingleOrDefault();
                if (exist == null)
                {
                    context.Idiomas.Add(idioma);
                    context.SaveChanges();
                    respuesta.Message = "Se registro el idioma";
                    respuesta.IsSuccess = true;
                }
                else
                {
                    respuesta.IsSuccess = true;
                    respuesta.Message = "Ya existe el registro " + idioma.nombre;
                }
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response PutIdioma(int id,Idiomas idioma)
        {
            respuesta = new Response();
            try
            {
                var count = context.Idiomas.Where(x => x.nombre.ToLower().Equals(idioma.nombre.ToLower())).ToList();
                if (count.Count >= 1) 
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "Ya existe un registro con el mismo nombre";
                    return respuesta;
                }

                var target = context.Idiomas.Where(x => x.IdiomasID == id).SingleOrDefault();
                if (target != null)
                {
                    target.nombre = idioma.nombre;
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }

        public Response DeleteIdioma(int id)
        {
            respuesta = new Response();
            try
            {
                var count = context.UsuarioIdioma.Where(x => x.idiomasID == id).ToList();
                if (count.Count > 0)
                {
                    respuesta.IsSuccess = false;
                    respuesta.Message = "No se puede eliminar el registro porque esta asociado a la información del usuario, primero elimine la asignación";
                    return respuesta;
                }

                var target = context.Idiomas.Where(x => x.IdiomasID == id).SingleOrDefault();
                if (target != null)
                {
                    context.Idiomas.Remove(target);
                    context.SaveChanges();
                    respuesta.IsSuccess = true;
                }
                else
                    throw new Exception("No se encontró el registro");
            }
            catch(Exception e)
            {
                //LogException.LogException.Save(this, e);
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
    }
}
