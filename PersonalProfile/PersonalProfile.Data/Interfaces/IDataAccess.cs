﻿using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Data.Interfaces
{
    public interface IDataAccess<T>
    {
        Response Get();
        Response GetById(int id);
        Response Post(T model);
        Response Put(T model, int id);
        Response Delete(int id);
    }
}
