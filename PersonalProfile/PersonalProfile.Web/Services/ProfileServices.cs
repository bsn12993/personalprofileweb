﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using PersonalProfile.Core.Domain;
using PersonalProfile.Web.Models;

namespace PersonalProfile.Web.Services
{
    public class ProfileServices : Comun
    {
        UsuarioDto usuario = null;
        public ProfileServices()
        {
            usuario = new UsuarioDto();
        }

        public UsuarioDto GetData(string user)
        {
            var url = $"{API}{Perfil}/{user}";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            var content = "";
            using(var response = (HttpWebResponse)request.GetResponse())
            {
                using(var stream = response.GetResponseStream())
                {
                    using(var sr=new StreamReader(stream))
                    {
                        content = sr.ReadToEnd();
                        var data = (JObject)JsonConvert.DeserializeObject(content);
                        if (data != null)
                        {
                            if ((bool)data["IsSuccess"])
                            {
                                var json = JsonConvert.SerializeObject(data["Result"]);
                                usuario = JsonConvert.DeserializeObject<UsuarioDto>(json);
                            }
                        }
                    }
                }
            }
            return usuario;
        } 
         
    }
}