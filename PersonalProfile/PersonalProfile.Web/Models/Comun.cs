﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PersonalProfile.Web.Models
{
    public class Comun
    {
        public string API { get; set; }
        public string Perfil { get; set; }

        public Comun()
        {
            API = ConfigurationManager.AppSettings["API"].ToString();
            Perfil = ConfigurationManager.AppSettings["Perfil"].ToString();
        }
    }
}