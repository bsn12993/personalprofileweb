﻿using PersonalProfile.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PersonalProfile.Web.Controllers
{
    public class PerfilController : Controller
    {
        ProfileServices _services = null;
        public PerfilController()
        {
            _services = new ProfileServices();
        }
        // GET: Perfil
        public ActionResult Index(string user = "YnNuXzEyOTAzQGhvdG1haWwuY29t")
        {
            if (!string.IsNullOrEmpty(user)) 
            {
                var data = _services.GetData(user);
                var lstOrdenada = data.experienciaLaboral.OrderByDescending(x => x.ela_fechaingresao).ToList();
                data.experienciaLaboral = lstOrdenada;
                return View(data);
            }
            else
                return View("Error");
        }
    }
}