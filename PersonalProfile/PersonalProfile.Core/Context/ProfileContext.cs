﻿using PersonalProfile.Core.EntityModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Context
{
    public class ProfileContext : DbContext
    {
        public ProfileContext():base("Data Source=(local);Initial Catalog=PersonalProfile_DEV;User ID=sa;Password=123;")
        {

        }

        public DbSet<ConocimientoTecnico> ConocimientoTecnico { get; set; }
        public DbSet<Contacto> Contacto { get; set; }
        public DbSet<ExperienciaLaboral> ExperienciaLaboral { get; set; }
        public DbSet<FormacionAcademica> FormacionAcademica { get; set; }
        public DbSet<Idiomas> Idiomas { get; set; }
        public DbSet<Proyectos> Proyectos { get; set; }
        public DbSet<ProyectoTecnologia> ProyectoTecnologia { get; set; }
        public DbSet<TipoTecnologia> TipoTecnologia { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<UsuarioConocimiento> UsuarioConocimiento { get; set; }
        public DbSet<UsuarioContacto> UsuarioContacto { get; set; }
        public DbSet<UsuarioExperienciaLaboral> UsuarioExperienciaLaboral { get; set; }
        public DbSet<UsuarioFormacionAcademica> UsuarioFormacionAcademica { get; set; }
        public DbSet<UsuarioIdioma> UsuarioIdioma { get; set; }
        public DbSet<UsuarioProyectos> UsuarioProyectos { get; set; }

    }
}
