﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Domain
{
    public class TipoTecnologiaDto
    {
        public int id { get; set; }
        public string nombre { get; set; }
    }
}
