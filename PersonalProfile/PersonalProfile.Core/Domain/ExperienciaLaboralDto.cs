﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Domain
{
    public class ExperienciaLaboralDto
    {
        public ExperienciaLaboralDto()
        {
            proyectos = new List<ProyectosDto>();
        }

        public int ela_idiexplaboral { get; set; }
        public string ela_nombrepuesto { get; set; }
        public string ela_nombreempresa { get; set; }
        public string ela_actividad { get; set; }
        public DateTime ela_fechaingresao { get; set; }
        public DateTime? ela_fechasalida { get; set; }
        public UsuarioDto cat_usuario { get; set; }
        public List<ProyectosDto> proyectos { get; set; }
        public int actual { get; set; }
    }
}
