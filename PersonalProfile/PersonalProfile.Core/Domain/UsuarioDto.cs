﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Domain
{
    public class UsuarioDto
    {
        public UsuarioDto()
        {
            this.experienciaLaboral = new List<ExperienciaLaboralDto>();
            this.formacionAcademica = new List<FormacionAcademicaDto>();
            this.idiomas = new List<IdiomasDto>();
            this.conocimiento = new List<ConocimientoTecnicoDto>();
            this.usu_perfil = string.Empty;
            this.proyectos = new List<ProyectosDto>();
        }

        public int usu_idusuario { get; set; } 
        public string usu_nombre { get; set; }
        public string usu_apellidopaterno { get; set; }
        public string usu_apellidomaterno { get; set; }
        public string usu_usuario { get; set; }
        public string usu_contrasenia { get; set; }
        public string usu_imagen { get; set; }
        public string usu_fechanacimiento { get; set; }
        public List<ExperienciaLaboralDto> experienciaLaboral { get; set; }
        public List<FormacionAcademicaDto> formacionAcademica { get; set; }
        public List<ProyectosDto> proyectos { get; set; }
        public string usu_correo { get; set; }
        public string usu_direccion { get; set; }
        public string usu_telefono { get; set; }
        public string usu_descripcion { get; set; }
        public List<IdiomasDto> idiomas { get; set; }
        public List<ConocimientoTecnicoDto> conocimiento { get; set; }
        public string usu_perfil { get; set; }
    }
}
