﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Domain
{
    public class ConocimientoTecnicoDto
    {
        public int ctc_idconocimientotec { get; set; }
        public string tct_nombre { get; set; }
        public int? rel_nivel { get; set; }
        public int tipo_tecnologia { get; set; }
    }
}
