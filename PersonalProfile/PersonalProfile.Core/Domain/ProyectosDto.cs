﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Domain
{
    public class ProyectosDto
    {
        public ProyectosDto()
        {
            this.tipo_tecnologia = new List<TipoTecnologiaDto>();
            this.tecnologia = new List<ConocimientoTecnicoDto>();
        }
        public int pro_idproyecto { get; set; }
        public string pro_nombre { get; set; }
        public string pro_descripcionproyecto { get; set; }
        public string pro_enlacegithub { get; set; }
        public UsuarioDto cat_usuario { get; set; }
        public List<TipoTecnologiaDto> tipo_tecnologia { get; set; }
        public List<ConocimientoTecnicoDto> tecnologia { get; set; }
    }
}
