﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Domain
{
    public class IdiomasDto
    {
        public int idi_ididioma { get; set; }
        public string idi_nombre { get; set; }
    }
}
