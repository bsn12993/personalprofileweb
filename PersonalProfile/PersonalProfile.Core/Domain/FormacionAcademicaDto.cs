﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Domain
{
    public class FormacionAcademicaDto
    {
        public int fac_idiformacionacademica { get; set; }
        public string fac_institucion { get; set; }
        public int fac_anioingresao { get; set; }
        public int fac_aniofinalizo { get; set; }
        public string fac_carrera { get; set; }
        public int fac_titulo { get; set; }
        public UsuarioDto cat_usuario { get; set; }
    }
}
