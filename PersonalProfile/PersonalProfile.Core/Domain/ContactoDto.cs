﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Domain
{
    public class ContactoDto
    {
        public int con_idcontacto { get; set; }
        public string con_nombre { get; set; }
        public string con_tema { get; set; }
        public string con_mensaje { get; set; }
        public string con_correo { get; set; }
    }
}
