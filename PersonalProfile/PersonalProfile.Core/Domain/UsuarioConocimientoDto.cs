﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.Domain
{
    public class UsuarioConocimientoDto
    {
        public int rel_idusuarioconocimiento { get; set; }
        public int rel_nivel { get; set; }
        public ConocimientoTecnicoDto cat_conocimientotecnico { get; set; }
        public UsuarioDto cat_usuario { get; set; }
    }
}
