namespace PersonalProfile.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v8 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.cat_conocimientotecnico",
                c => new
                    {
                        idconocimientotecnico = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 40),
                        idtipotecnologia = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idconocimientotecnico)
                .ForeignKey("dbo.cat_tecnologia", t => t.idtipotecnologia, cascadeDelete: true)
                .Index(t => t.idtipotecnologia);
            
            CreateTable(
                "dbo.cat_tecnologia",
                c => new
                    {
                        idtipotecnologia = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.idtipotecnologia);
            
            CreateTable(
                "dbo.cat_contacto",
                c => new
                    {
                        idcontacto = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 50),
                        url = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.idcontacto);
            
            CreateTable(
                "dbo.experiencialaboral",
                c => new
                    {
                        idexperiencialaboral = c.Int(nullable: false, identity: true),
                        nombrePuesto = c.String(nullable: false, maxLength: 40),
                        nombreEmpresa = c.String(nullable: false, maxLength: 40),
                        actividad = c.String(nullable: false, maxLength: 1000),
                        fechaIngreso = c.DateTime(nullable: false),
                        fechaSalida = c.DateTime(),
                        actual = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idexperiencialaboral);
            
            CreateTable(
                "dbo.formacionacademica",
                c => new
                    {
                        idformacionacademica = c.Int(nullable: false, identity: true),
                        instituto = c.String(nullable: false, maxLength: 50),
                        anioIngreso = c.Int(nullable: false),
                        anioSalida = c.Int(nullable: false),
                        carrera = c.String(nullable: false, maxLength: 50),
                        titulo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idformacionacademica);
            
            CreateTable(
                "dbo.cat_idiomas",
                c => new
                    {
                        ididioma = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 25),
                    })
                .PrimaryKey(t => t.ididioma);
            
            CreateTable(
                "dbo.proyectos",
                c => new
                    {
                        idproyecto = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 40),
                        descripcionProyecto = c.String(nullable: false, maxLength: 300),
                    })
                .PrimaryKey(t => t.idproyecto);
            
            CreateTable(
                "dbo.proyectotecnologia",
                c => new
                    {
                        idproyectotecnologia = c.Int(nullable: false, identity: true),
                        idproyecto = c.Int(nullable: false),
                        idconocimientotecnico = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idproyectotecnologia)
                .ForeignKey("dbo.cat_conocimientotecnico", t => t.idconocimientotecnico, cascadeDelete: true)
                .ForeignKey("dbo.proyectos", t => t.idproyecto, cascadeDelete: true)
                .Index(t => t.idproyecto)
                .Index(t => t.idconocimientotecnico);
            
            CreateTable(
                "dbo.usuarios",
                c => new
                    {
                        idusuario = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 25),
                        imagen = c.String(),
                        apellidoPaterno = c.String(nullable: false, maxLength: 25),
                        apellidoMaterno = c.String(nullable: false, maxLength: 25),
                        contrasenia = c.String(nullable: false, maxLength: 30),
                        fechaNacimiento = c.DateTime(),
                        correo = c.String(nullable: false, maxLength: 50),
                        direccion = c.String(maxLength: 100),
                        telefono = c.String(maxLength: 10),
                        descripcion = c.String(maxLength: 500),
                        perfil = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.idusuario);
            
            CreateTable(
                "dbo.usuarioconocimiento",
                c => new
                    {
                        idusuarioconocimiento = c.Int(nullable: false, identity: true),
                        nivel = c.Int(nullable: false),
                        idusuario = c.Int(nullable: false),
                        idconocimientotecnico = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idusuarioconocimiento)
                .ForeignKey("dbo.cat_conocimientotecnico", t => t.idconocimientotecnico, cascadeDelete: true)
                .ForeignKey("dbo.usuarios", t => t.idusuario, cascadeDelete: true)
                .Index(t => t.idusuario)
                .Index(t => t.idconocimientotecnico);
            
            CreateTable(
                "dbo.usuariocontacto",
                c => new
                    {
                        idusuariocontacto = c.Int(nullable: false, identity: true),
                        idusuario = c.Int(nullable: false),
                        idcontacto = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idusuariocontacto)
                .ForeignKey("dbo.cat_contacto", t => t.idcontacto, cascadeDelete: true)
                .ForeignKey("dbo.usuarios", t => t.idusuario, cascadeDelete: true)
                .Index(t => t.idusuario)
                .Index(t => t.idcontacto);
            
            CreateTable(
                "dbo.usuarioexperiencialaboral",
                c => new
                    {
                        idusuarioexperiencia = c.Int(nullable: false, identity: true),
                        idusuario = c.Int(nullable: false),
                        idexperiencialaboral = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idusuarioexperiencia)
                .ForeignKey("dbo.experiencialaboral", t => t.idexperiencialaboral, cascadeDelete: true)
                .ForeignKey("dbo.usuarios", t => t.idusuario, cascadeDelete: true)
                .Index(t => t.idusuario)
                .Index(t => t.idexperiencialaboral);
            
            CreateTable(
                "dbo.usuarioformacionacademica",
                c => new
                    {
                        idusuarioformacion = c.Int(nullable: false, identity: true),
                        idusuario = c.Int(nullable: false),
                        idformacionacademica = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idusuarioformacion)
                .ForeignKey("dbo.formacionacademica", t => t.idformacionacademica, cascadeDelete: true)
                .ForeignKey("dbo.usuarios", t => t.idusuario, cascadeDelete: true)
                .Index(t => t.idusuario)
                .Index(t => t.idformacionacademica);
            
            CreateTable(
                "dbo.usuarioidioma",
                c => new
                    {
                        idusuarioidioma = c.Int(nullable: false, identity: true),
                        idusuario = c.Int(nullable: false),
                        ididioma = c.Int(nullable: false),
                        nivel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idusuarioidioma)
                .ForeignKey("dbo.cat_idiomas", t => t.ididioma, cascadeDelete: true)
                .ForeignKey("dbo.usuarios", t => t.idusuario, cascadeDelete: true)
                .Index(t => t.idusuario)
                .Index(t => t.ididioma);
            
            CreateTable(
                "dbo.usuarioproyectos",
                c => new
                    {
                        idusuarioproyecto = c.Int(nullable: false, identity: true),
                        idusuario = c.Int(nullable: false),
                        idproyecto = c.Int(nullable: false),
                        idexperiencialaboral = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idusuarioproyecto)
                .ForeignKey("dbo.experiencialaboral", t => t.idexperiencialaboral, cascadeDelete: true)
                .ForeignKey("dbo.proyectos", t => t.idproyecto, cascadeDelete: true)
                .ForeignKey("dbo.usuarios", t => t.idusuario, cascadeDelete: true)
                .Index(t => t.idusuario)
                .Index(t => t.idproyecto)
                .Index(t => t.idexperiencialaboral);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.usuarioproyectos", "idusuario", "dbo.usuarios");
            DropForeignKey("dbo.usuarioproyectos", "idproyecto", "dbo.proyectos");
            DropForeignKey("dbo.usuarioproyectos", "idexperiencialaboral", "dbo.experiencialaboral");
            DropForeignKey("dbo.usuarioidioma", "idusuario", "dbo.usuarios");
            DropForeignKey("dbo.usuarioidioma", "ididioma", "dbo.cat_idiomas");
            DropForeignKey("dbo.usuarioformacionacademica", "idusuario", "dbo.usuarios");
            DropForeignKey("dbo.usuarioformacionacademica", "idformacionacademica", "dbo.formacionacademica");
            DropForeignKey("dbo.usuarioexperiencialaboral", "idusuario", "dbo.usuarios");
            DropForeignKey("dbo.usuarioexperiencialaboral", "idexperiencialaboral", "dbo.experiencialaboral");
            DropForeignKey("dbo.usuariocontacto", "idusuario", "dbo.usuarios");
            DropForeignKey("dbo.usuariocontacto", "idcontacto", "dbo.cat_contacto");
            DropForeignKey("dbo.usuarioconocimiento", "idusuario", "dbo.usuarios");
            DropForeignKey("dbo.usuarioconocimiento", "idconocimientotecnico", "dbo.cat_conocimientotecnico");
            DropForeignKey("dbo.proyectotecnologia", "idproyecto", "dbo.proyectos");
            DropForeignKey("dbo.proyectotecnologia", "idconocimientotecnico", "dbo.cat_conocimientotecnico");
            DropForeignKey("dbo.cat_conocimientotecnico", "idtipotecnologia", "dbo.cat_tecnologia");
            DropIndex("dbo.usuarioproyectos", new[] { "idexperiencialaboral" });
            DropIndex("dbo.usuarioproyectos", new[] { "idproyecto" });
            DropIndex("dbo.usuarioproyectos", new[] { "idusuario" });
            DropIndex("dbo.usuarioidioma", new[] { "ididioma" });
            DropIndex("dbo.usuarioidioma", new[] { "idusuario" });
            DropIndex("dbo.usuarioformacionacademica", new[] { "idformacionacademica" });
            DropIndex("dbo.usuarioformacionacademica", new[] { "idusuario" });
            DropIndex("dbo.usuarioexperiencialaboral", new[] { "idexperiencialaboral" });
            DropIndex("dbo.usuarioexperiencialaboral", new[] { "idusuario" });
            DropIndex("dbo.usuariocontacto", new[] { "idcontacto" });
            DropIndex("dbo.usuariocontacto", new[] { "idusuario" });
            DropIndex("dbo.usuarioconocimiento", new[] { "idconocimientotecnico" });
            DropIndex("dbo.usuarioconocimiento", new[] { "idusuario" });
            DropIndex("dbo.proyectotecnologia", new[] { "idconocimientotecnico" });
            DropIndex("dbo.proyectotecnologia", new[] { "idproyecto" });
            DropIndex("dbo.cat_conocimientotecnico", new[] { "idtipotecnologia" });
            DropTable("dbo.usuarioproyectos");
            DropTable("dbo.usuarioidioma");
            DropTable("dbo.usuarioformacionacademica");
            DropTable("dbo.usuarioexperiencialaboral");
            DropTable("dbo.usuariocontacto");
            DropTable("dbo.usuarioconocimiento");
            DropTable("dbo.usuarios");
            DropTable("dbo.proyectotecnologia");
            DropTable("dbo.proyectos");
            DropTable("dbo.cat_idiomas");
            DropTable("dbo.formacionacademica");
            DropTable("dbo.experiencialaboral");
            DropTable("dbo.cat_contacto");
            DropTable("dbo.cat_tecnologia");
            DropTable("dbo.cat_conocimientotecnico");
        }
    }
}
