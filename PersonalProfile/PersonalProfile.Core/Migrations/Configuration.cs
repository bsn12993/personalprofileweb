namespace PersonalProfile.Core.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PersonalProfile.Core.Context.ProfileContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PersonalProfile.Core.Context.ProfileContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.



            ////Tipo Tecnologia
            //context.TipoTecnologia.Add(new EntityModels.TipoTecnologia { nombre = "Frontend" });
            //context.TipoTecnologia.Add(new EntityModels.TipoTecnologia { nombre = "Backend" });
            //context.TipoTecnologia.Add(new EntityModels.TipoTecnologia { nombre = "Otro" });

            ////Idioma
            //context.Idiomas.Add(new EntityModels.Idiomas { nombre = "Espa�ol" });
            //context.Idiomas.Add(new EntityModels.Idiomas { nombre = "Ingles" });

            ////Contacto
            //context.Contacto.Add(new EntityModels.Contacto { nombre = "LinkendIn", url = "LinkendIn" });
            //context.Contacto.Add(new EntityModels.Contacto { nombre = "GitHub", url = "GitHub" });
            //context.Contacto.Add(new EntityModels.Contacto { nombre = "WebSite", url = "WebSite" });

            ////Proyectos
            //context.Proyectos.Add(new EntityModels.Proyectos { nombre = "Flotillas", descripcionProyecto = "aaaa" });
            //context.Proyectos.Add(new EntityModels.Proyectos { nombre = "Remarkable", descripcionProyecto = "aaaa" });
            //context.Proyectos.Add(new EntityModels.Proyectos { nombre = "Harley Davidson", descripcionProyecto = "aaaa" });
            //context.Proyectos.Add(new EntityModels.Proyectos { nombre = "PMO", descripcionProyecto = "aaaa" });
            //context.Proyectos.Add(new EntityModels.Proyectos { nombre = "Ryder", descripcionProyecto = "aaaa" });
            //context.Proyectos.Add(new EntityModels.Proyectos { nombre = "IEQSA", descripcionProyecto = "aaaa" });
            //context.Proyectos.Add(new EntityModels.Proyectos { nombre = "NACEL", descripcionProyecto = "aaaa" });

            ////Contacto
            //context.Contacto.Add(new EntityModels.Contacto { nombre = "LinkendIn", url = "LinkendIn" });
            //context.Contacto.Add(new EntityModels.Contacto { nombre = "WebSite", url = "WebSite" });


            ////Usuario
            //context.Usuario.Add(new EntityModels.Usuario
            //{
            //    nombre = "Bryan",
            //    apellidoPaterno = "Silverio",
            //    apellidoMaterno = "Nieves",
            //    correo = "bsn_12903@hotmail.com",
            //    contrasenia = "123",
            //    fechaNacimiento = new DateTime(1993, 09, 12),
            //    direccion = "Queretaro",
            //    telefono = "4426112291",
            //    perfil = "Desarrollador .NET"
            //});

            ////Experiencia Laboral
            //context.ExperienciaLaboral.Add(new EntityModels.ExperienciaLaboral
            //{
            //    nombreEmpresa = "Wingu Networks",
            //    nombrePuesto = "Analista y Atencion a Clientes",
            //    actividad = "Atenci�n y soporte t�cnico en Contact center, registro de dominios, contrataci�n de planes de hospedaje, solicitud datos de acceso al panel de control por parte del cliente, facturaci�n y cobranza, �rdenes de pago, actualizaci�n de DNS del dominio, transferencias de dominios.",
            //    fechaIngreso = new DateTime(2015, 03, 03),
            //    fechaSalida = new DateTime(2015, 06, 08),
            //    actual = 1
            //});
            //context.ExperienciaLaboral.Add(new EntityModels.ExperienciaLaboral
            //{
            //    nombreEmpresa = "IT Group",
            //    nombrePuesto = "Desarrollador IT",
            //    actividad = "Desarrollo de aplicaciones web dirigidos al sector automotriz, talleres y agencias de los diferentes estados del pa�s, donde las principales tecnolog�as implementadas como backend son C# y java y complementando con JS, JQuery, Bootstrap, HTML.",
            //    fechaIngreso = new DateTime(2015, 08, 20),
            //    fechaSalida = new DateTime(2017, 08, 15),
            //    actual = 0
            //});
            //context.ExperienciaLaboral.Add(new EntityModels.ExperienciaLaboral
            //{
            //    nombreEmpresa = "DW Software",
            //    nombrePuesto = "Desarrollador de Software",
            //    actividad = "Desarrollo de aplicaciones web para diferentes sectores principalmente para paqueter�a Ryder integraci�n con WebService SOAP de DHL, donde la tecnolog�a implementada fue C# en ASP.NET con MVC, consumo de Web Services.",
            //    fechaIngreso = new DateTime(2017, 08, 20),
            //    fechaSalida = new DateTime(2017, 11, 22),
            //    actual = 0
            //});

            ////Formacion Academica
            //context.FormacionAcademica.Add(new EntityModels.FormacionAcademica
            //{
            //    instituto = "Universidad Politecnica de Quer�taro",
            //    anioIngreso = 2012,
            //    anioSalida = 2015,
            //    carrera = "Ing. en Sistemas Computacionales",
            //    titulo = 1
            //});

            ////////////////////////////////////////////////////////////////////////////////////////////
            ////Conocimiento Tecnico
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "JAVA", TipoTecnologiaID = 2 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "C#", TipoTecnologiaID = 2 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "ASP.NET MVC", TipoTecnologiaID = 2 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "POO", TipoTecnologiaID = 3 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "JavaScript", TipoTecnologiaID = 1 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "Ajax", TipoTecnologiaID = 1 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "JSON", TipoTecnologiaID = 1 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "JQuery", TipoTecnologiaID = 1 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "SQL Server", TipoTecnologiaID = 3 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "MySQL", TipoTecnologiaID = 3 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "Tomcat", TipoTecnologiaID = 3 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "Erwin", TipoTecnologiaID = 3 });
            //context.ConocimientoTecnico.Add(new EntityModels.ConocimientoTecnico() { nombre = "Android Studio", TipoTecnologiaID = 3 });

            ////Proyectos tecnologicos
            //context.ProyectoTecnologia.Add(new EntityModels.ProyectoTecnologia
            //{
            //    ProyectosID = 1,
            //    ConocimientoTecnicoID = 2
            //});
            //context.ProyectoTecnologia.Add(new EntityModels.ProyectoTecnologia
            //{
            //    ProyectosID = 2,
            //    ConocimientoTecnicoID = 2
            //});
            //context.ProyectoTecnologia.Add(new EntityModels.ProyectoTecnologia
            //{
            //    ProyectosID = 3,
            //    ConocimientoTecnicoID = 2
            //});


            ////Usuario Conocimientos
            //context.UsuarioConocimiento.Add(new EntityModels.UsuarioConocimiento
            //{
            //    usuarioID = 1,
            //    UsuarioConocimientoID = 2,
            //    nivel = 50
            //});
            //context.UsuarioConocimiento.Add(new EntityModels.UsuarioConocimiento
            //{
            //    usuarioID = 1,
            //    UsuarioConocimientoID = 3,
            //    nivel = 50
            //});
            //context.UsuarioConocimiento.Add(new EntityModels.UsuarioConocimiento
            //{
            //    usuarioID = 1,
            //    UsuarioConocimientoID = 4,
            //    nivel = 50
            //});
            //context.UsuarioConocimiento.Add(new EntityModels.UsuarioConocimiento
            //{
            //    usuarioID = 1,
            //    UsuarioConocimientoID = 5,
            //    nivel = 50
            //});
            //context.UsuarioConocimiento.Add(new EntityModels.UsuarioConocimiento
            //{
            //    usuarioID = 1,
            //    UsuarioConocimientoID = 6,
            //    nivel = 50
            //});
            //context.UsuarioConocimiento.Add(new EntityModels.UsuarioConocimiento
            //{
            //    usuarioID = 1,
            //    UsuarioConocimientoID = 7,
            //    nivel = 50
            //});
            //context.UsuarioConocimiento.Add(new EntityModels.UsuarioConocimiento
            //{
            //    usuarioID = 1,
            //    UsuarioConocimientoID = 8,
            //    nivel = 50
            //});
            //context.UsuarioConocimiento.Add(new EntityModels.UsuarioConocimiento
            //{
            //    usuarioID = 1,
            //    UsuarioConocimientoID = 9,
            //    nivel = 50
            //});
            //context.UsuarioConocimiento.Add(new EntityModels.UsuarioConocimiento
            //{
            //    usuarioID = 1,
            //    UsuarioConocimientoID = 10,
            //    nivel = 50
            //});

            ////Usuario Formacion Academica
            //context.UsuarioFormacionAcademica.Add(new EntityModels.UsuarioFormacionAcademica
            //{
            //    usuarioID = 1,
            //    formacionAcademicaID = 1
            //});

            ////Usuario Idiomas
            //context.UsuarioIdioma.Add(new EntityModels.UsuarioIdioma
            //{
            //    usuarioID = 1,
            //    UsuarioIdiomaID = 1
            //});

            ////Usuario Contacto
            //context.UsuarioContacto.Add(new EntityModels.UsuarioContacto
            //{
            //    UsuarioID = 1,
            //    ContactoID = 1
            //});
            //context.UsuarioContacto.Add(new EntityModels.UsuarioContacto
            //{
            //    UsuarioID = 1,
            //    ContactoID = 2
            //});
            //context.UsuarioContacto.Add(new EntityModels.UsuarioContacto
            //{
            //    UsuarioID = 1,
            //    ContactoID = 3
            //});


            ////Experiencia Laboral
            //context.UsuarioExperienciaLaboral.Add(new EntityModels.UsuarioExperienciaLaboral
            //{
            //    ProyectosID = 1,
            //    usuarioID = 1,
            //});
            //context.UsuarioExperienciaLaboral.Add(new EntityModels.UsuarioExperienciaLaboral
            //{
            //    ProyectosID = 2,
            //    usuarioID = 1
            //});
            //context.UsuarioExperienciaLaboral.Add(new EntityModels.UsuarioExperienciaLaboral
            //{
            //    ProyectosID = 0,
            //    usuarioID = 1
            //});
            //context.UsuarioExperienciaLaboral.Add(new EntityModels.UsuarioExperienciaLaboral
            //{
            //    ProyectosID = 0,
            //    usuarioID = 1
            //});

        }
    }
}
