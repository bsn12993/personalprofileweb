﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("experiencialaboral")]
    public class ExperienciaLaboral
    {
        [Key]
        [Column("idexperiencialaboral")]
        public int ExperienciaLaboralID { get; set; }
        [MaxLength(40)]
        [Required]
        public string nombrePuesto { get; set; }
        [MaxLength(40)]
        [Required]
        public string nombreEmpresa { get; set; }
        [MaxLength(1000)]
        [Required]
        public string actividad { get; set; }
        [Required]
        public DateTime fechaIngreso { get; set; }
        public Nullable<DateTime> fechaSalida { get; set; }
        [Required]
        public int actual { get; set; }
    }
}
