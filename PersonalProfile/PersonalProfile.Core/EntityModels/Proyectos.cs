﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("proyectos")]
    public class Proyectos
    {
        [Key]
        [Column("idproyecto")]
        public int ProyectosID { get; set; }
        [MaxLength(40)]
        [Required]
        public string nombre { get; set; }
        [MaxLength(300)]
        [Required]
        public string descripcionProyecto { get; set; }
    }
}
