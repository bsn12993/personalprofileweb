﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("usuarioexperiencialaboral")]
    public class UsuarioExperienciaLaboral
    {
        [Key]
        [Column("idusuarioexperiencia")]
        public int UsuarioExperienciaLaboralID { get; set; }
        [Column("idusuario")]
        public int usuarioID { get; set; }
        [ForeignKey("usuarioID")]
        public Usuario usuario { get; set; }
        [Column("idexperiencialaboral")]
        public int experienciaLaboralID { get; set; }
        [ForeignKey("experienciaLaboralID")]
        public ExperienciaLaboral experienciaLaboral { get; set; }
        //[Column("idproyecto")]
        //public int ProyectosID { get; set; }
        //[ForeignKey("ProyectosID")]
        //public Proyectos proyectos { get; set; }

    }
}
