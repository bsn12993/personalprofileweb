﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("cat_idiomas")]
    public class Idiomas
    {
        [Key]
        [Column("ididioma")]
        public int IdiomasID { get; set; }
        [MaxLength(25)]
        [Required]
        public string nombre { get; set; }
    }
}
