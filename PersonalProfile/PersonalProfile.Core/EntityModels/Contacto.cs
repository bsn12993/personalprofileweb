﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("cat_contacto")]
    public class Contacto
    {
        [Key]
        [Column("idcontacto")]
        public int ContactoID { get; set; }
        [MaxLength(50)]
        [Required]
        public string nombre { get; set; }
        [MaxLength(250)]
        [Required]
        public string url { get; set; }
    }
}
