﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("usuariocontacto")]
    public class UsuarioContacto
    {
        [Key]
        [Column("idusuariocontacto")]
        public int UsuarioContactoID { get; set; }
        [Column("idusuario")]
        public int UsuarioID { get; set; }
        [ForeignKey("UsuarioID")]
        public Usuario usuario { get; set; }
        [Column("idcontacto")]
        public int ContactoID { get; set; }
        [ForeignKey("ContactoID")]
        public Contacto contacto { get; set; }
    }
}
