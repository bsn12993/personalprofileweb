﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("cat_tecnologia")]
    public class TipoTecnologia
    {
        [Key]
        [Column("idtipotecnologia")]
        public int TipoTecnologiaID { get; set; }
        [MaxLength(100)]
        [Required]
        public string nombre { get; set; }
    }
}
