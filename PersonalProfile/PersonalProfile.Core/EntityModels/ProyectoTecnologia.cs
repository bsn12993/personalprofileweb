﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("proyectotecnologia")]
    public class ProyectoTecnologia
    {
        [Key]
        [Column("idproyectotecnologia")]
        public int ProyectoTecnologiaID { get; set; }
        [Column("idproyecto")]
        public int ProyectosID { get; set; }
        [ForeignKey("ProyectosID")]
        public Proyectos proyectos { get; set; }
        [Column("idconocimientotecnico")]
        public int ConocimientoTecnicoID { get; set; }
        [ForeignKey("ConocimientoTecnicoID")]
        public ConocimientoTecnico conocimientoTecnico { get; set; }
    }
}
