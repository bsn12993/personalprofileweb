﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("usuarioproyectos")]
    public class UsuarioProyectos
    {
        [Key]
        [Column("idusuarioproyecto")]
        public int UsuarioProyectosID { get; set; }
        [Column("idusuario")]
        public int usuarioID { get; set; }
        [ForeignKey("usuarioID")]
        public Usuario usuario { get; set; }
        [Column("idproyecto")]
        public int proyectosID { get; set; }
        [ForeignKey("proyectosID")]
        public Proyectos proyectos { get; set; }
        [Column("idexperiencialaboral")]
        public int experienciaLaboralID { get; set; }
        [ForeignKey("experienciaLaboralID")]
        public ExperienciaLaboral experienciaLaboral { get; set; }
    }
}
