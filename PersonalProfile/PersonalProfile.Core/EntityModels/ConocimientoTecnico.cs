﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersonalProfile.Core.EntityModels
{
    [Table("cat_conocimientotecnico")]
    public class ConocimientoTecnico
    {
        [Key]
        [Column("idconocimientotecnico")]
        public int ConocimientoTecnicoID { get; set; }
        [MaxLength(40)]
        [Required]
        public string nombre { get; set; }
        [Column("idtipotecnologia")]
        public int TipoTecnologiaID { get; set; }
        [ForeignKey("TipoTecnologiaID")]
        public TipoTecnologia tipoTecnologia { get; set; }
    }
}
