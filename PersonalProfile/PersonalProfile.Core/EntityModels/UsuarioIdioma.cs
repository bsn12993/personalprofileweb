﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("usuarioidioma")]
    public class UsuarioIdioma
    {
        [Key]
        [Column("idusuarioidioma")]
        public int UsuarioIdiomaID { get; set; }
        [Column("idusuario")]
        public int usuarioID { get; set; }
        [ForeignKey("usuarioID")]
        public Usuario usuario { get; set; }
        [Column("ididioma")]
        public int idiomasID { get; set; }
        [ForeignKey("idiomasID")]
        public Idiomas idiomas { get; set; }
        [Column("nivel")]
        public int nivel { get; set; }
    }
}
