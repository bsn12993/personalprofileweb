﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("usuarios")]
    public class Usuario
    {
        [Key]
        [Column("idusuario")]
        public int UsuarioID { get; set; }
        [MaxLength(25)]
        [Required]
        public string nombre { get; set; }
        public string imagen { get; set; }
        [MaxLength(25)]
        [Required]
        public string apellidoPaterno { get; set; }
        [MaxLength(25)]
        [Required]
        public string apellidoMaterno { get; set; }
        [MaxLength(30)]
        [Required]
        public string contrasenia { get; set; }
        //public string usuario { get; set; }
        //[Required]
        public DateTime? fechaNacimiento { get; set; }
        [MaxLength(50)]
        [Required]
        public string correo { get; set; }
        [MaxLength(100)]
        //[Required]
        public string direccion { get; set; }
        [MaxLength(10)]
        //[Required]
        public string telefono { get; set; }
        [MaxLength(500)]
        public string descripcion { get; set; }
        [MaxLength(50)]
        [Required]
        public string perfil { get; set; }
    }
}
