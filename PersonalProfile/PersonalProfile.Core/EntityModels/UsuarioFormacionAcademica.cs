﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("usuarioformacionacademica")]
    public class UsuarioFormacionAcademica
    {
        [Key]
        [Column("idusuarioformacion")]
        public int UsuarioFormacionAcademicaID { get; set; }
        [Column("idusuario")]
        public int usuarioID { get; set; }
        [ForeignKey("usuarioID")]
        public Usuario usuario { get; set; }
        [Column("idformacionacademica")]
        public int formacionAcademicaID { get; set; }
        [ForeignKey("formacionAcademicaID")]
        public FormacionAcademica formacionAcademica { get; set; }
    }

}
