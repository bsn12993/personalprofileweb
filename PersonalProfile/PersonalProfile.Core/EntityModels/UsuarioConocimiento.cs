﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("usuarioconocimiento")]
    public class UsuarioConocimiento
    {
        [Key]
        [Column("idusuarioconocimiento")]
        public int UsuarioConocimientoID { get; set; }
        [Required]
        public int nivel { get; set; }
        [Column("idusuario")]
        public int usuarioID { get; set; }
        [ForeignKey("usuarioID")]
        public Usuario usuario { get; set; }
        [Column("idconocimientotecnico")]
        public int conocimientoTecnicoID { get; set; }
        [ForeignKey("conocimientoTecnicoID")]
        public ConocimientoTecnico conocimientoTecnico { get; set; }
    }
}
