﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Core.EntityModels
{
    [Table("formacionacademica")]
    public class FormacionAcademica
    {
        [Key]
        [Column("idformacionacademica")]
        public int FormacionAcademicaID { get; set; }
        [MaxLength(50)]
        [Required]
        public string instituto { get; set; }
        [Required]
        public int anioIngreso { get; set; }
        [Required]
        public int anioSalida { get; set; }
        [MaxLength(50)]
        [Required]
        public string carrera { get; set; }
        [Required]
        public int titulo { get; set; }
    }
}
