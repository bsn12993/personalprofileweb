﻿using PersonalProfile.Core.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalProfile.Core.ViewModels
{
    public class ProyectoTecnologiaVM
    {     
        public int ProyectoTecnologiaID { get; set; }
        public string nombre { get; set; }
        public int ConocimientoTecnicoID { get; set; }

    }
}