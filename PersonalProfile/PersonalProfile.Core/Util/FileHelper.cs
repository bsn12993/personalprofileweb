﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Pechkin;
using Pechkin.Synchronized;
using PersonalProfile.Core.Models;
using System;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace PersonalProfile.Core.Util
{
    public class FileHelper
    {
        static Response respuesta { get; set; }
        public static Response ConvertFileToBase64(HttpPostedFileBase file)
        {
            respuesta = new Response();
            byte[] fileData = null;
            try
            {
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                    respuesta.Message = "";
                    respuesta.Result = Convert.ToBase64String(fileData);
                    respuesta.IsSuccess = true;
                    
                }
            }
            catch(Exception e)
            {
                respuesta.Result = null;
                respuesta.Message = e.Message;
                respuesta.IsSuccess = false;
            }
            return respuesta;
        }

        public static Response ConvertBase64ToFile(string file)
        {
            respuesta = new Response();
            try
            {
                byte[] imageByes = Convert.FromBase64String(file);
                MemoryStream ms = new MemoryStream(imageByes, 0, imageByes.Length);
                ms.Write(imageByes, 0, imageByes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                respuesta.IsSuccess = true;
                respuesta.Result = image;
                respuesta.Message = "";
            }
            catch(Exception e)
            {
                respuesta.Result = null;
                respuesta.Message = null;
                respuesta.IsSuccess = false;
            }
            return respuesta;
            
        }

        public static string GetHtmlFromURL(string url)
        {
            string html = string.Empty;
            try
            {
                html = (new WebClient()).DownloadString(url);
                return html;
            }
            catch(Exception e)
            {
                return string.Empty;
            }
        }

        public static byte[] GetPDFFile(string html, string css)
        {
            byte[] bytePDF= null;
            try
            {
                //html = html.Replace("<link id=\"theme-style\" rel=\"stylesheet\" href=\"/Content/css/pillar-1.css\">",
                //    $"<style>{css}</style>");

                //GlobalConfig globalConfig = new GlobalConfig();
                //globalConfig.SetMargins(new Margins(100, 100, 100, 100))
                //    .SetDocumentTitle("CV")
                //    .SetPaperSize(PaperKind.A4);

                //ObjectConfig objectConfig = new ObjectConfig();
                //objectConfig.SetCreateExternalLinks(false)
                //    .SetFallbackEncoding(Encoding.ASCII)
                //    .SetLoadImages(true)
                //    .SetPageUri("https://themes.3rdwavemedia.com/demo/orbit/");


                //IPechkin pechkin = new SynchronizedPechkin(globalConfig);

                using (MemoryStream stream = new MemoryStream())
                {
                     
                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                    pdfDoc.Open();
                    using(MemoryStream streamCss=new MemoryStream(Encoding.UTF8.GetBytes(css)))
                    {
                        using(MemoryStream streamHtml=new MemoryStream(Encoding.UTF8.GetBytes(html)))
                        {
                            XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, streamHtml, streamCss);
                        }
                        //StringReader sr = new StringReader(html);
                        //
                    }
                    pdfDoc.Close();
                    bytePDF = stream.ToArray();
                }
                
                return bytePDF;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}