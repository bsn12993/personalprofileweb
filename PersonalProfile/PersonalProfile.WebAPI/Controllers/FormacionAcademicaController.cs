﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.WebAPI.Controllers
{
    [RoutePrefix("api/education")]
    public class FormacionAcademicaController : ApiController
    {
        DL_FormacionAcademica _services = null;
        public FormacionAcademicaController()
        {
            _services = new DL_FormacionAcademica();
        }

        [Route("all")]
        [HttpGet]
        public HttpResponseMessage GetFormacionAcademica()
        {
            var respuesta = _services.GetFormacionAcademica();
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("byid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetFormacionAcademicaById(int id)
        {
            var respuesta = _services.GetFormacionAcademicaById(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }

        [Route("create")]
        [HttpPost]
        public HttpResponseMessage PostFormacionAcademica([FromBody] FormacionAcademica formacionAcademica)
        {
            if (ModelState.IsValid && formacionAcademica != null)
            {
                var respuesta = _services.PostFormacionAcademica(formacionAcademica);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, "", "application/json");
        }

        [Route("update/{id}")]
        [HttpPut]
        public HttpResponseMessage PutFormacionAcademica(int id,[FromBody] FormacionAcademica formacionAcademica)
        {
            if (ModelState.IsValid && formacionAcademica != null)
            {
                var respuesta = _services.PutFormacionAcademica(id, formacionAcademica);
                if (respuesta.IsSuccess)
                    return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, "", "application/json");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public HttpResponseMessage DeleteFormacionAcademica(int id)
        {
            var respuesta = _services.DeleteFormacionAcademica(id);
            if (respuesta.IsSuccess)
                return Request.CreateResponse(HttpStatusCode.OK, respuesta, "application/json");
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, respuesta.Message, "application/json");
        }
    }
}
