﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.WebAPI.Controllers
{
    [RoutePrefix("api/typetech")]
    public class TipoTecnologiaController : ApiController
    {
        DL_TipoTecnologia _services;
        public TipoTecnologiaController()
        {
            _services = new DL_TipoTecnologia();
        }

        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetTipoTecnologia()
        {
            var data = _services.GetTipoTecnologia();
            if (data != null)
                return Ok(data);
            else
                return NotFound();
        }

        [Route("byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetTipoTecnologiaById(int id)
        {
            var data = _services.GetTipoTecnologiaById(id);
            if (data != null)
                return Ok(data);
            else
                return NotFound();
        }

        [Route("create")]
        [HttpPost]
        public IHttpActionResult PostTipoTecnologia([FromBody] TipoTecnologia tipoTecnologia)
        {
            if (ModelState.IsValid && tipoTecnologia != null)
            {
                var data = _services.PostTipoTecnologia(tipoTecnologia);
                if (data.IsSuccess)
                    return Ok(data);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("update/{id}")]
        [HttpPut]
        public IHttpActionResult PutTipoTecnologia(int id,[FromBody] TipoTecnologia tipoTecnologia)
        {
            if (ModelState.IsValid && tipoTecnologia != null)
            {
                var data = _services.PutTipoTecnologia(id, tipoTecnologia);
                if (data.IsSuccess)
                    return Ok(data);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteTipoTecnologia(int id)
        {

            var data = _services.DeleteTipoTecnologia(id);
            if (data.IsSuccess)
                return Ok(data);
            else
                return BadRequest("");
        }
    }
}
