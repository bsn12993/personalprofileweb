﻿using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.WebAPI.Controllers
{
    [RoutePrefix("api/profile")]
    public class ProfileController : ApiController
    {
        DL_Profile _ProfileServices;
        public ProfileController()
        {
            _ProfileServices = new DL_Profile();
        }

        [Route("resume/{user}")]
        [HttpGet]
        public IHttpActionResult GetUsuarioPerfil(string user)
        {
            var respuesta = _ProfileServices.GetDataProfile(user);
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return BadRequest(respuesta.Message);
        }
        
        [Route("userskill/{id}")]
        [HttpGet]
        public IHttpActionResult GetUsuarioConocimientos(int id)
        {
            var user = _ProfileServices.GetUsuarioConocimiento(id);
            if (user != null)
                return Ok(user);
            else
                return BadRequest(user.Message);
        }
        [Route("userworkexperience/{id}")]
        [HttpGet]
        public IHttpActionResult GetUsuarioExperienciaLaboral(int id)
        {
            var user = _ProfileServices.GetUsuarioExperienciaLaboral(id);
            if (user != null)
                return Ok(user);
            else
                return BadRequest(user.Message);
        }

        [Route("usereducation/{id}")]
        [HttpGet]
        public IHttpActionResult GetUsuarioFormacionAcademica(int id)
        {
            var user = _ProfileServices.GetUsuarioFormacionAcademica(id);
            if (user != null)
                return Ok(user);
            else
                return BadRequest(user.Message);
        }

        [Route("userlenguages/{id}")]
        [HttpGet]
        public IHttpActionResult GetUsuarioIdiomas(int id)
        {
            var user = _ProfileServices.GetUsuarioIdiomas(id);
            if (user != null)
                return Ok(user);
            else
                return BadRequest(user.Message);
        }
    }
}
