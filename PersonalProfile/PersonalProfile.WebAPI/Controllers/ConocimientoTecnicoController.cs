﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PersonalProfile.WebAPI.Controllers
{
    [EnableCors(origins:"*",headers:"*",methods:"*")]
    [RoutePrefix("api/technicalskill")]
    public class ConocimientoTecnicoController : ApiController
    {
        DL_ConocimientosTecnicos _services = null;
        public ConocimientoTecnicoController()
        {
            _services = new DL_ConocimientosTecnicos();
        }

        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetConocimientosTecnicos()
        {
            var respuesta = _services.GetConocimientosTecnicos();
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return BadRequest(respuesta.Message);
        }

        [Route("byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetConocimientoTecnicoById(int id)
        {
            var respuesta = _services.GetConocimientoTecnicoById(id);
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return BadRequest(respuesta.Message);
        }

        [Route("create")]
        [HttpPost]
        public IHttpActionResult PostConocimientoTecnico([FromBody] ConocimientoTecnico conocimiento)
        {
            if (ModelState.IsValid && conocimiento != null)
            {
                var respuesta = _services.PostConocimientoTecnico(conocimiento);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest(respuesta.Message);
            }
            else
                return BadRequest("No es valido los datos");
        }

        [Route("update/{id}")]
        [HttpPut]
        public IHttpActionResult PutConocimientoTecnico(int id,[FromBody]ConocimientoTecnico conocimiento)
        {
            if (ModelState.IsValid && conocimiento != null)
            {
                var respuesta = _services.PutConocimientoTecnico(id, conocimiento);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest(respuesta.Message);
            }
            else
                return BadRequest("No es valido los datos");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteConocimientoTecnico(int id)
        {
            var respuesta = _services.DeleteConocimientoTecnicos(id);
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return BadRequest(respuesta.Message);
        }
    }
}
