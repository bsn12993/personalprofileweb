﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Util;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace PersonalProfile.WebAPI.Controllers
{
    [RoutePrefix("api/user")]
    public class UsuarioController : ApiController
    {
        DL_Usuario _services;

        public UsuarioController()
        {
            _services = new DL_Usuario();
        }
        #region Usuario
        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetUser()
        {
            var users = _services.GetUsuarios();
            if (users != null)
                return Ok(users);
            else
                return NotFound();
        }
        [Route("byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserById(int id)
        {
            var user = _services.GetUsuarioById(id);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }
        [Route("validate")]
        [HttpGet]
        public IHttpActionResult GetUserValidate(string correo, string contrasenia)
        {
            var user = _services.GetUsuarioByCorreoContrasenia(correo, contrasenia);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }
        [Route("create")]
        [HttpPost]
        public IHttpActionResult PostUser([FromBody] Usuario usuario)
        {
            if (ModelState.IsValid && usuario != null)
            {
                var respuesta = _services.PostUsuario(usuario);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("update/{id}")]
        [HttpPut]
        public IHttpActionResult PutUser(int id, [FromBody] Usuario usuario)
        {
            //var path = HttpContext.Current.Request.MapPath("~");
            //var a = FileUtil.ConvertBase64ToFile(usuario.imagen);
            //var Image = (Image)a.data;
            //Image.Save(HttpContext.Current.Server.MapPath("~/Images/" + usuario.nombre + "" + usuario.apellidoPaterno + "" + usuario.apellidoMaterno));
            if (ModelState.IsValid && usuario != null)
            {
                var respuesta = _services.PutUsuario(id, usuario);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return NotFound();
            }
            else
                return NotFound();
        }
        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteUser(int id)
        {
            var respuesta = _services.DeleteUsuario(id);
            if (respuesta.IsSuccess)
                return Ok();
            else
                return NotFound();
        }
        #endregion

        #region Usuario Conocimiento
        [Route("skills/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserSkills(int id)
        {
            var skills = _services.GetUsuarioConocimiento(id);
            if (skills != null)
                return Ok(skills);
            else
                return NotFound();
        }
        [Route("skills/byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserSkillsById(int id)
        {
            var skills = _services.GetUsuarioConocimientoById(id);
            if (skills != null)
                return Ok(skills);
            else
                return NotFound();
        }
        [Route("skills/create")]
        [HttpPost]
        public IHttpActionResult PostUserSkills([FromBody] UsuarioConocimiento usuarioConocimiento)
        {
            if (ModelState.IsValid && usuarioConocimiento != null)
            {
                var respuesta = _services.PostUsuarioConocimiento(usuarioConocimiento);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("skills/update/{id}")]
        [HttpPut]
        public IHttpActionResult PutUserSkills(int id,[FromBody] UsuarioConocimiento usuarioConocimiento)
        {
            if (ModelState.IsValid && usuarioConocimiento != null)
            {
                var respuesta = _services.PutUsuarioConocimiento(id, usuarioConocimiento);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("skills/delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteUserSkills(int id)
        {
            var respuesta = _services.DeleteUsuarioConocimiento(id);
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return NotFound();
        }
        #endregion

        #region Usuario Formacion 
        [Route("education/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserEducation(int id)
        {
            var education = _services.GetUsuarioFormacionAcademica(id);
            if (education != null)
                return Ok(education);
            else
                return NotFound();
        }
        [Route("education/byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserEducationById(int id)
        {
            var education = _services.GetUsuarioFormacionAcademicaById(id);
            if (education != null)
                return Ok(education);
            else
                return NotFound();
        }
        [Route("education/create")]
        [HttpPost]
        public IHttpActionResult PostUserEducation([FromBody] UsuarioFormacionAcademica usuarioFormacion)
        {
            if (ModelState.IsValid && usuarioFormacion != null)
            {
                var respuesta = _services.PostUsuarioFormacionAcademica(usuarioFormacion);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("education/update/{id}")]
        [HttpPut]
        public IHttpActionResult PutUserEducation(int id,[FromBody] UsuarioFormacionAcademica usuarioFormacion)
        {
            if (ModelState.IsValid && usuarioFormacion != null)
            {
                var respuesta = _services.PutUsuarioFormacionAcademica(id, usuarioFormacion);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("education/delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteUserEducation(int id)
        {
            var respuesta = _services.DeleteUsuarioFormacionAcademica(id);
            if (respuesta.IsSuccess)
                return Ok();
            else
                return NotFound();
        }
        #endregion

        #region Usuario Experiencia
        [Route("workexperiences/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserWorkExperiences(int id)
        {
            var experiences = _services.GetUsuarioExperienciaLaboral(id);
            if (experiences != null)
                return Ok(experiences);
            else
                return NotFound();
        }
        [Route("workexperiences/byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserWorkExperiencesById(int id)
        {
            var experiences = _services.GetUsuarioExperienciaLaboralById(id);
            if (experiences != null)
                return Ok(experiences);
            else
                return NotFound();
        }
        [Route("workexperiences/create")]
        [HttpPost]
        public IHttpActionResult PostUserWorkExperiences([FromBody] UsuarioExperienciaLaboral usuarioExperiencia)
        {
            if (ModelState.IsValid && usuarioExperiencia != null)
            {
                var respuesta = _services.PostUsuarioExperienciaLaboral(usuarioExperiencia);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("workexperiences/update/{id}")]
        [HttpPut]
        public IHttpActionResult PutUserWorkExperiences(int id, [FromBody] UsuarioExperienciaLaboral usuarioExperiencia)
        {
            if (ModelState.IsValid && usuarioExperiencia != null)
            {
                var respuesta = _services.PutUsuarioExperienciaLaboral(id, usuarioExperiencia);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("workexperiences/delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteUserWorkExperiences(int id)
        {
            var respuesta = _services.DeleteUsuarioExperienciaLaboral(id);
            if (respuesta.IsSuccess)
                return Ok();
            else
                return NotFound();
        }
        #endregion

        #region Usuario Idioma
        [Route("languages/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserLanguages(int id)
        {
            var languages = _services.GetUsuarioIdiomas(id);
            if (languages != null)
                return Ok(languages);
            else
                return NotFound();
        }
        [Route("languages/byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserLanguagesById(int id)
        {
            var languages = _services.GetUsuarioIdiomasById(id);
            if (languages != null)
                return Ok(languages);
            else
                return NotFound();
        }
        [Route("languages/create")]
        [HttpPost]
        public IHttpActionResult PostUserLanguages([FromBody] UsuarioIdioma usuarioIdioma)
        {
            if (ModelState.IsValid && usuarioIdioma != null)
            {
                var respuesta = _services.PostUsuarioIdiomas(usuarioIdioma);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("languages/update/{id}")]
        [HttpPut]
        public IHttpActionResult PutUserLanguages(int id, [FromBody] UsuarioIdioma usuarioIdioma)
        {
            if (ModelState.IsValid && usuarioIdioma != null)
            {
                var respuesta = _services.PutUsuarioIdiomas(id, usuarioIdioma);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("languages/delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteUserLanguages(int id)
        {
            var respuesta = _services.DeleteUsuarioIdiomas(id);
            if (respuesta.IsSuccess)
                return Ok();
            else
                return NotFound();
        }
        #endregion

        #region Usuario Proyectos
        [Route("projects/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserProjects(int id)
        {
            var projects = _services.GetUsuarioProyectos(id);
            if (projects != null)
                return Ok(projects);
            else
                return NotFound();
        }
        [Route("projects/byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserProjectsById(int id)
        {
            var projects = _services.GetUsuarioProyectosById(id);
            if (projects != null)
                return Ok(projects);
            else
                return NotFound();
        }
        [Route("projects/create")]
        [HttpPost]
        public IHttpActionResult PostUserProjects([FromBody] UsuarioProyectos usuarioProyecto)
        {
            if (ModelState.IsValid && usuarioProyecto != null)
            {
                var respuesta = _services.PostUsuarioProyectos(usuarioProyecto);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("projects/update/{id}")]
        [HttpPut]
        public IHttpActionResult PutUserProjects(int id, [FromBody] UsuarioProyectos usuarioProyecto)
        {
            if (ModelState.IsValid && usuarioProyecto != null)
            {
                var respuesta = _services.PutUsuarioProyectos(id, usuarioProyecto);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("projects/delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteUserProjects(int id)
        {
            var respuesta = _services.DeleteUsuarioProyectos(id);
            if (respuesta.IsSuccess)
                return Ok();
            else
                return NotFound();
        }
        #endregion

        #region Usuario Contacto
        [Route("contact/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserContact(int id)
        {
            var contacts = _services.GetUsuarioContacto(id);
            if (contacts != null)
                return Ok(contacts);
            else
                return NotFound();
        }
        [Route("contact/byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetUserContactById(int id)
        {
            var contacts = _services.GetUsuarioContactoById(id);
            if (contacts != null)
                return Ok(contacts);
            else
                return NotFound();
        }
        [Route("contact/create")]
        [HttpPost]
        public IHttpActionResult PostUserContact([FromBody] UsuarioContacto usuarioContacto)
        {
            if (ModelState.IsValid && usuarioContacto != null)
            {
                var respuesta = _services.PostUsuarioContacto(usuarioContacto);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("contact/update/{id}")]
        [HttpPut]
        public IHttpActionResult PutUserContact(int id, [FromBody] UsuarioContacto usuarioContacto)
        {
            if (ModelState.IsValid && usuarioContacto != null)
            {
                var respuesta = _services.PutUsuarioContacto(id, usuarioContacto);
                if (respuesta.IsSuccess)
                    return Ok(respuesta);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }
        [Route("contact/delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteUserContact(int id)
        {
            var respuesta = _services.DeleteUsuarioContacto(id);
            if (respuesta.IsSuccess)
                return Ok();
            else
                return NotFound();
        }
        #endregion
    }
}
