﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.WebAPI.Controllers
{
    [RoutePrefix("api/projects")]
    public class ProyectosController : ApiController
    {
        DL_Proyectos _services = null;
        public ProyectosController()
        {
            _services = new DL_Proyectos();
        }

        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetProyectos()
        {
            var respuesta = _services.GetProyectos();
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return BadRequest(respuesta.Message);
        }

        [Route("byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetProyectoById(int id)
        {
            var resultados = _services.GetProyectosById(id);
            if (resultados.IsSuccess)
                return Ok(resultados);
            else
                return BadRequest(resultados.Message);
        }

        [Route("create")]
        [HttpPost]
        public IHttpActionResult PostProyecto([FromBody] Proyectos proyecto)
        {
            if (ModelState.IsValid && proyecto != null)
            {
                var respuesta = _services.PostProyectos(proyecto);
                if (respuesta.IsSuccess)
                    return Ok();
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("update{id}")]
        [HttpPut]
        public IHttpActionResult PutProyecto(int id, [FromBody] Proyectos proyecto)
        {
            if (ModelState.IsValid && proyecto != null)
            {
                var respuesta = _services.PutProyecto(id, proyecto);
                if (respuesta.IsSuccess)
                    return Ok();
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteProyecto(int id)
        {
            var respuesta = _services.DeleteProyecto(id);
            if (respuesta.IsSuccess)
                return Ok();
            else
                return BadRequest("");
        }
        
        [Route("technologies/byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetProyectosTecnologiaById(int id)
        {
            var data = _services.GetTecnologiaById(id);
            if (data.IsSuccess)
                return Ok(data);
            else
                return BadRequest("");
        }
        [Route("technologies/byproject/{id}")]
        [HttpGet]
        public IHttpActionResult GetProyectosTecnologia(int id)
        {
            var resultado = _services.GetProyectoTecnologiasById(id);
            if (resultado.IsSuccess)
                return Ok(resultado);
            else
                return BadRequest(resultado.Message);
        }

        [Route("technologies/create")]
        [HttpPost]
        public IHttpActionResult PostProyectosTecnologia([FromBody] ProyectoTecnologia proyectoTecnologia)
        {
            if (ModelState.IsValid && proyectoTecnologia != null)
            {
                var data = _services.PostProyectoTecnologias(proyectoTecnologia);
                if (data.IsSuccess)
                    return Ok(data);
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("technologies/update/{id}")]
        [HttpPut]
        public IHttpActionResult PutProyectosTecnologia(int id,[FromBody] ProyectoTecnologia proyectoTecnologia)
        {
            if (ModelState.IsValid && proyectoTecnologia != null)
            {
                var data = _services.PutProyectoTecnologias(id, proyectoTecnologia);
                if (data.IsSuccess)
                    return Ok(data);
                else return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("technologies/delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteProyectosTecnologia(int id)
        {
            var data = _services.DeleteProyectoTecnologia(id);
            if (data.IsSuccess)
                return Ok(data);
            else
                return BadRequest("");
        }
    }
}
