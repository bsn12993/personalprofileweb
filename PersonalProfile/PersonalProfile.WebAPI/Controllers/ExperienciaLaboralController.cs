﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.WebAPI.Controllers
{
    [RoutePrefix("api/workexperience")]
    public class ExperienciaLaboralController : ApiController
    {
        DL_ExperienciaLaboral _services = null;
        public ExperienciaLaboralController()
        {
            _services = new DL_ExperienciaLaboral();
        }

        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetExperienciasLaborales()
        {
            var respuesta = _services.GetExperienciaLaboral();
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return BadRequest(respuesta.Message);
        }

        [Route("byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetExperienciaLaboralById(int id)
        {
            var respuesta = _services.GetExperienciaLaboralById(id);
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return BadRequest(respuesta.Message);
        }

        [Route("create")]
        [HttpPost]
        public IHttpActionResult PostExperienciaLaboral([FromBody] ExperienciaLaboral experienciaLaboral)
        {
            if (ModelState.IsValid && experienciaLaboral != null)
            {
                var respuesta = _services.PostExperienciaLaborial(experienciaLaboral);
                if (respuesta.IsSuccess)
                    return Ok();
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("update/{id}")]
        [HttpPut]
        public IHttpActionResult PutExperienciaLaboral(int id,[FromBody] ExperienciaLaboral experienciaLaboral)
        {
            if (ModelState.IsValid && experienciaLaboral != null)
            {
                var respuesta = _services.PutExperienciaLaboral(id, experienciaLaboral);
                if (respuesta.IsSuccess)
                    return Ok();
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteExperienciaLaboral(int id)
        {
            var respuesta = _services.DeleteExperienciaLaboral(id);
            if (respuesta.IsSuccess)
                return Ok();
            else
                return BadRequest("");
        }
    }
}
