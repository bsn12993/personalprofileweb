﻿using PersonalProfile.Core.EntityModels;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PersonalProfile.WebAPI.Controllers
{
    [RoutePrefix("api/languages")]
    public class IdiomasController : ApiController
    {
        DL_Idiomas _services = null;
        public IdiomasController()
        {
            _services = new DL_Idiomas();
        }

        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetIdiomas()
        {
            var respuesta = _services.GetIdiomas();
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return BadRequest(respuesta.Message);
        }

        [Route("byid/{id}")]
        [HttpGet]
        public IHttpActionResult GetIdiomaById(int id)
        {
            var respuesta = _services.GetIdiomaById(id);
            if (respuesta.IsSuccess)
                return Ok(respuesta);
            else
                return BadRequest(respuesta.Message);
        }

        [Route("create")]
        [HttpPost]
        public IHttpActionResult PostIdioma([FromBody] Idiomas idioma)
        {
            if (ModelState.IsValid && idioma != null)
            {
                var respuesta = _services.PostIdioma(idioma);
                if (respuesta.IsSuccess)
                    return Ok();
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("update/{id}")]
        [HttpPut]
        public IHttpActionResult PutIdioma(int id,Idiomas idioma)
        {
            if (ModelState.IsValid && idioma != null)
            {
                var respuesta = _services.PutIdioma(id, idioma);
                if (respuesta.IsSuccess)
                    return Ok();
                else
                    return BadRequest("");
            }
            else
                return BadRequest("");
        }

        [Route("delete/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteIdioma(int id)
        {
            var respuesta = _services.DeleteIdioma(id);
            if (respuesta.IsSuccess)
                return Ok();
            else
                return BadRequest("");
        }
    }
}
