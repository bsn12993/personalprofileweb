﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalProfile.Admin.Enum
{
    public enum EnumRequest
    {
        /// <summary>
        /// 
        /// </summary>
        Get=1,
        /// <summary>
        /// 
        /// </summary>
        GetById=2,
        /// <summary>
        /// 
        /// </summary>
        Create=3,
        /// <summary>
        /// 
        /// </summary>
        Update=4,
        /// <summary>
        /// 
        /// </summary>
        Delete=5
    }
}