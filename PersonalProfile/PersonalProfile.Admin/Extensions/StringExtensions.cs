﻿using PersonalProfile.Admin.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalProfile.Admin.Extensions
{
    public static class StringExtensions
    {
        public static String GetUrl(this string value, EnumRequest enumRequest)
        {
            switch (enumRequest)
            {
                case EnumRequest.Create:return string.Format("{0}{1}", value, "create");
                case EnumRequest.Delete: return string.Format("{0}{1}", value, "delete");
                case EnumRequest.Get: return string.Format("{0}{1}", value, "all");
                case EnumRequest.GetById: return string.Format("{0}{1}", value, "byid");
                case EnumRequest.Update: return string.Format("{0}{1}", value, "update");
            }
            return string.Empty;
        }
    }
}