﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace PersonalProfile.Admin.Client
{
    public class RESTClient
    {
        public JObject data { get; set; }
        public Response respuesta = null;

        public JObject GetData(string url = "", string method = "")
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            var content = sr.ReadToEnd();
                            data = (JObject)JsonConvert.DeserializeObject(content);
                        }
                    }
                }
            }catch(Exception e)
            {
                data = new JObject();
                data.Add("message", e.Message);
                data.Add("result", false);
                data.Add("data", null);
            }
            return data;
        }


        public Response SetData(string url = "", string method = "", object obj = null)
        {
            respuesta = new Response();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;
                request.ContentType = "application/json;charset=utf-8";
                Stream stream = request.GetRequestStream();
                var dataPost = JsonConvert.SerializeObject(obj);
                byte[] post = System.Text.Encoding.ASCII.GetBytes(dataPost);
                stream.Write(post, 0, post.Length);
                stream.Close();
                StreamReader sr = new StreamReader(request.GetResponse().GetResponseStream());
                var result = sr.ReadToEnd();

                respuesta = JsonConvert.DeserializeObject<Response>(result);
            }
            catch(Exception e)
            {
                respuesta.Result = null;
                respuesta.IsSuccess = false;
                respuesta.Message = e.Message;
            }
            return respuesta;
        }
    }
}