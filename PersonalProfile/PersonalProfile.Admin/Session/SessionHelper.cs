﻿using Newtonsoft.Json;
using PersonalProfile.Core.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace PersonalProfile.Admin.Session
{
    public class SessionHelper
    {
        /// <summary>
        /// verifica si el usuario actual se encuentra en sesión o no
        /// </summary>
        /// <returns></returns>
        public static bool ExistUserInSession()
        {
            return (HttpContext.Current.Session["usuario"] != null) ? true : false;
        }
        /// <summary>
        /// elimina la sesión actual del usuario
        /// </summary>
        public static void DestroyUserSession()
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session["user"] = null;
            HttpContext.Current.Session["email"] = null;
            HttpContext.Current.Session["image"] = null;
        }
        /// <summary>
        /// obtiene los datos del usuario actual en sesión
        /// </summary>
        /// <returns></returns>
        public static Usuario GetUser()
        {
            Usuario usuario = new Usuario();
            usuario = (Usuario)HttpContext.Current.Session["usuario"];
            return usuario;
        }
        /// <summary>
        /// guarda los datos de nuestro usuario en sesión
        /// </summary>
        /// <param name="usuario"></param>
        public static void AddUserToSession(Usuario usuario)
        {
            HttpContext.Current.Session["usuario"] = usuario;
        }
    }
}