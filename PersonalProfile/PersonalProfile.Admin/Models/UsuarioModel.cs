﻿using PersonalProfile.Core.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalProfile.Admin.Models
{
    public class UsuarioModel : Usuario
    {
        public HttpPostedFileBase file { get; set; }
    }
}