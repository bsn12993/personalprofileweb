﻿using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Admin.Interface
{
    public interface IServices
    {
        Task<Response> Get<T>(string url);
        Task<Response> GetById<T>(string url);
        Task<Response> Post<T>(string url, T model);
        Task<Response> Put<T>(string url, T model, int id);
        Task<Response> Delete<T>(string url, int id);
    }
}
