﻿using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalProfile.Admin.Interface
{
    public interface IUsuarioServices : IServices
    {
        Task<Response> GetUsuarioCorreoContrasenia(string correo, string contrasenia);
    }
}
