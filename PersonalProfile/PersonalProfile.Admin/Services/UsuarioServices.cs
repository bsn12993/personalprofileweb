﻿using Newtonsoft.Json;
using PersonalProfile.Admin.Client;
using PersonalProfile.Admin.Interface;
using PersonalProfile.Admin.Models;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PersonalProfile.Admin.Services
{
    public class UsuarioServices : IUsuarioServices
    {
        public UsuarioServices()
        {
            
        }
        ///api/user/all
        public async Task<Response> Get<T>(string url)
        {
            return await ApiService.GetInstance().GetList<T>(url);           
        }
        ///api/user/byid/{id}
        public async Task<Response> GetById<T>(string url)
        {
            var result = await ApiService.GetInstance().GetItem<T>(url);
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<Usuario>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }
        public async Task<Response> GetUsuarioCorreoContrasenia(string correo, string contrasenia)
        {
            var result = await ApiService.GetInstance().GetItem<Response>($"/api/user/validate?correo={correo}&contrasenia={contrasenia}");
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<Usuario>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result; 
        }
        ///api/user/create
        public async Task<Response> Post<T>(string url, T model)
        {
            return await ApiService.GetInstance().PostItem<T>(url, model);
        }
        ///api/user/update/
        public async Task<Response> Put<T>(string url, T model, int id)
        {
            return await ApiService.GetInstance().PutItem<T>(url, model, id);
        }
        ///api/user/delete/
        public async Task<Response> Delete<T>(string url, int id)
        {
            return await ApiService.GetInstance().DeleteItem<T>(url, id);
        }

    }
}