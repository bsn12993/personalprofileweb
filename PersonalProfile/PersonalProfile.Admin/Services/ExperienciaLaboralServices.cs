﻿using Newtonsoft.Json;
using PersonalProfile.Admin.Client;
using PersonalProfile.Admin.Interface;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PersonalProfile.Admin.Services
{
    public class ExperienciaLaboralServices : IServices
    {
        //api/workexperience/all
        public async Task<Response> Get<T>(string url)
        {
            var result = await ApiService.GetInstance().GetList<List<ExperienciaLaboral>>(url);
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<ExperienciaLaboral>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }
        //api/workexperience/byid/
        public async Task<Response> GetById<T>(string url)
        {
            var result = await ApiService.GetInstance().GetItem<ExperienciaLaboral>(url);
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<ExperienciaLaboral>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }
        //api/workexperience/create
        public async Task<Response> Post<T>(string url, T experiencia)
        {
            return await ApiService.GetInstance().PostItem<T>(url, experiencia);
        }
        //api/workexperience/update
        public async Task<Response> Put<T>(string url, T experiencia, int id)
        {
            return await ApiService.GetInstance().PutItem<T>(url, experiencia, id);
        }
        //api/workexperience/delete
        public async Task<Response> Delete<T>(string url, int id)
        {
            return await ApiService.GetInstance().DeleteItem<T>(url, id);
        }

    }
}