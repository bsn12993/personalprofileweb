﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PersonalProfile.Admin.Config;
using PersonalProfile.Admin.Models;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PersonalProfile.Admin.Services
{
    public class ApiService : HttpClient
    {
        private static ApiService instance;

        private ApiService() : base()
        {
            Timeout = TimeSpan.FromMilliseconds(15000);
            MaxResponseContentBufferSize = 256000;
            BaseAddress = new Uri(AppSettingsValues.EndPoint);
            DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        static ApiService() { }

        public static ApiService GetInstance()
        {
            if (instance == null) return new ApiService();
            else return instance;
        }

        public async Task<Response> GetList<T>(string url)
        {
            try
            {
                var response = await GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();                
                if (!response.IsSuccessStatusCode)
                {                 
                    var msg = JsonConvert.DeserializeObject<string>(result);
                    return new Response
                    {
                        IsSuccess = false,
                        Message = msg,
                        Result = null
                    };
                }
                var responseData = JsonConvert.DeserializeObject<Response>(result);
                return responseData;
            }
            catch (OperationCanceledException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Response> GetItem<T>(string url)
        {
            try
            {
                var response = await GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    var msg = JsonConvert.DeserializeObject<string>(result);
                    return new Response
                    {
                        IsSuccess = false,
                        Message = msg,
                        Result = null
                    };
                }
                var responseData = JsonConvert.DeserializeObject<Response>(result);
                return responseData;
            }
            catch (OperationCanceledException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Response> PostItem<T>(string url, T item)
        {
            try
            {
                var body = JsonConvert.SerializeObject(item);
                var content = new StringContent(body, Encoding.UTF8, "application/json");
                var response = await PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    var msg = JsonConvert.DeserializeObject<string>(result);
                    return new Response
                    {
                        IsSuccess = false,
                        Message = msg
                    };
                }
                var responseData = JsonConvert.DeserializeObject<Response>(result);
                return responseData;
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Response> PutItem<T>(string url, T item, int id)
        {
            try
            {
                var body = JsonConvert.SerializeObject(item);
                var content = new StringContent(body, Encoding.UTF8, "application/json");
                var response = await PutAsync($"{url}/{id}", content);
                var result = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    var msg = JsonConvert.DeserializeObject<string>(result);
                    return new Response
                    {
                        IsSuccess = false,
                        Message = msg
                    };
                }
                 
                var responseData = JsonConvert.DeserializeObject<Response>(result);
                return responseData;
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Response> DeleteItem<T>(string url, int id)
        {
            try
            {
                var response = await DeleteAsync($"{url}/{id}");
                var result = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    var msg = JsonConvert.DeserializeObject<string>(result);
                    return new Response
                    {
                        IsSuccess = false,
                        Message = msg
                    };
                }
                var responseData = JsonConvert.DeserializeObject<Response>(result);
                return responseData;
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

    }
}