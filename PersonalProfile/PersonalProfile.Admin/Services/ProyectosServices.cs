﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PersonalProfile.Admin.Client;
using PersonalProfile.Admin.Interface;
using PersonalProfile.Core.Domain;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PersonalProfile.Admin.Services
{
    public class ProyectosServices : IServices
    {
        //api/projects/all
        public async Task<Response> Get<T>(string url)
        {
            var result = await ApiService.GetInstance().GetList<List<T>>(url);
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<T>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }
        //api/projects/byid
        public async Task<Response> GetById<T>(string url)
        {
            var result = await ApiService.GetInstance().GetItem<T>(url);
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<T>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }
        //api/projects/create
        public async Task<Response> Post<T>(string url, T model)
        {
            return await ApiService.GetInstance().PostItem<T>(url, model);
        }
        //api/projects/update
        public async Task<Response> Put<T>(string url, T model, int id)
        {
            return await ApiService.GetInstance().PutItem<T>(url, model, id);
        }
        //api/projects/delete
        public async Task<Response> Delete<T>(string url, int id)
        {
            return await ApiService.GetInstance().DeleteItem<T>(url, id);
        }

    }
}