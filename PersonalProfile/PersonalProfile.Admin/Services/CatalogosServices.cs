﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PersonalProfile.Admin.Client;
using PersonalProfile.Admin.Models;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Data.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace PersonalProfile.Admin.Services
{
    public class CatalogosServices
    {
        public CatalogosServices()
        {
             
        }

        #region Conocimientos Tecnicos Services
        public async Task<Response> GetConocimientosTecnicos()
        {
            var result = await ApiService.GetInstance().GetList<List<ConocimientoTecnico>>("api/technicalskill/all");
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<ConocimientoTecnico>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result; 
        }
        public async Task<Response> GetConocimientoTecnicoById(int id)
        {
            var result = await ApiService.GetInstance().GetItem<ConocimientoTecnico>($"api/technicalskill/byid/{id}");
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<ConocimientoTecnico>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }

        public async Task<Response> PostConocimientosTecnicos(ConocimientoTecnico conocimiento)
        {
            return await ApiService.GetInstance().PostItem<ConocimientoTecnico>("api/technicalskill/create", conocimiento);
        }

        public async Task<Response> PutConocimientoTecnico(int id,ConocimientoTecnico conocimiento)
        {
            return await ApiService.GetInstance().PutItem<ConocimientoTecnico>("api/technicalskill/update", conocimiento, id);
        }

        public async Task<Response> DeleteConocimientosTecnicos(int id)
        {
            return await ApiService.GetInstance().DeleteItem<Response>("api/technicalskill/delete", id);
        }
        #endregion

        #region Contacto
        //public List<Contacto> GetContacto()
        //{
        //    var data = restClient.GetData("", "GET");
        //    if (data != null)
        //    {
        //        if ((bool)data["result"])
        //        {
        //            var json = JsonConvert.SerializeObject(data["data"]);
        //            lstContacto = JsonConvert.DeserializeObject<List<Contacto>>(json);
        //        }
        //    }
        //    return lstContacto;
        //}

        //public Contacto GetContactoById(int id)
        //{
        //    var data = restClient.GetData($"", "GET");
        //    if (data != null)
        //    {
        //        if ((bool)data["result"])
        //        {
        //            var json = JsonConvert.SerializeObject(data["data"]);
        //            contacto = JsonConvert.DeserializeObject<Contacto>(json);
        //        }
        //    }
        //    return contacto;
        //}

        //public Response PostContacto(Contacto contacto)
        //{
        //    return restClient.SetData("", "POST", contacto);
        //}

        //public Response PutContacto(int id, Contacto contacto)
        //{
        //    return restClient.SetData($"", "PUT", contacto);
        //}

        //public Response DeleteContacto(int id)
        //{
        //    return restClient.SetData($"", "DELETE", new object());
        //}
        #endregion

        #region Idiomas Services
        public async Task<Response> GetIdiomas()
        {
            var result = await ApiService.GetInstance().GetList<List<Idiomas>>("api/languages/all");
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<Idiomas>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }

        public async Task<Response> GetIdiomaById(int id)
        {
            var result = await ApiService.GetInstance().GetItem<Idiomas>($"api/languages/byid/{id}");
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<Idiomas>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }

        public async Task<Response> PostIdioma(Idiomas idioma)
        {
            return await ApiService.GetInstance().PostItem<Idiomas>("api/languages/create", idioma);
        }

        public async Task<Response> PutIdioma(int id, Idiomas idioma)
        {
            return await ApiService.GetInstance().PutItem<Idiomas>("api/languages/update", idioma, id);
        }

        public async Task<Response> DeleteIdioma(int id)
        {
            return await ApiService.GetInstance().DeleteItem<Response>("api/languages/delete", id);
        }
        #endregion

        #region Tecnologia Services
        public async Task<Response> GetTecnologia()
        {
            var result = await ApiService.GetInstance().GetList<List<TipoTecnologia>>("api/typetech/all");
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<TipoTecnologia>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }

        public async Task<Response> GetTecnologiaById(int id)
        {
            var result = await ApiService.GetInstance().GetItem<List<TipoTecnologia>>($"api/typetech/byid/{id}");
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<TipoTecnologia>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }

        public async Task<Response> PostTecnologia(TipoTecnologia tipoTecnologia)
        {
            return await ApiService.GetInstance().PostItem<TipoTecnologia>($"api/typetech/create", tipoTecnologia);
        }

        public async Task<Response> PutTecnologia(int id, TipoTecnologia tipoTecnologia)
        {
            return await ApiService.GetInstance().PutItem<TipoTecnologia>($"api/typetech/update", tipoTecnologia, id);
        }

        public async Task<Response> DeleteTecnologia(int id)
        {
            return await ApiService.GetInstance().DeleteItem<Response>($"api/typetech/delete", id);
        }
        #endregion

        
    }
}