﻿using Newtonsoft.Json;
using PersonalProfile.Admin.Interface;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PersonalProfile.Admin.Services
{
    public class ProyectoTecnologiaServices : IServices
    {
        //api/projects/technologies/delete
        public async Task<Response> Delete<T>(string url, int id)
        {
            return await ApiService.GetInstance().DeleteItem<T>(url, id);
        }

        public async Task<Response> Get<T>(string url)
        {
            throw new NotImplementedException();
        }
        //api/projects/technologies/byid/{id}
        public async Task<Response> GetById<T>(string url)
        {
            var result = await ApiService.GetInstance().GetList<T>(url);
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<T>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }
        //api/projects/technologies/byproject/{id}
        public async Task<Response> GetByProject<T>(string url)
        {
            var result = await ApiService.GetInstance().GetList<List<Proyectos>>(url);
            if (result.IsSuccess)
            {
                ProyectoTecnologiaVM proyectoTecnologiaVM = null;
                List<ProyectoTecnologiaVM> proyectoTecnologiaVMs = new List<ProyectoTecnologiaVM>();
                var model = JsonConvert.DeserializeObject<List<ProyectoTecnologia>>(result.Result.ToString());
                foreach (var m in model)
                {
                    proyectoTecnologiaVM = new ProyectoTecnologiaVM();
                    proyectoTecnologiaVM.ProyectoTecnologiaID = m.ProyectoTecnologiaID;
                    proyectoTecnologiaVM.ConocimientoTecnicoID = m.conocimientoTecnico.ConocimientoTecnicoID;
                    proyectoTecnologiaVM.nombre = m.conocimientoTecnico.nombre;
                    proyectoTecnologiaVMs.Add(proyectoTecnologiaVM);
                }
                result.Result = proyectoTecnologiaVMs;
                return result;
            }
            else
                return result;
        }
        //api/projects/technologies/create
        public async Task<Response> Post<T>(string url, T model)
        {
            return await ApiService.GetInstance().PostItem<T>(url, model);
        }
        //api/projects/technologies/updat
        public async Task<Response> Put<T>(string url, T model, int id)
        {
            return await ApiService.GetInstance().PutItem<T>(url, model, id);
        }
    }
}