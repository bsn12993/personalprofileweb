﻿using Newtonsoft.Json;
using PersonalProfile.Admin.Interface;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PersonalProfile.Admin.Services
{
    public class UsuarioIdiomaServices : IServices
    {
        ///api/user/languages/{idusuario}
        public async Task<Response> Get<T>(string url)
        {
            var result = await ApiService.GetInstance().GetList<List<T>>(url);
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<T>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }
        ///api/user/languages/byid/{idusuarioIdioma}
        public async Task<Response> GetById<T>(string url)
        {
            return await ApiService.GetInstance().GetItem<T>(url);
        }
        ///api/user/languages/create
        public async Task<Response> Post<T>(string url, T model)
        {
            return await ApiService.GetInstance().PostItem<T>(url, model);
        }
        ///api/user/languages/update
        public async Task<Response> Put<T>(string url, T model, int id)
        {
            return await ApiService.GetInstance().PutItem<T>(url, model, id);
        }
        ///api/user/languages/delete
        public async Task<Response> Delete<T>(string url, int id)
        {
            return await ApiService.GetInstance().DeleteItem<T>(url, id);
        }
    }
}