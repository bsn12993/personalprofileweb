﻿using Newtonsoft.Json;
using PersonalProfile.Admin.Interface;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PersonalProfile.Admin.Services
{
    public class TecnologiaServices : IServices
    {
        //api/typetech/delete
        public async Task<Response> Delete<T>(string url, int id)
        {
            return await ApiService.GetInstance().DeleteItem<T>(url, id);
        }
        //api/typetech/all
        public async Task<Response> Get<T>(string url)
        {
            var result = await ApiService.GetInstance().GetList<List<T>>(url);
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<T>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }
        //api/typetech/byid/
        public async Task<Response> GetById<T>(string url)
        {
            var result = await ApiService.GetInstance().GetItem<List<T>>(url);
            if (result.IsSuccess)
            {
                var model = JsonConvert.DeserializeObject<List<T>>(result.Result.ToString());
                result.Result = model;
                return result;
            }
            else
                return result;
        }
        //api/typetech/create
        public async Task<Response> Post<T>(string url, T model)
        {
            return await ApiService.GetInstance().PostItem<T>(url, model);
        }
        //api/typetech/update
        public async Task<Response> Put<T>(string url, T model, int id)
        {
            return await ApiService.GetInstance().PutItem<T>(url, model, id);
        }
    }
}