﻿using System.Web;
using System.Web.Optimization;

namespace PersonalProfile.Admin
{
    public class BundleConfig
    {
        // Para obtener más información sobre Bundles, visite http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Script/js").Include(
                        //jQuery 2.2.3
                        "~/Scripts/js/jquery-2.2.3.min.js",
                        //Bootstrap 3.3.6
                        "~/Scripts/js/bootstrap.min.js",
                        //SlimScroll
                        "~/Scripts/js/jquery.slimscroll.min.js",
                        //FastClick
                        "~/Scripts/js/fastclick.js",
                        //AdminLTE App
                        "~/Scripts/js/app.min.js",
                        //AdminLTE for demo purposes
                        "~/Scripts/js/demo_js",
                        //Datatables
                        "~/Scripts/DataTables/jquery.dataTables.min.js"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new StyleBundle("~/Content/css").Include(
                        //Bootstrap 3.3.6
                        "~/Content/css/bootstrap.min.css",
                        //Theme style
                        "~/Content/css/AdminLTE.min.css",
                        //AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load
                        "~/Content/css/_all_skins.min.css",
                        //Datatables
                        "~/Content/DataTables/css/jquery.dataTables.min.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
