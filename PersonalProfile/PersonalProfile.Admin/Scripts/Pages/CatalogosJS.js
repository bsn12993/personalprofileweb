﻿//OBJETOS

var conocimiento = {
    ConocimientoTecnicoID: 0,
    nombre: '',
    TipoTecnologiaID: 0
};

$(function () {

    $("#tbl_Conocimientos").DataTable({
        "language": {
            "url": urlDataTable
        }
    });


    $("#btn_agregar").click(function () {
        $("#modalCatalogo").modal("show");
        $("#modalTitleCatalogos").html(msgAdd);
        $("#id").val("");
        $("#nombre").val("");
        $("#TipoTecnologia").val("");
    });

    $(".btn_editar").click(function () {
        debugger;
        var id = $(this).val();
        GetConocimientosTecnicos("/Catalogos/GetConocimientosTecnicosById", id, "POST");
    })

    $("#form_setConocimiento").on('submit', function (evt) {
        evt.preventDefault();
        conocimiento.ConocimientoTecnicoID = ($("#id").val() != "") ? $("#id").val() : 0;
        conocimiento.nombre = $("#nombre").val();
        conocimiento.TipoTecnologiaID = ($("#TipoTecnologia").val() != "") ? $("#TipoTecnologia").val() : 0;
        var url = (conocimiento.ConocimientoTecnicoID == 0) ? "/Catalogos/PostConocimientosTecnicos" : "/Catalogos/PutConocimientosTecnicos";
        SetConocimientoTecnico(url, conocimiento, "POST");
         
    });

    $(".btn_eliminar").click(function () {
        debugger;
        var id = $(this).val();
        SetConocimientoTecnico("/Catalogos/DeleteConocimientosTecnicos", { id: id }, "POST");
    })

    GetTecnologia("/Catalogos/GetTecnologia", "POST");
    GetConocimiento("/Catalogos/GetConocimientosTecnico", "POST");
    GetExperienciaLaboral("/ExperienciaLaboral/GetExperienciaLaboral", "POST");

});

function GetConocimientosTecnicos(url, id, metodo) {
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: { id: id },
        success: function (data) {
            if (data.ok) {
                var json = JSON.parse(data.obj);
                $("#modalCatalogo").modal("show");
                $("#modalTitleCatalogos").html(msgEdit);
                $("#id").val(json.ConocimientoTecnicoID);
                $("#TipoTecnologia").val(json.TipoTecnologiaID);
                $("#nombre").val(json.nombre);
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").text(data.msg);
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

function SetConocimientoTecnico(url, obj, metodo) {
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: obj,
        beforeSend: function () {
            $.LoadingOverlay("show");
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            $("#modalCatalogo").modal("hide");
            $("#msjRespuesta").modal("show");
            if (data.ok) {
                $("#modalTitle").html("Información");
                $("#texto").text(data.msg);
                $("#btn-Ok").click(function () {
                    window.location.href = "/Catalogos/ConocimientosTecnicos";
                })
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").text(data.msg);
                    $("#btn-Ok").click(function () {
                        $("#msjRespuesta").modal("hide");
                    })
                }
            }
        },
        error: function (x, y, z) {
            $.LoadingOverlay("hide");
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

function GetTecnologia(url, metodo) {
    debugger;
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: {},
        success: function (data) {
            if (data.ok) {
                var json = JSON.parse(data.obj);
                $.each(json, function (k, v) {
                    $("#TipoTecnologia").append("<option value='" + v.TipoTecnologiaID + "'>" + v.nombre + "</option>")
                });
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#TipoTecnologia").append("<option value=''>" + data.obj + "</option>")
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

function GetConocimiento(url, metodo) {
    debugger;
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: {},
        success: function (data) {
            if (data.ok) {
                var json = JSON.parse(data.obj);
                $.each(json, function (k, v) {
                    $("#Conocimiento").append("<option value='" + v.ConocimientoTecnicoID + "'>" + v.nombre + "</option>")
                });
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#Conocimiento").append("<option value=''>" + data.obj + "</option>")
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

function GetExperienciaLaboral(url, metodo) {
    debugger;
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: {},
        success: function (data) {
            if (data.ok) {
                //var json = JSON.parse(data.obj);
                $.each(data.obj, function (k, v) {
                    $("#experiencia").append("<option value='" + v.ExperienciaLaboralID + "'>" + v.nombreEmpresa + "</option>")
                });
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#experiencia").append("<option value=''>" + data.obj + "</option>")
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}