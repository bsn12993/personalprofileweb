﻿
var usuarioExperienciaLaboral = {
    UsuarioExperienciaLaboralID: 0,
    usuarioID: 0,
    experienciaLaboralID: 0,
    experienciaLaboral: null,
    ProyectosID: 0,
    proyectos: null
}

var experienciaLaboral={
    ExperienciaLaboralID: 0,
    nombrePuesto: '',
    nombreEmpresa: '',
    actividad: '',
    fechaIngreso: '',
    fechaSalida: '',
    actual: 0
}

$(function () {

    //GetProyectos();

    $("#tbl_Experiencia").DataTable({
        "language": {
            "url": urlDataTable
        }
    });

    $("#btn_agregar").click(function () {
        $("#modalUsuarioExperiencia").modal("show");
        $("#modalTitleUsuarioExperienciaLaboral").html(msgAdd);
    });

    // Post Usuario Experiencia
    $("#form_setUsuarioExperienciaLaboral").on('submit', function (evt) {
        debugger;
        evt.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var metodo = form.attr("method");
        var val = ($("#UsuarioExperienciaLaboralID").val() != "") ? $("#UsuarioExperienciaLaboralID").val() : "0";
        var url = (val == 0) ? form.attr("action") : "/Usuario/PutUsuarioExperienciaLaboral";

        experienciaLaboral.actividad = $("#actividad").val();
        experienciaLaboral.actual = $("#actual").val();
        experienciaLaboral.ExperienciaLaboralID = $("#ExperienciaLaboralID").val();
        experienciaLaboral.fechaIngreso = $("#fechaIngreso").val();
        experienciaLaboral.fechaSalida = $("#fechaSalida").val();
        experienciaLaboral.nombreEmpresa = $("#nombreEmpresa").val();
        experienciaLaboral.nombrePuesto = $("#nombrePuesto").val();

        usuarioExperienciaLaboral.UsuarioExperienciaLaboralID = val;
        usuarioExperienciaLaboral.experienciaLaboralID = $("#ExperienciaLaboralID").val();
        usuarioExperienciaLaboral.usuarioID = $("#usuarioID").val();
        usuarioExperienciaLaboral.ProyectosID = $("#proyecto").val();
        usuarioExperienciaLaboral.experienciaLaboral = experienciaLaboral;

        $.ajax(url, {
            type: metodo,
            dataType: 'json',
            data: usuarioExperienciaLaboral,
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                $("#msjRespuesta").modal("show");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#texto").html(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    })
                }
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").text(data.obj);
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
            }
        });
    });

    // Delete Usuario Experiencia Laboral
    $(".btn_eliminarUsuarioExperiencia").click(function () {
        var id = $(this).val();
        $.ajax("/Usuario/DeleteUsuarioExperienciaLaboral", {
            type: "POST",
            dataType: 'json',
            data: { id: id },
            success: function (data) {
                if (data.ok) {
                    $("#msjLoading").modal("hide");
                    $("#msjRespuesta").modal("show");
                    $("#modalTitle").html("Información");
                    $("#texto").text(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    })
                }
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#msjRespuesta").modal("show");
                    $("#texto").text(data.obj);
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {

            }
        });
    });

    $("#actual").change(function () {
        if ($(this).is(":checked")) {
            $(this).val(1);
        }
        else {
            $(this).val(0);
        }
    });

});


function GetExperienciaLaboralById(id) {
    $.ajax('/Usuario/UsuarioExperienciaLaboralByID', {
        type: "POST",
        dataType: 'json',
        data: { id: id },
        beforeSend: function () {
            $.LoadingOverlay("show");
            //$("#msjLoading").modal("show");
        },
        success: function (data) {
            if (data.ok) {
                var json = JSON.parse(data.obj);
                $("#modalTitleUsuarioExperienciaLaboral").html(msgEdit);
                $("#nombreEmpresa").val(json.experienciaLaboral.nombreEmpresa);
                $("#nombrePuesto").val(json.experienciaLaboral.nombrePuesto);
                var fechaIngreso = (json.experienciaLaboral.fechaIngreso.split('T')[0]).split('-');
                $("#fechaIngreso").val(fechaIngreso[2] + "/" + fechaIngreso[1] + "/" + fechaIngreso[0]);
                var fechaSalida = (json.experienciaLaboral.fechaSalida.split('T')[0]).split('-');
                $("#fechaSalida").val(fechaSalida[2] + "/" + fechaSalida[1] + "/" + fechaSalida[0]);
                $("#actividad").val(json.experienciaLaboral.actividad);
                $("#proyecto").val(json.ProyectosID);
                if (json.experienciaLaboral.actual == 1) {
                    $("#actual").prop("checked", true);
                    $("#actual").val(json.experienciaLaboral.actual);
                }
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#msjRespuesta").modal("show");
                    $("#texto").text(data.obj);
                }
            } 
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {
            $.LoadingOverlay("hide");
            //$("#msjLoading").modal("hide");
        }
    });
}

function SetExperienciaLaboral(ExperienciaLaboralID, UsuarioExperienciaLaboralID) {
    $("#modalUsuarioExperiencia").modal("show");
    $("#modalTitleUsuarioExperienciaLaboral").html(msgAdd);
    $("#UsuarioExperienciaLaboralID").val(UsuarioExperienciaLaboralID);
    $("#ExperienciaLaboralID").val(ExperienciaLaboralID);
    if (UsuarioExperienciaLaboralID != 0) {       
        GetExperienciaLaboralById(UsuarioExperienciaLaboralID);
    } else {
        $("#nombreEmpresa").val("");
        $("#nombrePuesto").val("");
        $("#fechaIngreso").val("");
        $("#fechaSalida").val("");
        $("#actividad").val("");
        $("#proyecto").val("");
        $("#actual").prop("checked", false);
        $("#actual").val(0);
    }
    
}


function SetProyectoExperiencia(experienciaLaboralID, UsuarioExperienciaLaboralID) {
    $("#modalProyectoExperiencia").modal("show");
}

$('#datepicker').datepicker({
    autoclose: true
});