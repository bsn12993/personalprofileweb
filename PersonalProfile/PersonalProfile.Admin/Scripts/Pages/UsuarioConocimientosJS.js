﻿var usuarioConocimiento = {
    UsuarioConocimientoID: 0,
    nivel: 0,
    usuarioID: 0,
    conocimientoTecnicoID: 0
}

$(function () {

    $("#tbl_Conocimientos").DataTable({
        "language": {
            "url": urlDataTable
        }
    });

    $("#btn_agregar").click(function () {
        $("#modalUsuarioConocimiento").modal("show");
        $("#modalTitleUsuarioConocimiento").html(msgAdd);
        $("#UsuarioConocimientoID").val(0);
        $("#Conocimientos").val("");
        $("#nivel").val("");
    });

    // Post Usuario Conocimiento 
    $("#form_setUsuarioConocimiento").on('submit', function (evt) {
        evt.preventDefault();
        debugger;
        var form = $(this);
        var data = form.serialize();
        var metodo = form.attr("method");
        var val = ($("#UsuarioConocimientoID").val() != "") ? $("#UsuarioConocimientoID").val() : "0";
        var url = (val == 0) ? form.attr("action") : "/Usuario/PutUsuarioConocimientos";
        
        usuarioConocimiento.usuarioID = $("#UsuarioID").val();
        usuarioConocimiento.nivel = $("#nivel").val();
        usuarioConocimiento.conocimientoTecnicoID = $("#Conocimientos").val();
        usuarioConocimiento.UsuarioConocimientoID = $("#UsuarioConocimientoID").val();

        $.ajax(url, {
            type: metodo,
            dataType: 'json',
            data: usuarioConocimiento,
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                $("#msjRespuesta").modal("show");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#texto").html(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    })
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#texto").text(data.obj);
                    }
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
            }
        });
    });
    // GET Usuario Conocimiento
    $(".btn_editarUsuarioConocimiento").click(function () {
        var id = $(this).val();
        $.ajax("/Usuario/UsuarioConocimientosById", {
            type: "POST",
            dataType: 'json',
            data: { id: id },
            success: function (data) {
                if (data.ok) {
                    //var json = JSON.parse(data.obj);
                    $("#modalUsuarioConocimiento").modal("show");
                    $("#modalTitleUsuarioConocimiento").html(msgEdit);
                    $("#UsuarioConocimientoID").val(data.obj.UsuarioConocimientoID);
                    $("#Conocimientos").val(data.obj.conocimientoTecnicoID);
                    $("#nivel").val(data.obj.nivel);
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#msjRespuesta").modal("show");
                        $("#texto").text(data.obj);
                    }
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {

            }
        });
    });
    // Delete Usuario Conocimientos
    $(".btn_eliminarUsuarioConocimiento").click(function () {
        var id = $(this).val();
        $.ajax("/Usuario/DeleteUsuarioConocimientos", {
            type: "POST",
            dataType: 'json',
            data: { id: id },
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                $("#msjRespuesta").modal("show");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#texto").html(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    })
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#texto").text(data.obj);
                    }
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });
    });

    GetConocimientos("/Catalogos/GetConocimientosTecnico", "POST");
 

});


function GetConocimientos(url, metodo) {
    debugger;
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: { },
        success: function (data) {
            if (data.ok) {
                var json = JSON.parse(data.obj);
                $.each(json, function (k, v) {
                    $("#Conocimientos").append("<option value='" + v.ConocimientoTecnicoID + "'>" + v.nombre + "</option>");
                });
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#Conocimientos").append("<option value=''>" + data.obj + "</option>");
                }
            } 
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

 