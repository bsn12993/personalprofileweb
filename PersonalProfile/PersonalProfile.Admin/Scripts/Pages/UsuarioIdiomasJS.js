﻿var UsuarioIdioma = {
    UsuarioID: 0,
    UsuarioIdiomaID: 0,
    IdiomasID: 0,
    nivel: 0
};

$(function () {


    GetIdioma("/Catalogos/GetIdiomas", "POST");

    $("#tbl_Idioma").DataTable({
        "language": {
            "url": urlDataTable
        }
    });

    $("#btn_agregar").click(function () {
        $("#modalUsuarioIdioma").modal("show");
        $("#UsuarioIdiomaID").val(0);
        $("#modalTitleUsuarioIdiomas").html(msgAdd);
    });

    // Post Usuario Idiomas
    $("#form_setUsuarioIdiomas").on('submit', function (evt) {
        evt.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var metodo = form.attr("method");
        var val = ($("#UsuarioIdiomaID").val() != "") ? $("#UsuarioIdiomaID").val() : "0";
        var url = (val == 0) ? form.attr("action") : "/Usuario/PutUsuarioIdiomas";

        UsuarioIdioma.IdiomasID = $("#idiomas").val();
        UsuarioIdioma.UsuarioIdiomaID = $("#UsuarioIdiomaID").val();
        UsuarioIdioma.nivel = $("#nivel").val();

        $.ajax(url, {
            type: metodo,
            dataType: 'json',
            data: UsuarioIdioma,
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                $("#msjRespuesta").modal("show");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#texto").html(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    })
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#texto").html(data.obj);
                    }
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
            }
        });
    });
    // Get Usuario Idiomas
    $(".btn_editarUsuarioIdiomas").click(function () {
        var id = $(this).data("id");
        var ididioma = $(this).data("ididioma");
        var nivel = $(this).data("nivel");
        $("#modalUsuarioIdioma").modal("show");
        $("#modalTitleUsuarioIdiomas").html(msgEdit);
        $("#UsuarioIdiomaID").val(id);
        $("#idiomas").val(ididioma);
        $("#nivel").val(nivel);

       
    });
    // Delete Usuario Idiomas
    $(".btn_eliminarUsuarioIdiomas").click(function () {
        var id = $(this).val();
        $.ajax("/Usuario/DeleteIdiomas", {
            type: "POST",
            dataType: 'json',
            data: { id: id },
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                $("#msjRespuesta").modal("show");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#texto").html(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    });
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#msjRespuesta").modal("show");
                        $("#texto").html(data.obj);
                    }
                }
            },
            error: function (x, y, z) {

            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });
    });
 
})

function GetIdioma(url, metodo) {
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: {},
        success: function (data) {
            if (data.ok) {
                var json = JSON.parse(data.obj);
                $.each(json, function (k, v) {
                    $("#idiomas").append("<option value='" + v.IdiomasID + "'>" + v.nombre + "</option>")
                });
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#idiomas").append("<option value=''>" + data.obj + "</option>")
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

function SetIdioma(url, obj, metodo) {
    debugger;
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: obj,
        success: function (data) {
            $("#modalCatalogo").modal("hide");
            $("#msjRespuesta").modal("show");
            if (data.ok) {
                $("#texto").text(data.msg);
                $("#btn-Ok").click(function () {
                    window.location.href = "/Usuario/UsuarioIdiomas";
                })
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").html(data.msg);
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}