﻿
GetLanguageDefault();

$(".selectLanguage").click(function () {
    var val = $(this).data("lng");
    LanguageSetting(val);
});

$("#btn_changeSpanish").click(function () {
    var val = $(this).data("lng");
    LanguageSetting(val);
});

$("#btn_changeEnglish").click(function () {
    var val = $(this).data("lng");
    LanguageSetting(val);
});
 

function LanguageSetting(lng) {
    $.ajax("/ConfiguracionIdioma/SetupLanguage", {
        data: { "language": lng },
        type: "POST",
        dataType: "json",
        success: function (data) {
            if (data.ok) {
                window.location.reload();
            }
        },
        error: function (x, y, z) {

        },
        complete: function () {

        }
    });
}


function GetLanguageDefault() {
    $.ajax("/ConfiguracionIdioma/GetLanguageDefault", {
        data: {},
        type: "POST",
        dataType: "json",
        success: function (data) {
            if (data.ok) {
                if (data.obj == "es-MX") {
                    $("#tag_spanish").css("font-weight", "bold")
                } else {
                    $("#tag_english").css("font-weight", "bold")
                }
            }
        },
        error: function (x, y, z) {

        },
        complete: function () {

        }
    });
}
