﻿
var usuarioFormacionAcademica = {
    UsuarioFormacionAcademicaID: 0,
    usuarioID: 0,
    usuario: null,
    formacionAcademicaID: 0,
    formacionAcademica: null
}

var formacionAcademica = {
    formacionAcademicaID: 0,
    instituto: '',
    carrera: '',
    anioIngreso: 0,
    anioSalida: 0,
    titulo: 0
}

$(function () {

    $("#tbl_Formacion").DataTable({
        "language": {
            "url": urlDataTable
        }
    });

    // Post Usuario Formacion
    $("#form_setUsuarioFormacion").on('submit', function (evt) {
        debugger;
        evt.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var metodo = form.attr("method");
        var val = ($("#UsuarioFormacionAcademicaID").val() != "") ? $("#UsuarioFormacionAcademicaID").val() : "0";
        var url = (val == 0) ? form.attr("action") : "/Usuario/PutUsuarioFormacionAcademica";

        formacionAcademica.formacionAcademicaID = $("#formacionAcademicaID").val();
        formacionAcademica.instituto = $("#instituto").val();
        formacionAcademica.carrera = $("#carrera").val();
        formacionAcademica.anioIngreso = $("#anioIngreso").val();
        formacionAcademica.anioSalida = $("#anioSalida").val();
        if ($("#titulo").is(":checked")) {
            formacionAcademica.titulo = 1;
        }
        else {
            formacionAcademica.titulo = 0;
        }


        usuarioFormacionAcademica.UsuarioFormacionAcademicaID = val;
        usuarioFormacionAcademica.usuarioID = $("#usuarioID").val();
        usuarioFormacionAcademica.formacionAcademicaID = $("#formacionAcademicaID").val();
        usuarioFormacionAcademica.formacionAcademica = formacionAcademica;

        $.ajax(url, {
            type: metodo,
            dataType: 'json',
            data: usuarioFormacionAcademica,
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                //$("#msjLoading").modal("hide");
                $.LoadingOverlay("hide");
                $("#msjRespuesta").modal("show");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#texto").html(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    })
                }
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").html(data.obj);
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
            }
        });
    });

    // Delete Usuario Formacion Academica
    $(".btn_eliminarUsuarioFormacion").click(function () {
        var id = $(this).val();
        $.ajax("/Usuario/DeleteUsuarioFormacionAcademica", {
            type: "POST",
            dataType: 'json',
            data: { id: id },
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                $("#msjRespuesta").modal("show");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#msjRespuesta").modal("show");
                    $("#texto").text(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    });
                }
                else {
                    $("#msjRespuesta").modal("show");
                    $("#texto").text(data.obj);
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });
    });

    $("#titulo").change(function () {
        if ($(this).is(":checked")) {
            $(this).val(1);
        }
        else {
            $(this).val(0);
        }
    });
});

// Get Usuario Formacion Academica
function GetUsuarioFormacionAcademicaById(id) {
    $.ajax("/Usuario/UsuarioFormacionAcademicaById", {
        type: "POST",
        dataType: 'json',
        data: { id: id },
        success: function (data) {
            if (data.ok) {
                //var json = JSON.parse(data.obj);
                $("#modalUsuarioFormacion").modal("show");
                $("#UsuarioFormacionAcademicaID").val(data.obj.UsuarioFormacionAcademicaID);
                $("#usuarioID").val(data.obj.usuarioID);
                $("#formacionAcademicaID").val(data.obj.formacionAcademicaID);

                $("#instituto").val(data.obj.formacionAcademica.instituto);
                $("#carrera").val(data.obj.formacionAcademica.carrera);
                $("#anioIngreso").val(data.obj.formacionAcademica.anioIngreso);
                $("#anioSalida").val(data.obj.formacionAcademica.anioSalida);
                if (data.obj.formacionAcademica.titulo == 1) {
                    $("#titulo").prop("checked", true);
                }
                 
            }
            if (!data.existSession) {
                setTimeout(function () {
                    window.location.href = data.url;
                }, 1000);
            }
            //else {
            //    $("#msjRespuesta").modal("show");
            //    $("#texto").text(data.obj);
            //}
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

function SetFormacionAcademica(UsuarioFormacionAcademicaID, formacionAcademicaID) {
    $("#modalUsuarioFormacion").modal("show");
    if (UsuarioFormacionAcademicaID == "" && formacionAcademicaID == "") {
        $("#modalTitleUsuarioFormacionAcademica").html(msgAdd);
    }
    else {
        $("#modalTitleUsuarioFormacionAcademica").html(msgEdit);
    }
    $("#UsuarioFormacionAcademicaID").val(UsuarioFormacionAcademicaID);
    $("#formacionAcademicaID").val(formacionAcademicaID);
    GetUsuarioFormacionAcademicaById(UsuarioFormacionAcademicaID);
}
 