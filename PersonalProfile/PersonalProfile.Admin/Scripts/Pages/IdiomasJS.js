﻿var idiomas = {
    IdiomasID: 0,
    nombre: ''
};


$("#tbl_idiomas").DataTable({
    "language": {
        "url": urlDataTable
    }
})

$("#btn_agregar").click(function () {
    $("#modalIdiomas").modal("show");
    $("#modalTitleIdiomas").html(msgAdd);
    $("#idioma").val("");
    $("#IdiomasID").val("");
});


$(".btn_editar").click(function () {
    var id = $(this).val();
    var name = $(this).data("name");
    $("#modalIdiomas").modal("show");
    $("#modalTitleIdiomas").html(msgEdit);
    $("#IdiomasID").val(id);
    $("#idioma").val(name);
});



$("#form_SetIdioma").on('submit', function (evt) {
    evt.preventDefault();
    debugger;
    var form = $(this);
    var data = form.serialize();
    var metodo = form.attr("method");
    var val = ($("#IdiomasID").val() != "") ? $("#IdiomasID").val() : "0";
    var url = (val == 0) ? form.attr("action") : "/Catalogos/PutIdioma";

    idiomas.IdiomasID = val;
    idiomas.nombre = $("#idioma").val();

    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: idiomas,
        beforeSend: function () {
            $.LoadingOverlay("show");
            //$("#msjLoading").modal("show");
        },
        success: function (data) {
            //$("#msjLoading").modal("hide");
            $.LoadingOverlay("hide");
            $("#msjRespuesta").modal("show");
            if (data.ok) {
                $("#modalTitle").html("Información");
                $("#texto").html(data.msg);
                $("#btn-Ok").click(function () {
                    window.location.href = data.url;
                })
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").text(data.msg);
                    $("#btn-Ok").click(function () {
                        $("#msjRespuesta").modal("hide");
                    })
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {
            $.LoadingOverlay("hide");
            //$("#msjLoading").modal("hide");
        }
    });
});




$(".btn_eliminar").click(function () {
    var id = $(this).val();
    $.ajax('/Catalogos/DeleteIdioma', {
        type: 'Post',
        dataType: 'json',
        data: { id: id },
        beforeSend: function () {
            $.LoadingOverlay("show");
            //$("#msjLoading").modal("show");
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            //$("#msjLoading").modal("hide");
            if (data.ok) {
                $("#modalTitle").html("Información");
                $("#msjRespuesta").modal("show");
                $("#texto").html(data.obj);
                $("#btn-Ok").click(function () {
                    window.location.href = data.url;
                })
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#msjRespuesta").modal("show");
                    $("#texto").html(data.obj);
                }
            }
        },
        error: function (x, y, z) {

        },
        complete: function () {
            $.LoadingOverlay("hide");
        }
    });
});






