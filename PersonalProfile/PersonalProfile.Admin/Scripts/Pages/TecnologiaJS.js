﻿
var obj = {
    TipoTecnologiaID: 0,
    nombre: ''
};

$("#tbl_tipoTecnologia").DataTable({
    "language": {
        "url": urlDataTable
    }
});


$(".btn_eliminar").click(function () {
    var id = $(this).val();
    $.ajax('/Catalogos/DeleteTecnologia', {
        type: 'Post',
        dataType: 'json',
        data: { id: id },
        beforeSend: function () {
            $.LoadingOverlay("show");
            //$("#msjLoading").modal("show");
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            //$("#msjLoading").modal("hide");
            if (data.ok) {
                $("#msjRespuesta").modal("show");
                $("#modalTitle").html("Información");
                $("#texto").html(data.msg);
                $("#btn-Ok").click(function () {
                    window.location.href = data.url;
                })
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#msjRespuesta").modal("show");
                    $("#texto").html(data.obj);
                }
            }
        },
        error: function (x, y, z) {

        },
        complete: function () {
            $.LoadingOverlay("hide");
        }
    });
});


$(".btn_editar").click(function () {
    var name = $(this).data("name");
    var id = $(this).val();
    $("#modalTecnologia").modal("show");
    $("#modalTitleTecnologia").html(msgEdit);
    $("#TecnologiaID").val(id);
    $("#tecnologia").val(name);
    console.log(id + " - " + name);
})


$("#btn_agregar").click(function () {
    $("#modalTecnologia").modal("show");
    $("#modalTitleTecnologia").html(msgAdd);
    $("#TecnologiaID").val("");
    $("#tecnologia").val("");
});


$("#form_setUsuarioConocimiento").on('submit', function (evt) {
    evt.preventDefault();
    debugger;
    var form = $(this);
    var data = form.serialize();
    var metodo = form.attr("method");
    var val = ($("#TecnologiaID").val() != "") ? $("#TecnologiaID").val() : "0";
    var url = (val == 0) ? form.attr("action") : "/Catalogos/PutTecnologia";

    obj.TipoTecnologiaID = val;
    obj.nombre = $("#tecnologia").val();

    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: obj,
        beforeSend: function () {
            $.LoadingOverlay("show");
            //$("#msjLoading").modal("show");
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            //$("#msjLoading").modal("hide");
            $("#msjRespuesta").modal("show");
            if (data.ok) {
                $("#modalTitle").html("Información");
                $("#texto").html(data.msg);
                $("#btn-Ok").click(function () {
                    window.location.href = data.url;
                })
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").text(data.msg);
                    $("#btn-Ok").click(function () {
                        $("#msjRespuesta").modal("hide");
                    })
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {
            $.LoadingOverlay("hide");
            //$("#msjLoading").modal("hide");
        }
    });
});
