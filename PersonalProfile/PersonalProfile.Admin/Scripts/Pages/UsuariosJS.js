﻿
$(function () {
    // Login 
    $("#form_login").on("submit", function (evt) {
        evt.preventDefault();
        var form = $(this);
        var url = form.attr("action");
        var metodo = form.attr("method");
        var data = form.serialize();

        $.ajax(url, {
            type: metodo,
            dataType: 'json',
            data: data,
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                //$("#msjLoading").modal("hide");
                $.LoadingOverlay("hide");
                //$("#msjRespuesta").modal("show");
                if (data.ok) {
                    window.location.href = data.url;
                }
                else {
                    $("#msjRespuesta").modal("show");
                    $("#texto").html(data.msg); console.log(data.msg);
                    $("#btn-Ok").click(function () {
                        $("#msjRespuesta").modal("hide");
                    });
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                $("#texto").html("");
            }
        });
    });

    // Editar Usuario 
    $("#form_editarUsuario").on("submit", function (evt) {
        evt.preventDefault();
        var form = $(this);
        var url = form.attr("action");
        var metodo = form.attr("method");
        var data = new FormData(this);
        //var data = form.serialize();
        //var formData = new FormData(document.getElementById("imagen"));

        $.ajax(url, {
            type: metodo,
            dataType: 'json',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $("#modalTitle").html("Información");
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                if (data.ok) {
                    $("#msjRespuesta").modal("show");
                    $("#texto").html(data.msg);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    })
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#msjRespuesta").modal("show");
                        $("#texto").html(data.msg);
                    }
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
            }
        });
    });

    // Registro Usuario
    $("#form_usuarioRegistro").on("submit", function (evt) {
        evt.preventDefault();
        var form = $(this);
        var url = form.attr("action");
        var metodo = form.attr("method");
        var data = form.serialize();

        var nombre = $("#nombre").val();
        var apellidoPaterno = $("#apellidoPaterno").val();
        var apellidoMaterno = $("#apellidoMaterno").val();
        var correo = $("#correo").val();
        var correoValido = $("#correo").data("correoValido");
        var contrasenia = $("#contrasenia").val();
        var contraseniaValida = $("#contrasenia").data("contraseniaValida");
        var conContrasenia=$("#confirmarContrasenia").val();
        var confirmarContrasenia = $("#confirmarContrasenia").data("contraseniaConfirmarValida");
        var perfil = $("#perfil").val();

        var contraseniasIguales = false;
        if (contraseniaValida == 1 && confirmarContrasenia == 1) {
            if (contrasenia == conContrasenia) contraseniasIguales = true;
            else contraseniasIguales = false;
        }
        else {
            contraseniasIguales = false;
        }

        if (nombre != "" && apellidoPaterno != "" && apellidoMaterno != "" &&
            (correo != "" && correoValido != 0) &&
            (contrasenia != "" && contraseniaValida!=0) &&
            (conContrasenia != "" && confirmarContrasenia != 0) && perfil != "" && contraseniasIguales) {
            $.ajax(url, {
                type: metodo,
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    $.LoadingOverlay("show");
                    //$("#msjLoading").modal("show");
                },
                success: function (data) {
                    //$("#msjLoading").modal("hide");
                    $.LoadingOverlay("hide");
                    $("#msjRespuesta").modal("show");
                    if (data.ok) {
                        $("#texto").html(data.msg);
                        $("#btn-Ok").click(function () {
                            window.location.href = data.url;
                        })      
                    }
                    else {
                        $("#texto").html(data.msg);
                    }
                },
                error: function (x, y, z) {
                    $("#msjRespuesta").modal("show");
                    $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
                },
                complete: function () {
                    $.LoadingOverlay("hide");
                    //$("#msjLoading").modal("hide");
                }
            });
        }
        else {
            var msg = "";
            if (nombre == "") msg += "Ingresa el nombre</br>";
            if (apellidoPaterno == "") msg += "Ingresa el apellidoPaterno</br>";
            if (apellidoMaterno == "") msg += "Ingresa el apellidoMaterno</br>";
            if (correoValido == 0) msg += "Ingresa el correoValido</br>";
            if (contrasenia == "") msg += "Ingresa el contrasenia</br>";
            if (contraseniaValida == 0) msg += "Ingresa el contraseniaValida</br>";
            if (conContrasenia == "") msg += "Ingresa el conContrasenia</br>";
            if (confirmarContrasenia == 0) msg += "Ingresa el confirmarContrasenia</br>";
            if (perfil == "") msg += "Ingresa el perfil</br>";
            if (!contraseniasIguales) msg += "Ingresa el contraseniasIguales</br>";
            $("#msjRespuesta").modal("show");
            $("#texto").html(msg);
            $("#btn-Ok").click(function () {
                $("#msjRespuesta").modal("hide");
            })  
        }
    })

    $("#btn_viewProfile").click(function () {
        debugger;
        var url = $(this).data("url");
        $("#msjViewProfile").modal("show");
        $("#contenido").html(url);
        $(".tools").html('<a href="' + url + '" class="btn" target="_blank">Ver</a>');
        $(".tools").append('<a href="/Usuario/GetPDFFile" id="btn_downloadFile" class="btn">Descargar CV</a>');
    });
});

 
function copiarAlPortapapeles(id_elemento) {
    var aux = document.createElement("input");
    aux.setAttribute("value", document.getElementById(id_elemento).innerHTML);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
}

 
