﻿
var usuarioProyecto = {
    UsuarioProyectosID: 0,
    usuarioID: 0,
    proyectosID: 0,
    proyectos: null,
    experienciaLaboralID: 0,
    experienciaLaboral: null
}

var proyecto = {
    ProyectosID: 0,
    nombre: '',
    descripcionProyecto: ''
}

var proyectoTecnologia = {
    ProyectoTecnologiaID: 0,
    ProyectosID: 0,
    ConocimientoTecnicoID: 0
}

$(function () {

    $("#tbl_Proyectos").DataTable({
        "language": {
            "url": urlDataTable
        }
    });

    $("#btn_agregar").click(function () {
        $("#modalUsuarioProyecto").modal("show");
        $("#modalTitleUsuarioProyecto").html(msgAdd);
        $("#UsuarioProyectosID").val(0);
        $("#nombre").val("");
        $("#descripcion").val("");
        $("#experiencia").val("");
    });

    // Post Usuario Proyecto
    $("#form_setUsuarioProyecto").on('submit', function (evt) {
        evt.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var metodo = form.attr("method");
        var val = ($("#UsuarioProyectosID").val() != "") ? $("#UsuarioProyectosID").val() : "0";
        var url = (val == 0) ? form.attr("action") : "/Usuario/PutUsuarioProyectos";

        proyecto.nombre = $("#nombre").val();
        proyecto.descripcionProyecto = $("#descripcion").val();
        proyecto.ProyectosID = $("#ProyectosID").val();

        usuarioProyecto.proyectosID = $("#ProyectosID").val();
        usuarioProyecto.usuarioID = $("#UsuarioID").val();
        usuarioProyecto.UsuarioProyectosID = $("#UsuarioProyectosID").val();
        usuarioProyecto.proyectos = proyecto;
        usuarioProyecto.experienciaLaboralID = $("#experiencia").val();

        $.ajax(url, {
            type: metodo,
            dataType: 'json',
            data: usuarioProyecto,
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                $("#msjRespuesta").modal("show");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#texto").html(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    })
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#texto").html(data.obj);
                    }
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
            }
        });
    });
    // Get Usuario Proyectos
    $(".btn_editarUsuarioProyectos").click(function () {
        var id = $(this).val();
        $.ajax("/Usuario/UsuarioProyectosById", {
            type: "POST",
            dataType: 'json',
            data: { id: id },
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                if (data.ok) {
                    //var json = JSON.parse(data.obj);
                    $("#modalTitleUsuarioProyecto").html(msgEdit);
                    $("#msjLoading").modal("hide");
                    $("#modalUsuarioProyecto").modal("show");
                    $("#UsuarioProyectosID").val(data.obj.UsuarioProyectosID);
                    $("#UsuarioID").val(data.obj.usuarioID);
                    $("#ProyectosID").val(data.obj.proyectosID);
                    $("#nombre").val(data.obj.proyectos.nombre);
                    $("#descripcion").val(data.obj.proyectos.descripcionProyecto);
                    $("#experiencia").val(data.obj.experienciaLaboralID);
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#msjRespuesta").modal("show");
                        $("#texto").html(data.obj);
                    }
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
            }
        });
    });
    // Delete Usuario Proyectos
    $(".btn_eliminarUsuarioProyectos").click(function () {
        var id = $(this).val();
        $.ajax("/Usuario/DeleteUsuarioProyectos", {
            type: "POST",
            dataType: 'json',
            data: { id: id },
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#msjRespuesta").modal("show");
                    $("#texto").html(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.href = data.url;
                    })
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#msjRespuesta").modal("show");
                        $("#texto").html(data.obj);
                    }
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
            }
        });
    });

    GetProyectos();
})


function GetProyectos() {
    $.ajax("/Usuario/GetUsuarioProyectos", {
        type: "POST",
        dataType: 'json',
        data: { },
        success: function (data) {
            if (data.ok) {
                //var json = JSON.parse(data.obj);
                $.each(data.obj, function (k, v) {
                    $("#proyecto").append("<option value='" + v.ProyectosID + "'>" + v.nombre + "</option>");
                });
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#msjRespuesta").modal("show");
                    $("#texto").html(data.obj);
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}