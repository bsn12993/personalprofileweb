﻿
//Se utiliza para que el campo de texto solo acepte letras
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
    especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial){
        //alert('Tecla no aceptada');
        return false;
    }
}

//Se utiliza para que el campo de texto solo acepte numeros
function SoloNumeros(evt) {
    if (window.event) {//asignamos el valor de la tecla a keynum
        keynum = evt.keyCode; //IE
    }
    else {
        keynum = evt.which; //FF
    }
    //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
    if ((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6) {
        return true;
    }
    else {
        return false;
    }
}

function validar_email(email) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

function validarCorreo(email,clase,id) {
    if (validar_email(email)) {
        $(clase).addClass("text-success");
        $(clase).removeClass("text-danger");
        $(clase).html("Formato correcto");
        $("#" + id).data("correoValido", 1);
    }
    else {
        $(clase).removeClass("text-success");
        $(clase).addClass("text-danger");
        $(clase).html("Formato incorrecto");
        $("#" + id).data("correoValido", 0);
    }
    var d = $(clase).data("correoValido");
}

//function validarCorreoConfirmar(email, clase,id) {
//    if (validar_email(email)) {
//        $(clase).addClass("text-success");
//        $(clase).removeClass("text-danger");
//        $(clase).html("Formato correcto");
//        $("#" + id).data("contraseniaConfirmarValida", 1);
//    }
//    else {
//        $(clase).removeClass("text-success");
//        $(clase).addClass("text-danger");
//        $(clase).html("Formato incorrecto");
//        $("#" + id).data("contraseniaConfirmarValida", 0);
//    }
//    var d = $(clase).data("contraseniaConfirmarValida");
//}




function validarContrasenia(password,clase,id) {
    var RegExPattern = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;
    if ((password.match(RegExPattern)) && (password.value != '')) {
        $(clase).addClass("text-success");
        $(clase).removeClass("text-danger");
        $(clase).html("Formato correcto");
        $("#" + id).data("contraseniaValida", 1);
    } else {
        $(clase).removeClass("text-success");
        $(clase).addClass("text-danger");
        $(clase).html("Formato incorrecto");
        $("#" + id).data("contraseniaValida", 0);
    }
    var d = $(clase).data("contraseniaValida");
}


function validarConfirmarContrasenia(password, clase, id) {
    var RegExPattern = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;
    if ((password.match(RegExPattern)) && (password.value != '')) {
        $(clase).addClass("text-success");
        $(clase).removeClass("text-danger");
        $(clase).html("Formato correcto");
        $("#" + id).data("contraseniaConfirmarValida", 1);
    } else {
        $(clase).removeClass("text-success");
        $(clase).addClass("text-danger");
        $(clase).html("Formato incorrecto");
        $("#" + id).data("contraseniaConfirmarValida", 0);
    }
    var d = $(clase).data("contraseniaConfirmarValida");
}

function validarArchivo() {
    var fileInput = document.getElementById('file');
    var filePath = fileInput.value;
    var allowedExtensions = /(.jpg|.jpeg)$/i;
    if (!allowedExtensions.exec(filePath)) {
        //alert('Please upload file having extensions .jpeg/.jpg only.');
        $("#msjRespuesta").modal("show");
        $("#texto").html("Archivos permitidos solo con extensiones .jpeg/.jpg");
        fileInput.value = '';
        return false;
    } else {
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //document.getElementById('imagePreview').innerHTML = '<img src="' + e.target.result + '"/>';
                $("#msjLImagePreview").modal("show");
                $("#imgPreview").html('<img src="' + e.target.result + '"/>')
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}




//Usuario
function validarUsuario(form) {
    //"UsuarioID=0&perfil=&imagen=%2FImages%2Fprofile.jpg&nombre=&apellidoPaterno=&apellidoMaterno=&contrasenia=&correo=&telefono=&fechaNacimiento=01%2F01%2F0001&direccion=&descripcion="
    var data = form.split("&");
    var nombre = data[0].split("=")[1];
    var apellidoP = "";
    var apellidoM = "";
    var contrasenia = "";
    var correo = "";
    var telefono = "";
    var fechaNacimiento = "";
    var direccion = "";
    
}