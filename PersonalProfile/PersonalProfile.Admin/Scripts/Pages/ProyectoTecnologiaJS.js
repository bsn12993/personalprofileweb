﻿
var proyectoTecnologia = {
    ProyectoTecnologiaID: 0,
    ProyectosID: 0,
    ConocimientoTecnicoID: 0
}

$(function () {


    GetListaProyectoTipoTecnologia("/Usuario/GetProyectoTecnologia", "POST", $("#proyectosID").val());

    $("#form_SetProyectoTecnologia").on('submit', function (evt) {
        evt.preventDefault();
        debugger;
        var form = $(this);
        var data = form.serialize();
        var metodo = form.attr("method");
        var val = ($("#ProyectoTecnologiaID").val() != "") ? $("#ProyectoTecnologiaID").val() : "0";
        var url = (val == 0) ? form.attr("action") : "/Usuario/PutProyectoTecnologia";

        proyectoTecnologia.ProyectoTecnologiaID = val;
        proyectoTecnologia.ConocimientoTecnicoID = $("#Conocimiento").val();
        proyectoTecnologia.ProyectosID = $("#proyectosID").val();

        $.ajax(url, {
            type: metodo,
            dataType: 'json',
            data: proyectoTecnologia,
            beforeSend: function () {
                $.LoadingOverlay("show");
                //$("#msjLoading").modal("show");
            },
            success: function (data) {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
                $("#msjRespuesta").modal("show");
                if (data.ok) {
                    $("#modalTitle").html("Información");
                    $("#texto").html(data.obj);
                    $("#btn-Ok").click(function () {
                        window.location.reload();
                    })
                }
                else {
                    if (!data.existSession) {
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 1000);
                    }
                    else {
                        $("#texto").text(data.obj);
                    }
                }
            },
            error: function (x, y, z) {
                $("#msjRespuesta").modal("show");
                $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
            },
            complete: function () {
                $.LoadingOverlay("hide");
                //$("#msjLoading").modal("hide");
            }
        });
    })

})



function DeleteProyectoTecnologia(id) {
    $.ajax('/Usuario/DeleteProyectoTecnologia', {
        type: 'POST',
        dataType: 'json',
        data: { id: id },
        beforeSend: function () {
            $.LoadingOverlay("show");
            //$("#msjLoading").modal("show");
        },
        success: function (data) {
            $.LoadingOverlay("hide");
            //$("#msjLoading").modal("hide");
            $("#msjRespuesta").modal("show");
            if (data.ok) {
                $("#modalTitle").html("Información");
                $("#btn-Ok").click(function () {
                    window.location.reload();
                });
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").text(data.obj);
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {
            $.LoadingOverlay("hide");
            //$("#msjLoading").modal("hide");
        }
    });
}
 

function GetProyectos(url, metodo) {
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: {},
        success: function (data) {
            if (data.ok) {
                $("#nombre").val();
                $("#descripcion").val();
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#nombre").val("");
                    $("#descripcion").val("");
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

function GetProyectoTipoTecnologia(url, metodo, proyectoid) {
    debugger;
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: { id: proyectoid },
        success: function (data) {
            if (data.ok) {
                var json = JSON.parse(data.obj);
                $("#Conocimiento").val(json.ConocimientoTecnicoID);
                $("#ProyectoTecnologiaID").val(json.ProyectoTecnologiaID);
                $("#proyectosID").val(json.ProyectosID);
            }
            else {
                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").text(data.obj);
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

function GetListaProyectoTipoTecnologia(url, metodo, proyectoid) {
 
    $.ajax(url, {
        type: metodo,
        dataType: 'json',
        data: { id: proyectoid },
        success: function (data) {
            if (data.ok) {
                var json = JSON.parse(data.obj);
                $("#tbl_proyectoTecnologia").DataTable({
                    "data": json,
                    "columns": [
                        { "data": "nombre" },
                        {
                            "render": function (data, type, row, meta) {
                                return "<div class='btn-group'>" +
                                        "<button type='button' class='btn btn-default btn-flat'>Action</button>" +
                                        "<button type='button' class='btn btn-default btn-flat dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>" +
                                        "<span class='caret'></span>" +
                                            "<span class='sr-only'>Toggle Dropdown</span>" +
                                        "</button>" +
                                        "<ul class='dropdown-menu' role='menu'>" +
                                    "<li><button class='btn btn-default btn-block' onclick='SetProyectoTecnologia(" + row.ConocimientoTecnicoID + "," + row.ProyectoTecnologiaID + ")'>Editar Tecnologia</button></li>" +
                                            "<li><button class='btn btn-default btn-block' onclick='DeleteProyectoTecnologia(" + row.ProyectoTecnologiaID + ")'>Eliminar Tecnologia</button></li>" +
                                        "</ul>" +
                                    "</div>";
                            }
                        }

                    ],
                    "language": {
                        "url": urlDataTable
                    }
                });
            }
            else {

                $("#tbl_proyectoTecnologia").DataTable({
                    "language": {
                        "url": urlDataTable
                    }
                });

                if (!data.existSession) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
                else {
                    $("#texto").text(data.obj);
                }
            }
        },
        error: function (x, y, z) {
            $("#msjRespuesta").modal("show");
            $("#texto").html(x.error + " - " + x.status + " - " + x.statusText + " - " + x.statusCode);
        },
        complete: function () {

        }
    });
}

function SetProyectoTecnologia(TipoTecnologiaID, ProyectoTecnologiaID) {
    $("#modalProyectoTecnologia").modal("show");
    if (TipoTecnologiaID == "" && ProyectoTecnologiaID == "") {
        $("#modalTitleProyectoTecnologia").html(msgAdd);
    }
    else {
        $("#modalTitleProyectoTecnologia").html(msgEdit);
    }
    $("#Conocimiento").val(TipoTecnologiaID);
    $("#ProyectoTecnologiaID").val(ProyectoTecnologiaID);
}
