﻿using Newtonsoft.Json;
using PersonalProfile.Admin.Interface;
using PersonalProfile.Admin.Services;
using PersonalProfile.Admin.Session;
using PersonalProfile.Core.Domain;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PersonalProfile.Admin.Controllers
{
    public partial class CatalogosController : Controller
    {
        CatalogosServices _catalogosServices = null;
        private IServices iIdiomas = null;
        private IServices iConocimientos = null;
        private IServices iTecnologias = null;

        public CatalogosController()
        {
            _catalogosServices = new CatalogosServices();
            iIdiomas = new IdiomasServices();
            iConocimientos = new ConocimientosServices();
            iTecnologias = new TecnologiaServices();
        }

        // GET: Catalogos
        #region Conocimientos Tecnicos
        public virtual async Task<ActionResult> ConocimientosTecnicos()
        {
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iConocimientos.Get<ConocimientoTecnico>("api/technicalskill/all");
                if (data.IsSuccess)
                    return View((List<ConocimientoTecnico>)data.Result);
                else
                    return View(new List<ConocimientoTecnico>());
            }
            else
                return RedirectToAction("TimeOut", "Usuario");  
        }

        [HttpPost]
        public virtual async Task<JsonResult> GetConocimientosTecnico()
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iConocimientos.Get<ConocimientoTecnico>("api/technicalskill/all");
                if (data.IsSuccess)
                {
                    if (((List<ConocimientoTecnico>)data.Result).Count <= 0)
                        json.Data = new { ok = false, existSession = true, obj = "No se encontrarón registros" };
                    else
                    {
                        var conocimientos = JsonConvert.SerializeObject(data.Result);
                        json.Data = new { ok = data.IsSuccess, existSession = true, obj = conocimientos };
                    }
                }
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> GetConocimientosTecnicosById(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iConocimientos.GetById<ConocimientoTecnico>($"api/technicalskill/byid/{id}");
                if (data.IsSuccess)
                {
                    var obj = JsonConvert.SerializeObject(data.Result);
                    json.Data = new { ok = true, obj = obj, existSession = true, url = Url.Action("ConocimientosTecnicos", "Catalogos") };
                }
                else
                {
                    json.Data = new { ok = false, existSession = true, obj = "" };
                }
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> PostConocimientosTecnicos(ConocimientoTecnico modelo)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iConocimientos.Post<ConocimientoTecnico>("api/technicalskill/create", modelo);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message };
                }
                else
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message };
                }
            }
            else
                json.Data = new { ok = false, msg = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> PutConocimientosTecnicos(ConocimientoTecnico modelo)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iConocimientos.Put<ConocimientoTecnico>($"api/technicalskill/update", modelo, modelo.ConocimientoTecnicoID);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message };
                }
                else
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message };
                }
            }
            else
                json.Data = new { ok = false, msg = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> DeleteConocimientosTecnicos(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iConocimientos.Delete<Response>("api/technicalskill/delete", id);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message };
                }
                else
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = "" };
                }
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") }; 
            return json;
        }
        #endregion

        #region Contacto
        public virtual async Task<ActionResult> Contacto()
        {
            
            return View();
        }
        #endregion

        #region Tecnologia
        public virtual async Task<ActionResult> Tecnologia()
        {
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iTecnologias.Get<TipoTecnologia>("api/typetech/all");
                if (data.IsSuccess)
                {
                    return View((List<TipoTecnologia>)data.Result);
                }
                else
                    return View(new List<TipoTecnologia>());
            }
            else
                return RedirectToAction("TimeOut", "Usuario");
        }

        [HttpPost]
        public virtual async Task<JsonResult> GetTecnologia()
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iTecnologias.Get<TipoTecnologia>("api/typetech/all");
                if (data.IsSuccess) 
                {
                    var obj = JsonConvert.SerializeObject(data.Result);
                    json.Data = new { ok = true, existSession = true, obj = obj };
                }
                else
                {
                    json.Data = new { ok = false, existSession = true, msg = data.Message };
                }
            }
            else
                json.Data = new { ok = false, msg = "", existSession = false, url = Url.Action("TimeOut", "Usuario") }; 
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> GetTecnologiaById(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iTecnologias.GetById<TipoTecnologia>($"api/typetech/byid/{id}");
                if (data.IsSuccess) 
                {
                    var obj = JsonConvert.SerializeObject(data.Result);
                    json.Data = new { ok = true, existSession = true, obj = obj };
                }
                else
                {
                    json.Data = new { ok = false, existSession = true, msg = data.Message };
                }
            }
            else
                json.Data = new { ok = false, msg = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> PostTecnologia(TipoTecnologia modelo)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iTecnologias.Post<TipoTecnologia>("api/typetech/create", modelo);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message, url = Url.Action("Tecnologia", "Catalogos") };
                }
                else
                {
                    json.Data = new { ok = false, existSession = true, msg = data.Message };
                }
            }
            else
                json.Data = new { ok = false, msg = "Se caduco la sesión", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> PutTecnologia(TipoTecnologia modelo)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iTecnologias.Put<TipoTecnologia>("api/typetech/update", modelo, modelo.TipoTecnologiaID);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message, url = Url.Action("Tecnologia", "Catalogos") };
                }
                else
                {
                    json.Data = new { ok = false, existSession = true, msg = data.Message };
                }
            }
            else
                json.Data = new { ok = false, msg = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> DeleteTecnologia(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iTecnologias.Delete<TipoTecnologia>("api/typetech/delete", id);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message, url = Url.Action("Tecnologia", "Catalogos") };
                }
                else
                {
                    json.Data = new { ok = false, existSession = true, msg = data.Message };
                }
            }
            else
                json.Data = new { ok = false, msg = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        #endregion

        #region Idiomas
        public virtual async Task<ActionResult> Idiomas()
        {
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iIdiomas.Get<Idiomas>("api/languages/all");
                if (data.IsSuccess)
                {
                    var idiomas = (List<Idiomas>)data.Result;
                    return View(idiomas);
                }
                return View(new List<Idiomas>());
            }
            else
                return RedirectToAction("TimeOut", "Usuario");
        }

        [HttpPost]
        public virtual async Task<JsonResult> GetIdiomas()
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iIdiomas.Get<Idiomas>("api/languages/all");
                if (data.IsSuccess)
                {
                    if (((List<Idiomas>)data.Result).Count <= 0)
                        json.Data = new { ok = false, existSession = true, obj = data.Message };
                    else
                    {
                        var jobject = JsonConvert.SerializeObject(data.Result);
                        json.Data = new { ok = true, existSession = true, obj = jobject };
                    }
                }
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        
        [HttpPost]
        public virtual async Task<JsonResult> PostIdioma(Idiomas idiomas)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iIdiomas.Post<Idiomas>("api/languages/create", idiomas);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message, url = Url.Action("Idiomas", "Catalogos") };
                }
                else
                    json.Data = new { ok = false, existSession = true, msg = data.Message };
            }
            else
                json.Data = new { ok = false, msg = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> PutIdioma(Idiomas idioma)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iIdiomas.Put<Idiomas>("api/languages/update", idioma, idioma.IdiomasID);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message, url = Url.Action("Idiomas", "Catalogos") };
                }
                else
                    json.Data = new { ok = false, existSession = true, msg = data.Message };
            }
            else
                json.Data = new { ok = false, msg = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        [HttpPost]
        public virtual async Task<JsonResult> DeleteIdioma(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await iIdiomas.Delete<Response>("api/languages/delete", id);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message, url = Url.Action("Idiomas", "Catalogos") };
                }
                else
                    json.Data = new { ok = false, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        #endregion
    }
}