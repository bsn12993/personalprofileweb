﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace PersonalProfile.Admin.Controllers
{
    public class ConfiguracionIdiomaController : Controller
    {
        // GET: ConfiguracionIdioma
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SetupLanguage(string language)
        {
            JsonResult json = new JsonResult();
            if (language != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(language);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            }

            HttpCookie cookie = new HttpCookie("language");
            cookie.Value = language;
            Response.Cookies.Add(cookie);
            json.Data = new { ok = true, msg = "", url = Url.Action("Login", "Usuario") };
            return json;
        }

        [HttpPost]
        public JsonResult GetLanguageDefault()
        {
            JsonResult json = new JsonResult();
            var languageDefault = CultureInfo.CurrentCulture.Name;
            if (!string.IsNullOrEmpty(languageDefault))
            {
                json.Data = new { ok = true, msg = "", obj = languageDefault };
            }
            else
                json.Data = new { ok = true, msg = "", obj = "es-MX" };
            return json;
        }
    }
}