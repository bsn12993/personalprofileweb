﻿using PersonalProfile.Admin.Services;
using PersonalProfile.Admin.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PersonalProfile.Admin.Controllers
{
    public partial class ProyectoTecnologiaController : Controller
    {
        ProyectosServices _proyectoServices = null;

        public ProyectoTecnologiaController()
        {
            _proyectoServices = new ProyectosServices();
        }
        // GET: ProyectoTecnologia
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult ProyectoTecnologia(int idProyecto = 0, int idUsuario = 0, int idUsuarioProyecto = 0, string nombreProyecto = "")
        {
            if (SessionHelper.ExistUserInSession())
            {
                ViewBag.proyectosID = idProyecto;
                ViewBag.UsuarioProyectosID = idUsuarioProyecto;
                ViewBag.idUsuario = idUsuario;
                ViewBag.nombreProyecto = nombreProyecto;
                return View();
            }
            else
                return RedirectToAction("TimeOut", "Usuario");  
        }


    }
}