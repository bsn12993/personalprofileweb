﻿using PersonalProfile.Admin.Interface;
using PersonalProfile.Admin.Services;
using PersonalProfile.Admin.Session;
using PersonalProfile.Core.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PersonalProfile.Admin.Controllers
{
    public partial class ExperienciaLaboralController : Controller
    {
        IServices experienciaLaboralServices = null;

        public ExperienciaLaboralController()
        {
            experienciaLaboralServices = new ExperienciaLaboralServices();
        }
        // GET: ExperienciaLaboral
        public virtual ActionResult Proyectos(int experienciaLaboralID, int UsuarioExperienciaLaboralID, string nombre)
        {
            if (SessionHelper.ExistUserInSession())
            {
                ViewBag.nombre = nombre;
                ViewBag.experienciaLaboralID = experienciaLaboralID;
                ViewBag.UsuarioExperienciaLaboralID = UsuarioExperienciaLaboralID;
                return View();
            }
            else
                return RedirectToAction("TimeOut", "Usuario");
        }

        [HttpPost]
        public virtual async Task<JsonResult> GetExperienciaLaboral()
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await experienciaLaboralServices.Get<ExperienciaLaboral>("api/workexperience/all");
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Result };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
    }
}