﻿using Newtonsoft.Json;
using PersonalProfile.Admin.Config;
using PersonalProfile.Admin.Enum;
using PersonalProfile.Admin.Extensions;
using PersonalProfile.Admin.Interface;
using PersonalProfile.Admin.Models;
using PersonalProfile.Admin.Services;
using PersonalProfile.Admin.Session;
using PersonalProfile.Core.EntityModels;
using PersonalProfile.Core.Models;
using PersonalProfile.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PersonalProfile.Admin.Controllers
{
    public partial class UsuarioController : Controller
    {
        IUsuarioServices _usuarioServices = null;
        IServices _usuarioConocimientosServices = null;
        IServices _usuarioExperiencia = null;
        IServices _usuarioIdioma = null;
        IServices _usuarioProyecto = null;
        IServices _proyectoServices = null;
        IServices _usuarioFormacion = null;
        ProyectoTecnologiaServices _proyectoTecnologia = null;

        public UsuarioController()
        {
            _usuarioServices = new UsuarioServices();
            _usuarioConocimientosServices = new UsuarioConocimientoServices();
            _usuarioExperiencia = new UsuarioExperienciaServices();
            _usuarioIdioma = new UsuarioIdiomaServices();
            _usuarioProyecto = new UsuarioProyectoServices();
            _proyectoServices = new ProyectosServices();
            _usuarioFormacion = new FormacionAcademicaServices();
            _proyectoTecnologia = new ProyectoTecnologiaServices();
        }
   
        #region Usuario
        public virtual ActionResult RegistrarUsuario()
        {
            return View(new Usuario());
        }       
        [HttpPost]
        public virtual async Task<JsonResult> PostUsuarioNuevo(Usuario model)
        {
            JsonResult json = new JsonResult();
            if (ModelState.IsValid && model != null)
            {
                var data = await _usuarioServices.Post<Usuario>(AppSettingsValues.WsUser.GetUrl(Enum.EnumRequest.Create), model);
                if (data.IsSuccess)
                {
                    json.Data = new { ok = data.IsSuccess, obj = data.Message, url=Url.Action("Login","Usuario") };
                }
                else
                    json.Data = new { ok = data.IsSuccess, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "Datos no correctos" };
            return json;
        }
        public virtual async Task<ActionResult> Usuario()
        {
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioServices.GetById<Usuario>
                    (AppSettingsValues.WsUser.GetUrl(EnumRequest.GetById) + "/" + SessionHelper.GetUser().UsuarioID);
                if (data.IsSuccess)
                {
                    var usuario = (Usuario)data.Result;
                    usuario.imagen = (!string.IsNullOrEmpty(usuario.imagen)) ? usuario.imagen : Url.Content("~/Images/profile.jpg");
                    usuario.contrasenia = HashHelper.Base64Decode(usuario.contrasenia);
                    ViewBag.IdUser = SessionHelper.GetUser().UsuarioID;
                    usuario.fechaNacimiento = (usuario.fechaNacimiento.HasValue)
                        ? usuario.fechaNacimiento : DateTime.Now.Date;
                    ViewBag.Nombre = SessionHelper.GetUser().nombre + " " + SessionHelper.GetUser().apellidoPaterno + " " + SessionHelper.GetUser().apellidoMaterno;
                    //data.contrasenia = HashHelper.Base64Decode(data.contrasenia);
                    var userKey = Core.Util.HashHelper.Base64Encode(usuario.correo);
                    ViewBag.UrlResume = $"{AppSettingsValues.URLProfile}Perfil/index?user={userKey}";
                    Session["user"] = string.Format("{0} {1} {2}", SessionHelper.GetUser().nombre, SessionHelper.GetUser().apellidoPaterno, SessionHelper.GetUser().apellidoMaterno);
                    Session["email"] = SessionHelper.GetUser().correo;
                    Session["image"] = (!string.IsNullOrEmpty(SessionHelper.GetUser().imagen)) ? SessionHelper.GetUser().imagen : Url.Content("~/Images/profile.jpg");
                    return View(usuario);
                }
                return View("_Error");
            }
            else
                return View("TimeOut");
        }
        [HttpPost]
        public virtual async Task<JsonResult> PutUsuario(UsuarioModel model)
        {
            JsonResult json = new JsonResult();
            Core.Models.Response convertirImagen = new Core.Models.Response();
            Usuario user = new Core.EntityModels.Usuario();
            if (SessionHelper.ExistUserInSession()) 
            {
                if (model.file != null)
                {
                    convertirImagen = FileHelper.ConvertFileToBase64(model.file);
                    if (convertirImagen.IsSuccess)
                        model.imagen = (string)convertirImagen.Result;
                    else
                    {
                        json.Data = new { ok = convertirImagen.IsSuccess, obj = convertirImagen.Message };
                        return json;
                    }
                }

                user.UsuarioID = model.UsuarioID;
                user.apellidoPaterno = model.apellidoPaterno;
                user.apellidoMaterno = model.apellidoMaterno;
                user.contrasenia = HashHelper.Base64Encode(model.contrasenia);
                user.correo = SessionHelper.GetUser().correo;
                user.descripcion = (!string.IsNullOrEmpty(model.descripcion)) ? model.descripcion : string.Empty;
                user.direccion = model.direccion;
                user.fechaNacimiento = model.fechaNacimiento;
                user.imagen = model.imagen;
                user.nombre = model.nombre;
                user.perfil = model.perfil;
                user.telefono = model.telefono;

                var data = await _usuarioServices.Put<Usuario>(AppSettingsValues.WsUser.GetUrl(EnumRequest.Update), user, user.UsuarioID);
                if (data.IsSuccess)
                {
                    data.Result = JsonConvert.DeserializeObject<Usuario>(data.Result.ToString());
                    SessionHelper.GetUser().apellidoPaterno = ((Usuario)data.Result).apellidoPaterno;
                    SessionHelper.GetUser().apellidoMaterno = ((Usuario)data.Result).apellidoMaterno;
                    SessionHelper.GetUser().contrasenia = ((Usuario)data.Result).contrasenia;
                    SessionHelper.GetUser().descripcion = (!string.IsNullOrEmpty(((Usuario)data.Result).descripcion))
                        ? ((Usuario)data.Result).descripcion : string.Empty;
                    SessionHelper.GetUser().direccion = ((Usuario)data.Result).direccion;
                    SessionHelper.GetUser().fechaNacimiento = ((Usuario)data.Result).fechaNacimiento;
                    SessionHelper.GetUser().imagen = ((Usuario)data.Result).imagen;
                    SessionHelper.GetUser().nombre = ((Usuario)data.Result).nombre;
                    SessionHelper.GetUser().perfil = ((Usuario)data.Result).perfil;
                    SessionHelper.GetUser().telefono = ((Usuario)data.Result).telefono;
                    //SessionHelper.AddUserToSession((Usuario)data.Result);
                    ViewBag.IdUser = SessionHelper.GetUser().UsuarioID;
                    ViewBag.Nombre = SessionHelper.GetUser().nombre + " " + SessionHelper.GetUser().apellidoPaterno + " " + SessionHelper.GetUser().apellidoMaterno;
                    Session["user"] = string.Format("{0} {1} {2}", SessionHelper.GetUser().nombre, SessionHelper.GetUser().apellidoPaterno, SessionHelper.GetUser().apellidoMaterno);
                    Session["email"] = SessionHelper.GetUser().correo;
                    Session["image"] = (!string.IsNullOrEmpty(SessionHelper.GetUser().imagen)) ? SessionHelper.GetUser().imagen : Url.Content("~/Images/profile.jpg");
                    json.Data = new { ok = data.IsSuccess, existSession = true, msg = data.Message, url = Url.Action("Usuario", "Usuario") };
                }
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PostUsuario(Usuario model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioServices.Post<Usuario>(AppSettingsValues.WsUser.GetUrl(EnumRequest.Create), model);
                if (data != null && data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> DeleteUsuario(int id = 0)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioServices.Delete<Response>(AppSettingsValues.WsUser.GetUrl(EnumRequest.Delete), id);
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }

        public FileResult GetPDFFile()
        {
            var usuario = SessionHelper.GetUser();
            var userKey = HashHelper.Base64Encode(usuario.correo);
            var urlHtml = $"{AppSettingsValues.URLProfile}Perfil/index?user={userKey}";
            var urlCss= $"{AppSettingsValues.URLProfile}Content/css/pillar-1.css";
            var html = FileHelper.GetHtmlFromURL(urlHtml);
            var css = FileHelper.GetHtmlFromURL(urlCss);
            
            var byteFile = FileHelper.GetPDFFile(html, css);
            return File(byteFile, "application/pdf", usuario.nombre + " " + usuario.apellidoPaterno + " " + usuario.apellidoMaterno + ".pdf");
        }

        #endregion

        #region UsuarioConocimientos
        public virtual async Task<ActionResult> UsuarioConocimientos()
        {
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioServices.Get<UsuarioConocimiento>($"/api/user/skills/{SessionHelper.GetUser().UsuarioID}");
                if (data.IsSuccess)
                {
                    var lst = JsonConvert.DeserializeObject<List<UsuarioConocimiento>>(data.Result.ToString());
                    ViewBag.IdUser = SessionHelper.GetUser().UsuarioID;
                    ViewBag.Nombre = SessionHelper.GetUser().nombre + " " + SessionHelper.GetUser().apellidoPaterno + " " + SessionHelper.GetUser().apellidoMaterno;
                    return View(lst);
                }
                else
                    return View(new List<UsuarioConocimiento>());
            }
            else
                return View("TimeOut");
        }
        [HttpPost]
        public virtual async Task<JsonResult> UsuarioConocimientosById(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioConocimientosServices.GetById<UsuarioConocimiento>($"/api/user/skills/byid/{id}");
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Result };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PostUsuarioConocimientos(UsuarioConocimiento model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioConocimientosServices.Post<UsuarioConocimiento>($"/api/user/skills/create", model);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioConocimientos", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PutUsuarioConocimientos(UsuarioConocimiento model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioConocimientosServices.Put<UsuarioConocimiento>($"/api/user/skills/update", model, model.UsuarioConocimientoID);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioConocimientos", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> DeleteUsuarioConocimientos(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioConocimientosServices.Delete<Response>("/api/user/skills/delete", id);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioConocimientos", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        #endregion

        #region UsuarioExperienciaLaboral
        public virtual async Task<ActionResult> UsuarioExperienciaLaboral()
        {
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioExperiencia.Get<UsuarioExperienciaLaboral>($"/api/user/workexperiences/{SessionHelper.GetUser().UsuarioID}");
                if (data.IsSuccess)
                {
                    ViewBag.IdUser = SessionHelper.GetUser().UsuarioID;
                    ViewBag.Nombre = SessionHelper.GetUser().nombre + " " + SessionHelper.GetUser().apellidoPaterno + " " + SessionHelper.GetUser().apellidoMaterno;
                    return View((List<UsuarioExperienciaLaboral>)data.Result);
                }
                else
                    return View(new List<UsuarioExperienciaLaboral>());
            }
            return View("TimeOut");
        }
        [HttpPost]
        public virtual async Task<JsonResult> UsuarioExperienciaLaboralByID(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioExperiencia.GetById<UsuarioExperienciaLaboral>($"/api/user/workexperiences/byid/{id}");
                if (data.IsSuccess)
                {
                    var Jobj = JsonConvert.SerializeObject(data.Result);
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = Jobj };
                }
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PostUsuarioExperienciaLaboral(UsuarioExperienciaLaboral model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioExperiencia.Post<UsuarioExperienciaLaboral>("/api/user/workexperiences/create", model);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioExperienciaLaboral", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PutUsuarioExperienciaLaboral(UsuarioExperienciaLaboral model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioExperiencia.Put<UsuarioExperienciaLaboral>("/api/user/workexperiences/update/", model, model.UsuarioExperienciaLaboralID);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioExperienciaLaboral", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> DeleteUsuarioExperienciaLaboral(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioExperiencia.Delete<Response>($"/api/user/workexperiences/delete/", id);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioExperienciaLaboral", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        #endregion

        #region UsuarioIdiomas
        public virtual async Task<ActionResult> UsuarioIdiomas()
        {
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioIdioma.Get<UsuarioIdioma>($"/api/user/languages/{SessionHelper.GetUser().UsuarioID}");
                if (data.IsSuccess)
                {
                    ViewBag.IdUser = SessionHelper.GetUser().UsuarioID;
                    ViewBag.Nombre = SessionHelper.GetUser().nombre + " " + SessionHelper.GetUser().apellidoPaterno + " " + SessionHelper.GetUser().apellidoMaterno;
                    return View((List<UsuarioIdioma>)data.Result);
                }
                else
                    return View(new List<UsuarioIdioma>());
            }
            else
                return View("TimeOut");
        }
        [HttpPost]
        public virtual async Task<JsonResult> UsuarioIdiomasById(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioIdioma.GetById<UsuarioIdioma>($"/api/user/languages/byid/{id}");
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Result };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PostUsuarioIdiomas(UsuarioIdioma model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioIdioma.Post<UsuarioIdioma>("/api/user/languages/create", model);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioIdiomas", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PutUsuarioIdiomas(UsuarioIdioma model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioIdioma.Put<UsuarioIdioma>("/api/user/languages/update", model, model.UsuarioIdiomaID);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioIdiomas", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> DeleteIdiomas(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioIdioma.Delete<Response>("/api/user/languages/delete", id);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        #endregion

        #region UsuarioProyectos
        public virtual async Task<ActionResult> UsuarioProyectos()
        {
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioProyecto.Get<UsuarioProyectos>($"/api/user/projects/{SessionHelper.GetUser().UsuarioID}");
                if (data.IsSuccess)
                {
                    ViewBag.IdUser = SessionHelper.GetUser().UsuarioID;
                    ViewBag.Nombre = SessionHelper.GetUser().nombre + " " + SessionHelper.GetUser().apellidoPaterno + " " + SessionHelper.GetUser().apellidoMaterno;
                    return View((List<UsuarioProyectos>)data.Result);
                }
                else
                    return View(new List<UsuarioProyectos>());
            }
            return View("TimeOut");
        }
        [HttpPost]
        public virtual async Task<JsonResult> GetUsuarioProyectos()
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _proyectoServices.Get<Proyectos>("api/projects/all");
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Result };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> UsuarioProyectosById(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioProyecto.GetById<UsuarioProyectos>($"/api/user/projects/byid/{id}");
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Result };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PostUsuarioProyectos(UsuarioProyectos model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioProyecto.Post<UsuarioProyectos>("/api/user/projects/create", model);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioProyectos", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PutUsuarioProyectos(UsuarioProyectos model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioProyecto.Put<UsuarioProyectos>("/api/user/projects/update", model, model.UsuarioProyectosID);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioProyectos", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> DeleteUsuarioProyectos(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioProyecto.Delete<Response>("/api/user/projects/delete", id);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioProyectos", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        #endregion

        #region UsuarioFormacionAcademica
        public virtual async Task<ActionResult> UsuarioFormacionAcademica()
        {
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioFormacion.Get<UsuarioFormacionAcademica>($"/api/user/education/{SessionHelper.GetUser().UsuarioID}");
                if (data.IsSuccess)
                {
                    ViewBag.IdUser = SessionHelper.GetUser().UsuarioID;
                    ViewBag.Nombre = SessionHelper.GetUser().nombre + " " + SessionHelper.GetUser().apellidoPaterno + " " + SessionHelper.GetUser().apellidoMaterno;
                    return View((List<UsuarioFormacionAcademica>)data.Result);
                }
                else
                    return View(new List<UsuarioFormacionAcademica>());
            }
            return View("TimeOut");
        }
        [HttpPost]
        public virtual async Task<JsonResult> UsuarioFormacionAcademicaById(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioFormacion.GetById<UsuarioFormacionAcademica>($"/api/user/education/byid/{id}");
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Result };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PostUsuarioFormacionAcademica(UsuarioFormacionAcademica model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioFormacion.Post<UsuarioFormacionAcademica>("/api/user/education/create", model);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioFormacionAcademica", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PutUsuarioFormacionAcademica(UsuarioFormacionAcademica model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                model.usuarioID = SessionHelper.GetUser().UsuarioID;
                var data = await _usuarioFormacion.Put<UsuarioFormacionAcademica>("/api/user/education/update", model, model.UsuarioFormacionAcademicaID);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioFormacionAcademica", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> DeleteUsuarioFormacionAcademica(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _usuarioFormacion.Delete<UsuarioFormacionAcademica>("/api/user/education/delete", id);
                if (data.IsSuccess && data != null)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        #endregion

        #region ProyectoTecnologia
        [HttpPost]
        public virtual async Task<JsonResult> GetProyectoTecnologia(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _proyectoTecnologia.GetByProject<ProyectoTecnologia>($"api/projects/technologies/byproject/{id}");
                if (data.IsSuccess)
                {
                    var tipoTecnologia = JsonConvert.SerializeObject(data.Result);
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = tipoTecnologia };
                }
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PostProyectoTecnologia(ProyectoTecnologia model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _proyectoTecnologia.Post<ProyectoTecnologia>("api/projects/technologies/create", model);
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioProyectos", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> PutProyectoTecnologia(ProyectoTecnologia model)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _proyectoTecnologia.Put<ProyectoTecnologia>("api/projects/technologies/update", model, model.ProyectoTecnologiaID);
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioProyectos", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        [HttpPost]
        public virtual async Task<JsonResult> DeleteProyectoTecnologia(int id)
        {
            JsonResult json = new JsonResult();
            if (SessionHelper.ExistUserInSession())
            {
                var data = await _proyectoTecnologia.Delete<Response>("api/projects/technologies/delete", id);
                if (data.IsSuccess)
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message, url = Url.Action("UsuarioProyectos", "Usuario") };
                else
                    json.Data = new { ok = data.IsSuccess, existSession = true, obj = data.Message };
            }
            else
                json.Data = new { ok = false, obj = "", existSession = false, url = Url.Action("TimeOut", "Usuario") };
            return json;
        }
        #endregion

        #region Login
        public virtual ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public virtual async Task<JsonResult> LogIn(Usuario usuario)
        {
            JsonResult json = new JsonResult();
            if (!string.IsNullOrEmpty(usuario.correo) && !string.IsNullOrEmpty(usuario.contrasenia))
            {
                var data = await _usuarioServices.GetUsuarioCorreoContrasenia(usuario.correo, usuario.contrasenia);
                if (data.IsSuccess)
                {
                    SessionHelper.AddUserToSession((Usuario)data.Result);
                    Session["user"] = string.Format("{0} {1} {2}", SessionHelper.GetUser().nombre, SessionHelper.GetUser().apellidoPaterno, SessionHelper.GetUser().apellidoMaterno);
                    Session["email"] = SessionHelper.GetUser().correo;
                    Session["image"] = (!string.IsNullOrEmpty(SessionHelper.GetUser().imagen)) ? SessionHelper.GetUser().imagen : Url.Content("~/Images/profile.jpg");
                    json.Data = new { ok = true, obj = "", url = Url.Action("Usuario", "Usuario") };
                }
                else
                    json.Data = new { ok = false, msg = data.Message };
            }
            else
                json.Data = new { ok = false, msg = "Datos no correctos" };
            return json;
        }
        #endregion

        public virtual ActionResult TimeOut()
        {
            SessionHelper.DestroyUserSession();
            return View();
        }

        public virtual ActionResult LogOut()
        {
            SessionHelper.DestroyUserSession();
            return RedirectToAction("Login");
        }
    }
}