﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PersonalProfile.Admin.Config
{
    public static class AppSettingsValues
    {
        public static string EndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["API"].ToString();
            }
        }

        public static string URLProfile
        {
            get
            {
                return ConfigurationManager.AppSettings["url_Profile"].ToString();
            }
        }

        public static string WsSkill
        {
            get
            {
                return ConfigurationManager.AppSettings["ws_Skill"].ToString();
            }
        }

        public static string WsExperience
        {
            get
            {
                return ConfigurationManager.AppSettings["ws_Experiences"].ToString();
            }
        }

        public static string WsEducation
        {
            get
            {
                return ConfigurationManager.AppSettings["ws_Education"].ToString();
            }
        }

        public static string WsLanguages
        {
            get
            {
                return ConfigurationManager.AppSettings["ws_Languages"].ToString();
            }
        }

        public static string WsProfile
        {
            get
            {
                return ConfigurationManager.AppSettings["ws_Profile"].ToString();
            }
        }

        public static string WsProjects
        {
            get
            {
                return ConfigurationManager.AppSettings["ws_Projects"].ToString();
            }
        }

        public static string WsUser
        {
            get
            {
                return ConfigurationManager.AppSettings["ws_User"].ToString();
            }
        }

        public static string WsTypeTech
        {
            get
            {
                return ConfigurationManager.AppSettings["ws_TypeTech"].ToString();
            }
        }

    }
}